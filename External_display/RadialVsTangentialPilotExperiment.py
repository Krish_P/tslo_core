from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import customTime
import random
from random import randint
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from PixelConversion import PixelConversion
import pixlet


# We have 2 conditions
# 1. 2 flankers along different meridians
# 2. all 4 flankers @ different distances
num_flankers='2_flankers'
subj_id=1
session_num=3

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)

#USER INPUT:
# Enter diagonal monitor size in inches
monitor='laptop'
if monitor=='laptop':
    diagonal_size_inch=15
    dist_to_screen= 50 #cm (APPROX.)
    refreshRate=60
    diagonal_size=diagonal_size_inch * 2.54 #diagonal screen size
else:
    refreshRate=window.getActualFrameRate()
    diagonal_size=68.58 #diagonal screen size
    dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))


#We enter the monitor parameters here and calculate pixels per cm
horz_res=monsize[0]
vert_res=monsize[1]


#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
if monitor=='laptop':
    msg_size=pixel_per_degree/2
    msg_offset=200
else:
    msg_size=pixel_per_degree/5
    msg_offset=400

# Specify cue offset as a multiple of number of pixels per degree 
peripheral_cue=True
# Fixation target could be either cross or plus with circle in the middle
# Options: 1: "x" or 2" "+"
fixation_type='x'

if fixation_type=="+":
    fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
elif fixation_type=='x':
    fix_line_UR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
fix_c=visual.Circle(window, size=12, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(-1,-1,-1) )

#Experiment conditions
flanker_info=100
num_repeats=25 # Sets the number of repeats for each flanker spacing at a given ecc
oblique_cond=0
#flanker_cond: 0 for both rad/tang flankers; 1: for radial and 2: for tangential
flanker_cond=0
if monitor=='laptop':
    eccentricity_deg_diag=5
    target_presentation=12 #flips @ 60Hz
    letter_height=(30/60)
else:
    eccentricity_deg_diag=1
    target_presentation=20 #flips @ 120 Hz
    letter_height=(15/60)

cue_condition=True
mask_condition=False
location_uncertainity=True 
reverse_flanker_contrast=False
save_image=True
foveal_mode=False
flanker_space=['radial','tangential']#,'oblique1_horizontal','oblique2_vertical']
flanker_spacing_list=np.repeat(flanker_space,num_repeats)
random.shuffle(flanker_spacing_list)
stim_location_list=['lowerLeft','lowerRight']

#Create basic stimuli here that could be called multiple times during the course of the experiment
small_square=visual.Rect(window,width=128,height=128,units='pix',fillColor='white', fillColorSpace='rgb',pos=(975,1625))
wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start the experiment')
wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

# The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
# Cue for rightward target location
cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
#cue for leftward target location
cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
# Cue for voluntary microsaccade
cue_c=visual.Circle(window, size=4, fillColor=(0,0,0), pos=(0,0), lineColor=(0,0,0) )
# Cue for target location
target_c=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
# For oblique targets presented at 1 of 4 locations we have separate cues
target_c_ll=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
target_c_lr=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )

#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="T",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1],height=pixel_per_degree )
flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
    
# This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
letter_height_pix=letter_height*pixel_per_degree
#We use the position of the fixation cross center to position the target and flankers
fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 
eccentricity_deg=np.sqrt(eccentricity_deg_diag**2/2)

#This is used to provide a audio feedback to the user
beep_sound_correct=Sound(value=1000,secs=0.25)
beep_sound_incorrect=Sound(value=250,secs=0.25)

trialCounter=1
for aflanker in flanker_spacing_list:
    # Check if escape was pressed and stop loop
    if 'key_pressed' in locals():
        if key_pressed=='escape':
            keep_repeating=False
            break
    if 'keys' in locals():
        if keys=='espace':
            keep_repeating=False
            break
    # we create a new variable that would hold the value for flanker spacing 
    #condition
    flanker_val_str=aflanker
    file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())

    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_condition_%s'%(subj_id,session_num,trialCounter,eccentricity_deg_diag, flanker_val_str)
    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
    outfile = open(outfilename, "a")
    #Add the header 
    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
    "stim_dur","num_flankers","corr") )

    event.clearEvents()
    #The letter size is set here
    letter_size_deg=letter_height
    letter_size_pix=letter_size_deg*pixel_per_degree
    target.height=letter_size_pix
    flanker_left.height=letter_size_pix
    flanker_right.height=letter_size_pix
    flanker_up.height=letter_size_pix
    flanker_down.height=letter_size_pix
    
    # The following set of lines set the flanker condition 
    stim_location=np.random.choice(stim_location_list)
    eccentricity_pix=int(eccentricity_deg*pixel_per_degree)
    
    # Can either be left or right
    if stim_location=='left':
        eccentricity_x=fixation_pos[0]-eccentricity_pix
        eccentrcitiy_y=0
    elif stim_location=='right':
        eccentricity_x=fixation_pos[0]+eccentricity_pix
        eccentricity_y=0
    # Or can be along 4 diagonal meridians from fixation
    elif stim_location=='lowerLeft':
        eccentricity_x=fixation_pos[0]-eccentricity_pix
        eccentricity_y=fixation_pos[1]-eccentricity_pix
    elif stim_location=='lowerRight':
        eccentricity_x=fixation_pos[0]+eccentricity_pix
        eccentricity_y=fixation_pos[1]-eccentricity_pix
        
    # We set the position of the target placeholder using the eccentricity provided
    # and the location of the fixation cross
    target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
    target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
    # we then set the target location randomly based on the different location choices
    # available 
    target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 

    # We would overwrite this if we want oblique targets
    if oblique_cond==True:
        target_location=(eccentricity,-eccentricity)

    target.pos=target_location
    # We would make sure we offset cue location horizontally and not vertically
    # important when doing oblique conditions
    if stim_location!='left' or stim_location!='right':
        cue_c.pos=target_location            
    else:
        cue_c.pos=(target_location[0],0)
    # we would drop another marker for target location
    if oblique_cond==True:
        target_c.pos=target_location
    
    # First we would reset fixation cross to original color
    fix_line_UL.color=[-1,-1,-1]
    fix_line_UR.color=[-1,-1,-1]
    fix_line_LL.color=[-1,-1,-1]
    fix_line_LR.color=[-1,-1,-1]
    #THe flanker spacing is set on eacn trial based on the stimulus size
    # We would have separate spacings for testing meridian and opposite meridian
    flanker_spacing_testing_meridian=2.5
    flanker_deg_testing_meridian=flanker_spacing_testing_meridian*letter_height # The flanker spacing scales based on target letter size
    flanker_spacing_pix_testing_meridian=int(flanker_deg_testing_meridian*pixel_per_degree)
    # Set the same for the opposite meridian
    if num_flankers=='4_flankers':
        flanker_spacing_opp_meridian=6
    elif num_flankers=='2_flankers':
        flanker_spacing_opp_meridian=100
    flanker_deg_opp_meridian=flanker_spacing_opp_meridian*letter_height # The flanker spacing scales based on target letter size
    flanker_spacing_pix_opp_meridian=int(flanker_deg_opp_meridian*pixel_per_degree)
    if aflanker=='radial': #[Radial flankers tested; Tangential flankers not tested]
        if stim_location=='lowerLeft':
            # Flankers being tested are set first
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
            # Flankers not being tested
            flanker_up.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
            flanker_down.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
        elif stim_location=='lowerRight':
            flanker_up.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
            flanker_down.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
            #flankers not being tested 
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
    elif aflanker=='tangential': # [Tangential flankers tested;Radial flankers not tested]
        if stim_location=='lowerLeft':
            # flankers being tested are set first
            flanker_up.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
            flanker_down.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
            #flankers not being tested 
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
        elif stim_location=='lowerRight':
            # Flankers being tested are set first
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
            # Flankers not being tested
            flanker_up.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
            flanker_down.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
    elif aflanker=='oblique1_horizontal': # This would test horizontal meridian
        #flankers being tested
        flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1])
        flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1])
        #flankers not being tested 
        flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_opp_meridian)
        flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_opp_meridian)
    elif aflanker=='oblique2_vertical': # This would test along vertical meridian
            # Flankers being tested
            flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_testing_meridian)
            flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_testing_meridian)
            # Flankers not being tested
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1])
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1])
            # Flankers being tested
            flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_testing_meridian)
            flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_testing_meridian)
            # Flankers not being tested
            flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1])
            flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1])
    target_type_list=['+']*5

    # We shuffle the list of stimuli before assigning them
    random.shuffle(target_type_list)
    # We first assign target stimuli
    target_lists=['u','d','l','r']
    target_let=np.random.choice(target_lists)

    # We finally assign flanker stimuli
    flanker_let_1=target_type_list[1]
    flanker_let_2=target_type_list[2]
    flanker_let_3=target_type_list[3]
    flanker_let_4=target_type_list[4]

    # We indicate stimulus location
    cueLocation=stim_location
    if fixation_type=="x":
        if cueLocation=='lowerLeft':
            fix_line_LL.color=[1,0,0]
        elif cueLocation=='lowerRight':
            fix_line_LR.color=[1,0,0]
    # we then set the respective target and flanker characters 
    target.text=target_let
    flanker_left.text=flanker_let_1
    flanker_right.text=flanker_let_2
    flanker_up.text=flanker_let_3
    flanker_down.text=flanker_let_4
    # The stimulus duration is used as a fraction of the frame rate of the display;
    # THis ensures better timing of the target presentations
    target.render()
    flanker_right.render()
    flanker_left.render()
    flanker_up.render()
    flanker_down.render()

    while True:
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
            stim1.draw()
        wait_msg.draw()
        window.flip()
                        
        keys=event.getKeys()
        if len(keys)>0 :
            break

    initialDelay=np.random.choice(np.arange(0.750,1.250,0.100))
    for nflips in range(int(refreshRate*initialDelay)):
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                    stim1.draw()

        wait_msg2.draw()

    # we clear key events that could occured after the end of the previous trial 
    event.clearEvents()
    # we would only draw the next stimulus only when a saccade was not detected 
    # during the delay period. If saccade did occur we skip to the response window
    for frameN in range(target_presentation):
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
            stim1.draw()
        target.draw()                
        # we test whether the stimulus would appear on the left/right and based
        # on that we set the inner and outer flankers
        if stim_location=='left' or stim_location=='lowerLeft':                    
            inner_flanker=flanker_right
            outer_flanker=flanker_left
        elif stim_location=='right' or stim_location=='lowerRight':
            inner_flanker=flanker_left
            outer_flanker=flanker_right
        
        inner_flanker.draw()
        outer_flanker.draw()
        flanker_up.draw()
        flanker_down.draw()

    keepGoing=True
    while keepGoing==True:
        response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
        response_wait.draw()
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
            stim1.draw()
        if oblique_cond==True:
            target_c.draw()

        flip_time=window.flip()

        key=event.getKeys()
        if len(key)>0:
            keepGoing=False
            continue

    # we check if there was a keypress made duration the response window otherwise we set a defualt value
    if 'key' not in locals():
        key_pressed='Nil' # arbitary value 
    elif len(key)==0:
        key_pressed='Nil'
    else:
        key_pressed=key[0]
            
    #An escape key press here terminates the loop
    if key_pressed=='escape':
        keep_repeated=False
        break
    # we assign a response value based on the keypress that was made by the subject
    if key_pressed=='up':
        response_key='u'
    elif key_pressed=='down':
        response_key='d'
    elif key_pressed=='left':
        response_key='l'
    elif key_pressed=='right':
        response_key='r'
    else:
        response_key='100'

    # we woudl provide feedback based on whether or not the response was correct
    if response_key==target.text:
        beep_sound_correct.play()
        corr=1
    else:
        beep_sound_incorrect.play()
        corr=0

    # we would write the response to the output file
    # we convert target presentation to ms from flips before writing to output file
    target_presentation_ms=target_presentation*(1000/refreshRate)
    if key_pressed!='escape':
        outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%s,%s,%2f,%s,%d\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                        letter_size_deg,eccentricity_deg_diag,aflanker,stim_location,target_presentation_ms,num_flankers,corr) )
    #increment trial counter
    trialCounter+=1

#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

# we close the different programs/processes that were opened
window.close()
core.quit()