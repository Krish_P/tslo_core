from psychopy import core, visual, event, monitors, data, gui
import numpy as np
from os import path
import time
import random
from psychopy.sound import Sound
from psychopy import logging
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
import pixlet
from PixelConversion import PixelConversion
from make_mask import make_mask
#===================================================================================================================================
# We specify the eccentricity to test/ find threshold
eccentricity_deg=0.33
subj_id=2
session_num=1
trial_num=1
#===================================================================================================================================

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=6, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

#===================================================================================================================================
## Experimental Parameters

#ONLY PART TO BE CHANGED AT THE START OF EXPERIMENT:
#The following set of variables control the main set of parameters for the experiment 
# The following are the list of parameters that can be altered and brief description of what it does to
# the protocol:
    # location_uncertainity: This determines whether the stimulus would change in position across the vertical midline b/w trials
    # inner_flanker: We would want to remove the inner flanker to prevent overlap with the fixation target at close eccentricities
    # cue_condition: This defines whether a cue would be used in the experimental sequence or not
    # reverse_flanker_contrast: changes the polarity of the flankers
    # foveal_mode: the targets would be presented at the point of fixation
    # save_image: To save stimulus frame as image

# Experimental sequence parameters would be set here
cue_condition=False
foveal_mode=False
    

# These parameters are set locally (Stimulus Machine)
num_repeats=1 # sets the number of times the quest is repeated for each flanker spacing
mask_condition=False
location_uncertainity=False 
inner_flanker=False
reverse_flanker_contrast=False
save_image=False 
# We would draw stimuli based on target type, currently this can be one of the following:
# 1. bars
# 2. E
# 3. numbers
target_type='T'

# These parameters would be set at the start of the experiment and ideally would not want to change them 
# after the start of the experiment
#Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
# on the fixation cross for variable duration before stimulus onset
# Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
target_presentation=0.048 #sec
cue_presentation= 0.050 #sec
fixation_duration=0.450 #sec 
mask_duration=0.200 #sec
response_window=2.00#sec

# This creates a list of different flanker spacings to be used which would be multiplied
# with the letter height within the QUEST method to given spacings as a function of letter height : nominal spacings
# We also randomize the order of the flanker spacing on each run of the script
# Depending on the spacing condition that is set at the start of the experiment we would run all spacings otherwise
# we would run just the ones specified manually below
flanker_space=[100]
timing_list=np.arange(0.250,1.5,0.25)

# We create a dictionary that contains the starting letter size for a particular flanker spacing at a certain eccentricity. We would 
# generally want this size to be supra threshold, so as to ensure that the subjects are more likely to get correct responses on the 
# first set of trials
# Here we have a nested dictionary with the eccentricity as the first layer and 
# flanker spacing as the second
params={0.50:0.25,
        0.75:0.25,
        1:0.25,
        2:0.35,
        3:0.35,
        4:0.35,
        5:0.5}
#===================================================================================================================================
## Experiment Stimuli: Here we create the different stimuli that would later be used in the experiment

# The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
if location_uncertainity==True:
    stim_location_list=['left','right']
    fix_xpos=0
    msg_offset=150
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line
else:
    stim_location_list=['right'] #This would make all stimuli appear to the right or left of the fixation target
    fix_xpos=0
    msg_offset=150
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line


#Create basic stimuli here that could be called multiple times during the course of the experiment
msg_size=pixel_per_degree/6
small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start the experiment')
wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

# The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
# Cue for rightward target location
cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
#cue for leftward target location
cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)

#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
# This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
# The letter size is kept constant and is set to 1.5x the threshold from the isolated letter threshold measures.
# To ensure letter size is supra threshold
#letter_height=(params[eccentricity_deg])*1.5
letter_height=0.25
letter_height_pix=letter_height*pixel_per_degree

# We also create a set of post masks for different letter sizes that might be used in the experiment. 
# Note: The main advantage of pre-rendered masks is to ensure smooth operation of the script 
prog_bar = visual.Rect(win=window, pos=(0,0), size=(200,30), lineColor='black',fillColor='black')
render_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Rendering Masks...')
render_msg.draw()
window.flip()

# Mask parameters
mask_height=250
mask_width=250
mask_density=0.4
num_mask=5 #4 masks for flankers and one for the target

prog_bar.size=(200*(6)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()

# we then create the masks
mask_target=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
prog_bar.size=(200*(5)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()
mask_inner_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
prog_bar.size=(200*(4)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()
mask_outer_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
prog_bar.size=(200*(3)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()
mask_up_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
prog_bar.size=(200*(2)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()
mask_lower_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
prog_bar.size=(200*(1)/num_mask,50)
render_msg.draw()
prog_bar.draw()
window.flip()
#===================================================================================================================================
## Experimental sequence:

#We use the position of the fixation cross center to position the target and flankers
fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 

# The following set of lines set the flanker condition 
eccentricity_pix=eccentricity_deg*pixel_per_degree
eccentricity=fixation_pos[0]+eccentricity_pix
if foveal_mode==True:
    target_location=(fix_h.start[0],0)
else:
    target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
target.pos=target_location
                              
#This is used to provide a audio feedback to the user 
beep_feedback=core.CountdownTimer()
beep_sound_correct=Sound(value=1000,secs=0.25)
beep_sound_incorrect=Sound(value=250,secs=0.25)

# This is the outer bigger loop which controls the different flanker spacing/letter height and in the inner loop the other parameter is 
# tested.  
#For eg: when the main outer loop (such as the part below) is looping over the different flanker spacing, the inner loop would test/QUEST 
# over the different letter sizes for that flanker spacing.
for aflanker in flanker_space:
    #The flanker spacing is kept constant and stimulus size is adjusted in the QUEST paradigm
    # QUEST parameters are set at this point which would then be used in a loop
    # to run the staircase method
#    starting_letter_size=(params[eccentricity_deg])
    starting_letter_size=0.125
    log_letter_size=np.log10(starting_letter_size)
    staircase = data.QuestHandler(log_letter_size, 0.5, pThreshold=0.75, gamma=0.5,beta=3.5,nTrials=32
                               ,minVal=-1.75) #The minval of 1.75 corresponds to 1 arc min on the external display on a log scale
            
        
    # We set the output file parameters here and write the header line for the same
    outfilename = ("results/S0%d_S%d_T%03d_ecc_%d_CrowdingQuest_%s.csv" % ((subj_id),(session_num),(trial_num), eccentricity_deg,
                    time.strftime("%d%m%Y", time.localtime() ) ) )
    threshold_filename=("results/S0%d_S%d_T%03d_CrowdingThresh.csv" % ((subj_id),(session_num),(trial_num)) )
    timerDuration=1
    
    #Add the header row 
    if path.exists(outfilename):
        outfile = open(outfilename, "a")
    else:
        outfile = open(outfilename, "a")
        outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,% s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc","fix_dur",
                    "cue_condition","ISI","stim_dur","RT","corr","ntrial") )
                    
    if path.exists(threshold_filename):
        thresh_file=open(threshold_filename,"a")
    else:
        thresh_file=open(threshold_filename,"a")
        thresh_file.write("%s,%s"%("ecc","size\n"))
           
    #Draw a fixation cross and wait for the start of the experiment
    keepGoing=True
    while keepGoing:
        for stim1 in [fix_v, fix_h, fix_c]:
            stim1.draw()

        wait_msg.draw()
        window.flip()
        keys=event.getKeys()

        if len(keys)>0:
            keepGoing=False
    
    for repeat in range(num_repeats):
        #This is where the QUEST method begins
        #set a timer so that the program waits for video to finish
        timer = core.CountdownTimer(timerDuration)
        #We start trial counter here
        questCounter=0
        for letter_height in staircase:
            # We adjust the letter_height to be on a log scale to improve performance of the quest method
            letter_height = 10**letter_height
            
            # we initially test if the trial number/count is higher or equal to the upper limit
            questCounter+=1
            
            #An escape key press here terminates the loop
            if keys[0]=='escape':
                runExpt=False
                break
            # The following set of lines set the flanker condition 
            # Additionally the horizontal position of the target is randomized
            eccentricity_pix=eccentricity_deg*pixel_per_degree
            stim_location=np.random.choice(stim_location_list)
            
            if stim_location=='left':
                eccentricity=fixation_pos[0]-eccentricity_pix
            elif stim_location=='right':
                eccentricity=fixation_pos[0]+eccentricity_pix
            
            #We also have a condition where the target can be presented at the foveally
            # replacing the fixation cross
            if foveal_mode==True:
                target_location=(fix_h.start[0],0)
            else:
                target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
            target.pos=target_location
            if target_type=='bars':
                target_type_list=['h','v']*3
            elif target_type=='T':
                target_type_list=['u','d','l','r']
                # Since we need five different stimuli we randomly add a fifth one
                target_type_list.append(random.choice(target_type_list))
            elif target_type=='numbers':
                target_type_list=['0','1','2','3','4','5','6','7','8','9']
            elif target_type=='E':
                target_type_list=['E','3','e','f']
                # Since we need five different stimuli we randomly add a fifth one
                target_type_list.append(random.choice(target_type_list))
            random.shuffle(target_type_list)
            target_let=target_type_list[0]
            flanker_let_1=target_type_list[1]
            flanker_let_2=target_type_list[2]
            flanker_let_3=target_type_list[3]
            flanker_let_4=target_type_list[4]


            #The letter size is set on each run by the QUEST method 
            letter_size_deg=letter_height
            letter_size_pix=letter_size_deg*pixel_per_degree
            target.height=letter_size_pix
            flanker_left.height=letter_size_pix
            flanker_right.height=letter_size_pix
            flanker_up.height=letter_size_pix
            flanker_down.height=letter_size_pix
            
            #THe flanker spacing is set on eacn trial based on the stimulus size
            flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
            flanker_left.pos=(target_location[0]-flanker_spacing_pix,0)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix,0)
            flanker_up.pos=(target_location[0],flanker_spacing_pix)
            flanker_down.pos=(target_location[0],-flanker_spacing_pix)
            
            
            # we then set the respective target and flanker characters
            target.text=target_let
            flanker_left.text=flanker_let_1
            flanker_right.text=flanker_let_2
            flanker_up.text=flanker_let_3
            flanker_down.text=flanker_let_4
            
            # we can also switch the contrast between the target and flankers
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]
            # MOCS: to prevent the same mask from being used on every trial we shuffle the order of masks
            # to switch between the masks used for target and flankers
            masks=[mask_target,mask_inner_flanker,mask_outer_flanker,mask_up_flanker,mask_lower_flanker]
            random.shuffle(masks)
            target_mask=masks[0]
            left_flanker_mask=masks[1]
            right_flanker_mask=masks[2]
            up_flanker_mask=masks[3]
            lower_flanker_mask=masks[4]
            
            target_mask.pos=target_location #we center the mask with the stimulus
            left_flanker_mask.pos=flanker_left.pos
            right_flanker_mask.pos=flanker_right.pos
            up_flanker_mask.pos=flanker_up.pos
            lower_flanker_mask.pos=flanker_down.pos

            # The following set of code is used to present the stimulus & cue at pre-determined
            # locations and duration
            # The experimental sequence is as follows:
            # 1. Fixation cross for 450 msec
            # 2. Cue presentation for 50 msec (if cue_condition is set to True)
            # 3. Variable delay b/w 750-2000 msec 
            # 4. Stimulus presentation for 100 msec
            # 5. Post-mask for 200 msec
            # 6. Response window following mask offset and reaction time is measured in relation 
            #       to stimulus onset

            # the fixation period before each trial is set to a constant value
            clock.reset()
            
            for frameN in range(int(refreshRate*fixation_duration)):
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()  
                # We capture fixation target and save to image file if needed
                if save_image==True:
                    window.getMovieFrame(buffer='back')
                window.flip()

            #The following set of lines is used to present the cue (neutral/valid) cue as a fraction
            # of the frame rate of the display
            for frameN in range(int(refreshRate*cue_presentation)):
                if cue_condition==True:
                    if stim_location=='left':
                        for cue1 in [cue_target_h,cue_target_v_left1,cue_target_v_left2]:
                            cue1.draw()
                    elif stim_location=='right':
                         for cue1 in [cue_target_h,cue_target_v_right1,cue_target_v_right2]:
                            cue1.draw() 
                    
                    # We capture the cue buffer and save to image file if needed
                    if save_image==True :
                        window.getMovieFrame(buffer='back')
                        
                    window.flip()
                else:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()  
                    window.flip()

            #we wait for a pseudo-random time before stimulus onset
            #to prevent anticipation
            clock.reset()
            ISI=np.random.choice(timing_list)
            while clock.getTime()< ISI:
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()            
                window.flip()
            
                
            # The stimulus duration is used as a fraction of the frame rate of the display;
            # THis ensures better timing of the target presentations
            #we also the reset the kb clock to the reaction time in relation to stimulus onset
            kb_timer = core.CountdownTimer(timerDuration)
            # we clear key events that could occured after the end of the previous trial 
            event.clearEvents()
            target.render()
            flanker_right.render()
            flanker_left.render()
            flanker_up.render()
            flanker_down.render()
            for frameN in range(int(refreshRate*target_presentation)):
                target.draw()                
                # we test whether the stimulus would appear on the left/right and based
                # on that we set the inner and outer flankers
                if stim_location=='left':                    
                    inner_flanker=flanker_right
                    outer_flanker=flanker_left
                elif stim_location=='right':
                    inner_flanker=flanker_left
                    outer_flanker=flanker_right
                # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
                if inner_flanker==True:
                    inner_flanker.draw()
                outer_flanker.draw()
                flanker_up.draw()
                flanker_down.draw()
                                                   
                # We only draw the fixation cross for the peripheral condition and not for the foveal one
                if foveal_mode==False:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                # we capture the stimulus on the buffer before adding in the square marker
                # This can later be saved if needed
                if save_image==True:
                    window.getMovieFrame(buffer='back')

                #Draw the small square for Photodiode to detect and to create a marker in the video
                small_square.draw()

                flip_time=window.flip()
            
            # Followin the stimulus offset we have a post-mask presented immediately after the same
            mask_isi=np.random.uniform(0.400,0.500)
            if mask_condition==True:
                for frameN in range(int(refreshRate*mask_isi)):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw() 
                    window.flip()
                for frameN in range(int(refreshRate*mask_duration)):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    # we would set the inner and outer flanker masks based on the position of the target/stimulus
                    if stim_location=='left':
                        inner_flanker_mask=right_flanker_mask
                        outer_flanker_mask=left_flanker_mask
                    elif stim_location=='right':
                        inner_flanker_mask=left_flanker_mask
                        outer_flanker_mask=right_flanker_mask
                    # We would draw the inner flanker mask only when the inner flanker is also used otherwise we would skip this
                    if inner_flanker==True:
                        inner_flanker_mask.draw()
                    # we would draw separate masks for target and flankers
                    target_mask.draw()
                    outer_flanker_mask.draw()
                    up_flanker_mask.draw()
                    lower_flanker_mask.draw()
                    # we capture the mask buffer and save to image file when needed
                    if save_image==True:
                        window.getMovieFrame(buffer='back')

                    window.flip()
            #Use this to save the stimulus image
            if save_image==True:
                window.saveMovieFrames('Stimulus_'+str(np.abs(eccentricity_deg))+'_'+str(flanker_deg)+'_'+str(stim_location)+'.jpg')

            
            # we wait for the behavioral response from the subject. The waiting duration is set at the start of the experiment
            # The duration is mainly to ensure that the timing of each trial is pre-determined and can be used to estimate the overall
            # duration of the corresponding retinal video recording on the TSLO machine
            reaction_timer = core.CountdownTimer(response_window)
            keepGoing=True
            while reaction_timer.getTime()>0 and keepGoing==True:
                response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                response_wait.draw()
                
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                
                flip_time=window.flip()

                key=event.getKeys()
                if len(key)>0:
                    response_time=kb_timer.getTime()
                    keepGoing=False
                else:
                    response_time=timerDuration
                    
            # we check if there was a keypress made duration the response window otherwise we set a defualt value
            if 'key' not in locals():
                key_pressed='Nil' # arbitary value 
            elif len(key)==0:
                key_pressed='Nil'
            else:
                key_pressed=key[0]
                
            #An escape key press here terminates the loop
            if key_pressed=='escape':
                break
                
            #we also store the reaction time
            RT=timerDuration-response_time
            # we assign a response value based on the keypress that was made by the subject
            if target_type=='numbers':
                if key_pressed=='num_0':
                    response_key='0'
                elif key_pressed=='num_1':
                    response_key='1'
                elif key_pressed=='num_2':
                    response_key='2'
                elif key_pressed=='num_3':
                    response_key='3'
                elif key_pressed=='num_4':
                    response_key='4'
                elif key_pressed=='num_5':
                    response_key='5'
                elif key_pressed=='num_6':
                    response_key='6'
                elif key_pressed=='num_7':
                    response_key='7'
                elif key_pressed=='num_8':
                    response_key='8'
                elif key_pressed=='num_9':
                    response_key='9'
                else:
                    response_key='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
            elif target_type=='bars':
                if key_pressed=='left' or key_pressed=='right' or key_pressed=='num_4' or key_pressed=='num_6':
                    response_key='h'
                elif key_pressed=='up' or key_pressed=='down' or key_pressed=='num_8' or key_pressed=='num_2':
                    response_key='v'
                else:
                    response_key='100'
            elif target_type=='T':
                if key_pressed=='up' or key_pressed=='num_8':
                    response_key='u'
                elif key_pressed=='down' or key_pressed=='num_2':
                    response_key='d'
                elif key_pressed=='left' or key_pressed=='num_4':
                    response_key='l'
                elif key_pressed=='right' or key_pressed=='num_6':
                    response_key='r'
                else:
                    response_key='100'
            elif target_type=='E':
                if key_pressed=='right':
                    response_key='E'
                elif key_pressed=='left':
                    response_key='3'
                elif key_pressed=='up':
                    response_key='e' # need to verify this
                elif key_pressed=='down':
                    response_key='f'
                else:
                    response_key='100'

            # we would provide feedback based on whether or not the response was correct
            if response_key==target.text:
                beep_sound_correct.play()
                corr=1
            else:
                beep_sound_incorrect.play()
                corr=0
                
            # we write the responses to the output file     
            if key_pressed!='escape':
                outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%d\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                letter_size_deg,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT, 
                                corr,questCounter) )

            #inform QUEST of the response, needed to calculate next level
            staircase.addResponse(corr)
        
        outfile.close()
        # we get the threshold from the quest method and write the results to a separate file
        s=10**(staircase.mean()) #mean threshold from staircase method
        print('The threshold is %2f'%(s))
        thresh_file.write("%d,%2f\n"%(eccentricity_deg,s))
        thresh_file.close()
        # The program waits here for until the recording duration finishes
        # Note: We would generally have the TSLO record the retinal videos for a little bit longer after
        # the presentation of the last stimulus so as to ensure that the recording does not stop abruptly 
        # If the subjects finish sooner than the end of the video recording, the program would wait at this point
        # and provide a fixation target for the subjects to fixate upon during this time.
        while timer.getTime()>0:
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            wait_msg2.draw()
            window.flip()
        
    
#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

window.close()
core.quit()