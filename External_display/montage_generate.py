from psychopy import visual, core, monitors,event
import os
import numpy as np
import time
import psychopy.gui
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
from psychopy import logging
logging.console.setLevel(logging.CRITICAL)
import matplotlib.pyplot as plt
import numpy as np
import operator
from PIL import Image
from PIL import ImageDraw
import pandas as pd

#image_dir=psychopy.gui.fileOpenDlg(tryFilePath='', tryFileName='', prompt='Select file to open', allowed=None)
image_dir=r'E:\TSLO Experiment KSP\KSP Montage\KSP_montage_good\montage videos'
#image_dir=r'C:\Users\C-LAB\projects\tslo_core\Sample Code\External display\Retinal images'
#
retina_images= []
image_filenames=[]
for file in os.listdir(image_dir):
    if file.lower().endswith(".png"):
        path=os.path.join(image_dir,file)
        retina_images.append(path)
        image_filenames.append(file)
    if str(file)=='image_offset.txt':
        path=os.path.join(image_dir,file)
        offset_df=pd.read_csv(path,delimiter='\t',header=None)
        x_pos_list=offset_df.iloc[:,0]
        y_pos_list=offset_df.iloc[:,1]
        image_size_list=offset_df.iloc[:,2]
        image_orientation_list=offset_df.iloc[:,3]
        

if 'x_pos_list' not in locals():
    offset=300
    x_pos_list=[0,-offset,offset,0,-offset,offset,0,-offset,offset] #need to fix sequence
    y_pos_list=[0,0,0,offset,offset,offset,-offset,-offset,-offset]
    image_size_list=[512 for n in range(len(x_pos_list))]
    image_orientation_list=[0 for n in range(len(x_pos_list))]


monsize =  [1920, 1080]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[-1,-1,-1], screen=0)
text_ypos=np.linspace(-350,-512,num=9)


images=[visual.ImageStim(window,image=retina_images[n],size=(image_size_list[n],image_size_list[n]),units='pix',ori=image_orientation_list[n],pos=(x_pos_list[n],y_pos_list[n]),opacity=0.5) for n in range(len(retina_images))]


for img_idx in range(len(retina_images)):
    image_width=image_size_list[img_idx]
    image_height=image_size_list[img_idx]
    image_orientation=image_orientation_list[img_idx]
    posx=x_pos_list[img_idx]
    posy=y_pos_list[img_idx]
#    retinal_file=retina_images[img_idx]
    filename_text=visual.TextStim(window,text=str(image_filenames[img_idx][:-4]),pos=(750,text_ypos[img_idx]))
#    image=visual.ImageStim(window,image=retinal_file,size=(image_width,image_height),units='pix',ori=image_orientation,pos=(posx,posy))
    filename_text.draw()
    keys=[]
    images[img_idx].opacity=0.5

    
    while 'num_5' not in keys:
        images[img_idx].pos=(posx,posy)
        filename_text.draw()
        for image1 in images[::-1]:
            image1.draw()
        images[img_idx].draw()
        window.flip()
        
        keys=event.waitKeys()
        if len(keys)>0:
            if (keys[len(keys)-1] == 'num_3'):
                image_width+=10
                image_height+=10
                images[img_idx].size=(image_width,image_height)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_1'):
                image_width-=10
                image_height-=10
                images[img_idx].size=(image_width,image_height)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_9'):
                image_orientation+=1
                images[img_idx].ori=image_orientation
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_7'):
                image_orientation-=1
                images[img_idx].ori=image_orientation
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_4'):
                posx-=5
                images[img_idx].pos=(posx,posy)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_6'):
                posx+=5
                images[img_idx].pos=(posx,posy)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_2'):
                posy-=5
                images[img_idx].pos=(posx,posy)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_8'):
                posy+=5                 
                images[img_idx].pos=(posx,posy)
                event.clearEvents()
            if (keys[len(keys)-1] == 'num_0'):
                if images[img_idx].opacity==0.5:
                    images[img_idx].opacity=1
                elif images[img_idx].opacity==1:
                    images[img_idx].opacity=0.2
                else:
                    images[img_idx].opacity=0.5
            if (keys[len(keys)-1] == 'm'):
                if img_idx+1!=len(retina_images):
                    if images[img_idx+1].opacity==0.5:
                        images[img_idx+1].opacity=1
                    else:
                        images[img_idx+1].opacity=0.5
            if (keys[len(keys)-1]=='n'):
                if images[img_idx-1].opacity==0.5:
                    images[img_idx-1].opacity=1
                else:
                    images[img_idx-1].opacity=0.5
                    
        
        x_pos_list[img_idx]=posx
        y_pos_list[img_idx]=posy
        image_size_list[img_idx]=image_height
        image_orientation_list[img_idx]=image_orientation

        #window.flip(clearBuffer=False)
        
    if img_idx==0:
        images[img_idx].opacity=1
    else:
        images[img_idx].opacity=0.2

#once we get the X and Y displacement for each image we reconstruct that and save the image
images=[visual.ImageStim(window,image=retina_images[n],size=(image_size_list[n],image_size_list[n]),units='pix',ori=image_orientation_list[n],pos=(x_pos_list[n],y_pos_list[n]),opacity=1,mask='raisedCos', maskParams={'fringeWidth':0.2}) for n in range(len(retina_images))]
for idx in range(len(retina_images)):
    for image1 in images[::-1]:
            image1.draw()
#    images[idx].draw()
window.flip()
window.getMovieFrame(buffer='front')
os.chdir(image_dir)
offset_file=open('image_offset.txt','w')
for pos_idx in range(len(x_pos_list)):
    offset_file.write('%d\t%d\t%d\t%d\n'%(x_pos_list[pos_idx],y_pos_list[pos_idx],image_size_list[pos_idx],image_orientation_list[pos_idx]) ) 
offset_file.close()
window.saveMovieFrames('montage_img.tif')
window.close()
core.quit()

for nidx,img in enumerate(images):
    print ( img.pos ) 
