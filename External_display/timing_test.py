from psychopy import visual, core, monitors,event
import os
import numpy as np
import time

#display options
#1."4k"
#2. "1080"
disp='4k'
if disp=='4k':
    monsize =  [2160, 3840]
    patch_pos=(975,1625)
else:
    monsize=[1920,1080]
    patch_pos=(900,500)
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
white_patch=visual.Rect(window,width=128,height=128,units='pix',fillColor='white', lineColor='white',fillColorSpace='rgb',pos=patch_pos)
black_patch=visual.Rect(window,width=128,height=128,units='pix',fillColor='black', lineColor='black',fillColorSpace='rgb',pos=patch_pos)
#Second patch for latency test
second_patch_pos=(-900,1625)
second_patch_white=visual.Rect(window,width=2000,height=1024,units='pix',fillColor='white', lineColor='white',fillColorSpace='rgb',pos=second_patch_pos)
second_patch_black=visual.Rect(window,width=2000,height=1024,units='pix',fillColor='black', lineColor='black',fillColorSpace='rgb',pos=second_patch_pos)
refreshRate=window.getActualFrameRate()
squareFlicker=False
window.recordFrameIntervals=False
flicker_frequency = 60
current_frame = 0
keepGoing=True

#flicker parameters are set here
#Regular flicker Params
if squareFlicker==True:
    flicker_frequency = 1 #Number of frames with/without stimulus
    current_frame = 0
else:
    stim_dur=0.033 #second
    blank_dur=0.500#second
    
while keepGoing:
    if squareFlicker==True:
        # When to draw  stimuli
         if current_frame % (2*flicker_frequency) < flicker_frequency:
            white_patch.draw()

         # Show whatever has been drawn. Sometimes the stimuli, other times a blank screen. 
         # flip() waits for the next monitor update so the loop is time-locked to the screen here.
         window.flip()
         current_frame += 1  # increment by 1.
    
    else:    
        for frameN in range(int(refreshRate*stim_dur)):
            white_patch.draw()
            second_patch_white.draw()
            window.flip()
            
        for frameN in range(int(refreshRate*blank_dur)):
            second_patch_black.draw()
            black_patch.draw()
            window.flip()
     
    keys=event.getKeys()
     
    if len(keys)>0:
        keepGoing=False
window.close()
