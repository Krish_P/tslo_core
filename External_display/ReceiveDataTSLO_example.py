from ReceiveDataFunction import stimMachineRecv
from pylsl import StreamInlet, resolve_stream
from psychopy import core
import time

streams = resolve_stream('type', 'Server')

# first resolve an TSLO stream on the lab network
streams = resolve_stream('type', 'Server')

# create a new inlet to read from the stream
inlet = StreamInlet(streams[0],max_buflen=1)
inlet.open_stream()

# core.wait(1)
while True:
    sample1=stimMachineRecv(streams,inlet,commSetup=False,timed=False)
    sample2=stimMachineRecv(streams,inlet,commSetup=False,timed=False)
    print(sample2)

    if sample1!=None and sample2!=None:
        break

#inlet.close_stream()