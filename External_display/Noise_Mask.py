from psychopy import gui, visual, core, data, event, logging, clock 
import numpy as np
import os  # handy system and path functions
import sys 

window = visual.Window(size=(900, 900),units='pix',fullscr=False,allowGUI=True,color=[0,0,0], screen=1)

mask_array=np.random.randint(2,size=(512,512))

mask=visual.ImageStim(window,image=mask_array,size=(100,100),units='pix',colorSpace='rgb')
#grating = visual.GratingStim(window,units='pix',pos=(0,0), color=[-1,-1,-1] ,size=100,sf=0.02)

mask.draw()
window.flip()
core.wait(1.0)

# close the window
window.close()