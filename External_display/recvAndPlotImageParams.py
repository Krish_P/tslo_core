# Purpose: This uses LSL to receive image parameters (mean & SD) from ICANDI and plots
# inside psychopy [mainly for solo-piloting]
from scipy.stats import norm
from ReceiveDataFunction import ICANDIreceiver
import matplotlib.pyplot as plt
import numpy as np
from psychopy import visual, core, monitors,event
import cv2
import os

def recvAndPlotImageParams(window,disp_val=True,plot_dist=True):
    # we get values & return
    imageParams=ICANDIreceiver()
    imgMean=imageParams[0]
    imgSD=imageParams[1]
    x_axis=np.arange(0,200,1) # scale for plot
    params_msg="M: %d; SD:%d"%(imgMean,imgSD)
    if disp_val==True:
        # Draw basic shapes which will be draw later in the loop
        fix_xpos=0
        step_size=20
        fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
        fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
        fix_c=visual.Circle(window, size=15, fillColor=(1,1,1), pos=(0,0), lineColor=(0,0,0) )
        if imgMean>0:
            if plot_dist==True:
                plt.figure()
                plt.plot(x_axis, norm.pdf(x_axis, imgMean, imgSD),color='k')
                plt.xlim(0,200)
                plt.xticks(fontsize=14)
                plt.yticks(fontsize=14)
                plt.title(params_msg,fontsize=14)
                imageFilename='imageParams.png'
                if os.path.isfile(imageFilename):
                    os.remove(imageFilename)   # Opt.: os.system("rm "+strFile)
                plt.savefig(imageFilename)
                # then show image in psychopy
                image=visual.ImageStim(win=window,pos=[0,0],image='imageParams.png',size=(500,500),opacity=1.0,flipHoriz=False, flipVert=False,ori=0)
                image.draw()
                # we remove the file
                os.remove(imageFilename)
            else:
                image_params_msg=visual.TextStim(win=window,pos=(0,100),height=50,text=params_msg)
                image_params_msg.draw()

            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()

            window.flip()
        else:
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            if plot_dist==False:
                params_msg="M: %d; SD:%d"%(imgMean,imgSD)
                image_params_msg=visual.TextStim(win=window,pos=(0,100),height=50,text=params_msg)
                image_params_msg.draw()

            window.flip()

    return imgMean,imgSD

if __name__=="__main__":
    horz_res=2160
    vert_res=3840
    disp_val=False
    if disp_val==True:
        # We would use image params from LSL to indicate quality
        monsize=[horz_res,vert_res]
        window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)
        window.setMouseVisible(False)
        while True:
            mean,sd=recvAndPlotImageParams(window,disp_val=True,plot_dist=False)
            #break when we get a keypress
            keys=event.getKeys()
            
            if len(keys) != 0:
                cv2.destroyAllWindows()
                break
    else:
        mean,SD=recvAndPlotImageParams('dummy',disp_val=False,plot_dist=False)
        print(mean,SD)
