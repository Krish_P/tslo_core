"""Example program to show how to read a multi-channel time series from LSL."""

from pylsl import StreamInlet, resolve_stream


def TSLOreceiver():
    # first resolve an TSLO stream on the lab network
    print("looking for an TSLO stream...")
    streams = resolve_stream('type', 'TSLO')
    
    # create a new inlet to read from the stream
    inlet = StreamInlet(streams[0])
    
    repeat=True
    while repeat:
        # get a new sample (you can also omit the timestamp part if you're not
        # interested in it)
        sample, timestamp = inlet.pull_sample()
        print(timestamp, sample)
        if len(sample)>0:
            repeat=False
            inlet.close_stream()
    return (sample)
        
def ICANDIreceiver():
    # first resolve an TSLO stream on the lab network
    streams = resolve_stream('type', 'ICANDI')
    
    # create a new inlet to read from the stream
    inlet = StreamInlet(streams[0])
    
    repeat=True
    while repeat:
        # get a new sample (you can also omit the timestamp part if you're not
        # interested in it)
        sample, timestamp = inlet.pull_sample()
        if len(sample)>0:
            repeat=False
            inlet.close_stream()
    return (sample)

def stimMachineRecv(stream_info,stream_inlet,commSetup=False,timed=False):
    if commSetup==True:
        # first resolve an TSLO stream on the lab network
        streams = resolve_stream('type', 'Server')
        
        # create a new inlet to read from the stream
        inlet = StreamInlet(streams[0])
    else:
        streams=stream_info
        inlet=stream_inlet
    
    repeat=True
    while repeat:
        # get a new sample (you can also omit the timestamp part if you're not
        # interested in it)
        try:
            if timed==True:
                sample, timestamp = inlet.pull_sample(timeout=0)
            else:
                sample, timestamp = inlet.pull_sample()
        except UnboundLocalError:
            print('Inlet not found!')
            continue
        if timed==False:
            if len(sample)>0:
                repeat=False
        else:
            repeat=False
        if commSetup==True:
            inlet.close_stream()
    return (sample)
        