from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import customTime
import random
from random import randint
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from PixelConversion import PixelConversion
import pixlet
import online_sac_detect_module
import serial 
from SendDataFunction import TSLOsender 
from ReceiveDataFunction import TSLOreceiver
from multiprocessing.connection import Listener
from multiprocessing.connection import Client
from make_mask import make_mask  
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
eye_tracker=True
lsl_connection=True
keep_repeating=True
cam_connection=True
#===================================================================================================================================
## First we set up the parameters to set up hardware

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
    
if eye_tracker==True:
    ## Setup listener
    # This would listen to saccade messages
    # Socket connection setup
    listenAddress=('localhost',6000)
     # family is deduced to be 'AF_INET'
    print('Looking for connection')
    listener = Listener(listenAddress, authkey=b'secret password')
    connListener = listener.accept()
    ## Setup Sender
    # This would send stimulus time and final close message to terminate all sockets
    # Set up the parameters for the socket
    sendAddress = ('localhost', 6001)
    while True:
        try:
            connSender = Client(sendAddress, authkey=b'secret password')
            break
        except ConnectionRefusedError:
            time.sleep(0.1)
            continue
    print('Connection established!')

# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
msg_size=pixel_per_degree/5
msg_offset=400
# Specify cue offset as a multiple of number of pixels per degree 
peripheral_cue=True
# Fixation target could be either cross or plus with circle in the middle
# Options: 1: "x" or 2" "+"
fixation_type='x'

if fixation_type=="+":
    fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
elif fixation_type=='x':
    fix_line_UR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    # For oblique targets presented at 1 of 4 locations we have separate cues
    target_c_ul=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_ur=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_ll=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_lr=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
fix_c=visual.Circle(window, size=4, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

# Additionally,we set up the camera feature,so that subjects can visualize the retinal image quality and the PMT gain setting.
# For this to work the webcam would have to be setup such that it faces the retinal image window on ICANDI and the PMT box with the 
# gain setting
if cam_connection==True:
    from ximea import xiapi
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[1000,1000],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()
#===================================================================================================================================
while keep_repeating==True:
    #Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            startup_msg1=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg1.draw()
            startup_msg2.draw()
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                    stim1.draw()
            window.flip()

            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    webcam_state=True                
                    #start data acquisition
                    cam.start_acquisition()
                    while webcam_state==True & cam_connection==True:
                        #get data and pass them from camera to img
                        cam.get_image(img)
                        #create numpy array with data from camera. Dimensions of the array are 
                        #determined by imgdataformat
                        camData = img.get_image_data_numpy()
                        #show acquired image 
                        newdata=np.subtract(np.divide(camData,128.0),1.0)
                        cam_image.setImage(newdata)
                        cam_image.draw()
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                stim1.draw()# overlays fixation cross
                        window.flip()
                        theKey = event.getKeys()
                        if len(theKey)!=0:
                            webcam_state=False
                            #stop data acquisition
                            cam.stop_acquisition()

    # we stop if escape is pressed
    if 'keys' in locals() and lsl_connection==True:
        if keys[0]=='escape':
            keep_repeating=False
            continue
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        protocol_packet=TSLOreceiver()
        # These parameters are provided from TSLO machine
        subj_id=protocol_packet[0]
        session_num=protocol_packet[1]
        num_repeats=int(protocol_packet[2])
        
    else: 
        #no_microsacc_cond=1 # Sets whether it should stop with or w/o microsaccades
        subj_id=1
        session_num=1
        num_repeats=10 # Sets the number of repeats for each flanker spacing at a given ecc

    # These parameters are set locally (Stimulus Machine)
    cue_condition=True
    mask_condition=False
    location_uncertainity=False 
    inner_flanker=False
    reverse_flanker_contrast=False
    save_image=False 
    foveal_mode=False
    behav_output=True
    eccentricity_deg=0.33
    # For obliqe locations we would have to adjust eccentricity to make sure its at the distance
    # specified
    if fixation_type=='x':
        eccentricity_deg=np.sqrt(eccentricity_deg**2/2)
        eccentricity_pix=int(eccentricity_deg*pixel_per_degree)

    # we create a list that would grow after each trial indicating the saccadic latency
    sacc_latency_list=[]

    #===================================================================================================================================
    ## Create basic shapes
    # These would be drawn & /or manipulated during the main experiment
    # The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
    if location_uncertainity==True:
        stim_location_list=['left','right']
    else:
        stim_location_list=['right']
    fix_xpos=0
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line

    #Create basic stimuli here that could be called multiple times during the course of the experiment
    small_square=visual.Rect(window,width=128,height=128,units='pix',fillColor='white', fillColorSpace='rgb',pos=(975,1625))
    wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start the experiment')
    wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

    # The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
    cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    # Cue for rightward target location
    cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    #cue for leftward target location
    cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    # Cue for voluntary microsaccade
    cue_c=visual.Circle(window, size=2, fillColor=(0,0,0), pos=(0,0), lineColor=(0,0,0) )
    # for X fixation cross we can saccade to 1 of 4 possible locations
    if fixation_type=='x':
        # We set the position of the target placeholder using the eccentricity provided
        # and the location of the fixation cross
        target_c_ul.pos=((-eccentricity_pix),(+eccentricity_pix))
        target_c_ur.pos=((+eccentricity_pix),(eccentricity_pix))
        target_c_ll.pos=((-eccentricity_pix),(-eccentricity_pix))
        target_c_lr.pos=((+eccentricity_pix),(-eccentricity_pix))
        #Possible cue locations
        cue_locs_list=['upperLeft','upperRight','lowerLeft','lowerRight']
    #===================================================================================================================================
    ## Experimental sequence:

    #We use the position of the fixation cross center to position the target and flankers
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 

    # The following set of lines set the flanker condition 
    eccentricity_pix=int(eccentricity_deg*pixel_per_degree)
    eccentricity=fixation_pos[0]+eccentricity_pix
    if foveal_mode==True:
        target_location=(fix_h.start[0],0)
    else:
        target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
    cue_c.pos=target_location
                                  
    #This is used to provide a audio feedback to the user
    beep_sound_correct=Sound(value=1000,secs=0.25)
    beep_sound_incorrect=Sound(value=250,secs=0.25)
       

    #===================================================================================================================================
    ## Start of main loop of experiment
    # We would get this from the TSLO machine and would help number the files 
    # consistently across both machines
    repeatCounterStart=1

    # 1. We create the behavioral output file
    # We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
    # accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
    # data
    #output filename
    file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())
    if lsl_connection==True:
        trial_info='S0%d_S%d_ecc_%.2f'%(subj_id,session_num, eccentricity_deg)
        outfilename = ("results/%s_%s.csv" % (trial_info,file_timestamp) ) 
        timerDuration=protocol_packet[3]
    else:
        timerDuration=2 
        trial_info='S0%d_S%d_ecc_%.2f'%(subj_id,0,eccentricity_deg)
        outfilename = ("results/%s_%s.csv" % (trial_info,file_timestamp ) )
    if behav_output==True:
        outfile = open(outfilename, "a")


        
    if behav_output==True:
        #Add the header 
        outfile.write("%s,%s\n"%("trial_counter","saccade_latency") )

    event.clearEvents()

    for arepeat in range(num_repeats):
        if eye_tracker==True:
            # We send the trial details to the tracker/saccade detection method
            connSender.send(trial_info)
            # Also send a message to indicate the start of the sequence/stream. This 
            # would be used to store data and write to log file
            connSender.send("trialStart")
            # we get the starting time stamp in ms and this can be used to get relative
            # time stamp for stimulus onset
            startTime=customTime.millis()
            
        # First we would reset fixation cross to original color
        fix_line_UL.color=[-1,-1,-1]
        fix_line_UR.color=[-1,-1,-1]
        fix_line_LL.color=[-1,-1,-1]
        fix_line_LR.color=[-1,-1,-1]
        #Have something that waits for a key press from the subject. This would send the flanker spacing condition to the TSLO system.
        # We change the color of the central circle of the fixation cross
        # We only have this (cue) for the microsaccade condition other wise we set the color to be the same
        if fixation_type=='+':
            fix_c.fillColor=[0,1,0]
            fix_c.lineColor=[0,1,0]
            cue_c.fillColor=[0,1,0]
            cue_c.lineColor=[0,1,0]
        elif fixation_type=='x':
            cueLocation=np.random.choice(cue_locs_list)
            fix_c.fillColor=[0,1,0]
            fix_c.lineColor=[0,1,0]
            # First we would reset fixation cross to original color
            if cueLocation=='upperLeft':
                fix_line_UL.color=[0,1,0]
            elif cueLocation=='upperRight':
                fix_line_UR.color=[0,1,0]
            elif cueLocation=='lowerLeft':
                fix_line_LL.color=[0,1,0]
            elif cueLocation=='lowerRight':
                fix_line_LR.color=[0,1,0]

        if lsl_connection==True:
            keepGoing=True
            while keepGoing:
                trialStart1=visual.TextStim(window,text=('Waiting for Imageing system'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                trialStart2=visual.TextStim(window,text=('Press any key to start trial'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)

                trialStart1.draw()
                trialStart2.draw()
                    
                if peripheral_cue==True:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                else:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                if mask_condition==True:
                    # we draw the mask
                    mask_object.draw()
                window.flip()
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    keepGoing=False
                elif len(keys)>0:
                    if keys[0]=='space':
                        webcam_state=True                
                        #start data acquisition
                        cam.start_acquisition()
                        while webcam_state==True and cam_connection==True:
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw() # overlays the fixation cross
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()
            #TSLO sender keeps sending packets until it is received by the TSLO machine. The second message here
            # is a dummy message
            TSLOsender(repeatCounterStart,0)
        else:
            keepGoing=True
            while keepGoing:
                wait_msg.draw()
              
                if peripheral_cue==True:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                else:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                #also draw peripheral saccade cue
                window.flip()
                    
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    keepGoing=False
                elif len(keys)>0:
                    if keys[0]=='space':
                        webcam_state=True                
                        while webcam_state==True and cam_connection==True:
                            #start data acquisition
                            cam.start_acquisition()
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw() # overlays the fixation cross
                            if mask_condition==True:
                                # we draw a mask that would cover both the stimulus and flankers
                                mask_object.draw()
                            #also draw peripheral saccade cue
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()
        if keys[0]=='escape':
            keep_repeating=False
            break
            

        # we add a delay on the very first trial so each video is padded to prevent 
        # any stimuli from being missed from frame grab/recording
        # We would 
        initialDelay=np.random.choice(np.arange(0.750,1.250,0.100))
        if lsl_connection==True:
            for nflips in range(int(refreshRate*initialDelay)):
                if peripheral_cue==True:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                else:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                wait_msg2.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                #also draw peripheral saccade cue
                window.flip()
                # throw away any saccade messages
                if eye_tracker==True:
                    status=connListener.poll()
                    if status==True:
                        clear_msg=connListener.recv()
        else:
            for nflips in range(int(refreshRate*initialDelay)):
                if peripheral_cue==True:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                else:
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                wait_msg2.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                #also draw peripheral saccade cue
                window.flip()
                
        #set a timer to have psychopy wait until video is complete
        timer = core.CountdownTimer(timerDuration)
        # The cue would be an exogenous peripheral cue that would on for the whole trial 
        # The observer would only move their eye towards the cue when fixation cross 
        # goes away/ central circle changes color
        clock.reset()

        # we revert the color of the central circle from the fixation cross
        fix_c.fillColor=[0,0,0]
        fix_c.lineColor=[0,0,0]
        # we would reset fixation cross to original color
        fix_line_UL.color=[-1,-1,-1]
        fix_line_UR.color=[-1,-1,-1]
        fix_line_LL.color=[-1,-1,-1]
        fix_line_LR.color=[-1,-1,-1]
        # We have a place holder for saccade time stamp that would be changed when 
        # we get a saccade event
        sacc_cond=0
        sacc_latency=0
        sacc_timeStamp=0
        nflips=0
        while timer.getTime()>0:
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                    stim1.draw()
            if mask_condition==True:
                # we draw a mask that would cover both the stimulus and flankers
                mask_object.draw()
            #Draw the small square for Photodiode to detect and to create a marker in the video
            if nflips<2:
                small_square.draw()

            window.flip()
            # we record time stamp for when the cue came on
            if nflips==0:
                cueTime=customTime.millis()
            # Look for saccade messages
            if eye_tracker==True:
                status=connListener.poll()
                if status==True and sacc_timeStamp==0:
                    try:
                        # This occured durig stimulus presentation and we would
                        # not use it
                        got_one=connListener.recv()
                        sacc_cond= sacc_cond| got_one;
                        sacc_timeStamp=customTime.millis()
                    except EOFError:
                        print('receive function error')
            nflips+=1
        # We finally compute saccade latency
        if sacc_cond==1:
            sacc_latency=sacc_timeStamp-cueTime
            sacc_latency_list.append(sacc_latency)
        # We reset eye tracking protocol & drop trial marker to output file
        if lsl_connection==True:
            connSender.send("trialStop")
        if behav_output==True & sacc_cond==1:
            outfile.write("%d,%.2f\n"%(repeatCounterStart,sacc_latency))
        # increment repeat counter
        repeatCounterStart+=1
    # we clear key events that could occured after the end of the previous trial 
    event.clearEvents()

    #close the output file here
    if behav_output==True:
        outfile.close()   

        # We would print out a message for the subject and wait for them to hit a key before restarting the program
        loop_msg1=visual.TextStim(window,text='Block Complete!',pos=(0,0),height=msg_size)
        loop_msg2=visual.TextStim(window,text='Press any key to continue!',pos=(0,-150),height=msg_size)
        loop_msg1.draw()
        loop_msg2.draw()
        window.flip()
        event.waitKeys()

# Just to see the output
print(sacc_latency_list)

#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

# we close the different programs/processes that were opened
window.close()
core.quit()

if eye_tracker==True:
    # we close the socket program
    connListener.close()
    listener.close()
    connSender.close()
    print('connection closed')
 