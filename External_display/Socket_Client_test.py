import socket

def Main():

    host='192.168.0.143' #client ip
    port = 4005
    
    server = ('192.168.0.101', 4000)
    
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((host,port))
    s.setblocking(0)
    
    while True:
        try:
            message = 'Hello World'
            s.sendto(message.encode('utf-8'), server)
            (data,addr)=s.recvfrom(1024)
            data=data.decode('utf-8')
            print(data)
            if data=='q':
                break
        except BlockingIOError:
            pass
    s.close()

if __name__=='__main__':
    Main()