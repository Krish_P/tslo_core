"""Example program to demonstrate how to send a multi-channel time series to
LSL."""
## Purpose: To send trial status at the end of each iteration of the experiment 
#   sequence
import time
#from random import random as rand
from pylsl import StreamInfo, StreamOutlet
import warnings
warnings.filterwarnings("ignore")

def TSLOsender(TrialStatus,SequenceStatus):
    # first create a new stream info (here we set the name to BioSemi,
    # the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
    # last value would be the serial number of the device or some other more or
    # less locally unique identifier for the stream as far as available (you
    # could also omit it but interrupted connections wouldn't auto-recover)
    #KSP: params:Format [SubjectID,SessionNum,TrialNum,RecordingDuration]
    params=[TrialStatus,SequenceStatus]
    msg=('sending params...')
    info = StreamInfo('Clight', 'TSLO', 2, 100, 'float32', 'myuid34234')
    
    # next make an outlet
    outlet = StreamOutlet(info)
            
    counter=0
    
    repeat=True
    while repeat:
        if counter>1:
            repeat=False
        if outlet.have_consumers()==False:
            continue

        mysample = [params[0],params[1]]
        # now send it and wait for a bit
        outlet.push_sample(mysample)
        counter+=1
        time.sleep(0.01)

def TSLOsenderlong(msg1,msg2,msg3,msg4,msg5,msg6):
    # first create a new stream info (here we set the name to BioSemi,
    # the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
    # last value would be the serial number of the device or some other more or
    # less locally unique identifier for the stream as far as available (you
    # could also omit it but interrupted connections wouldn't auto-recover)
    #KSP: params:Format [SubjectID,SessionNum,TrialNum,RecordingDuration]
    params=[msg1,msg2,msg3,msg4,msg5,msg6]
    msg=('sending params...')
    info = StreamInfo('Clight', 'TSLO', 6, 100, 'float32', 'myuid34234')
    
    # next make an outlet
    outlet = StreamOutlet(info)
            
    counter=0
    
    repeat=True
    while repeat:
        if counter>1:
            repeat=False
        if outlet.have_consumers()==False:
            continue

        mysample = [params[0],params[1],params[2],params[3],params[4],params[5]]
        # now send it and wait for a bit
        outlet.push_sample(mysample)
        counter+=1
        time.sleep(0.01)

def stimMachineSender(smallMsg,stream_info,stream_outlet,commSetup=True):
     #first create a new stream info (here we set the name to BioSemi,
    # the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
    # last value would be the serial number of the device or some other more or
    # less locally unique identifier for the stream as far as available (you
    # could also omit it but interrupted connections wouldn't auto-recover)
    #KSP: params:Format [SubjectID,SessionNum,TrialNum,RecordingDuration]
    params=[smallMsg]
    msg=('sending info...')
    # We would set up the LSL comm on the first trial and would leave it open
    if commSetup==True:
        info = StreamInfo('ASUS', 'Server', 1, 100, 'string', 'myuid34234')
        
        # next make an outlet
        outlet = StreamOutlet(info)
    else:
        info=stream_info
        outlet=stream_outlet
            
    counter=0
    
    while True:
        if counter>1:
            break
        try:
            if outlet.have_consumers()==False:
                continue
        except UnboundLocalError:
            print('Outlet not found') 
            continue

        mysample = [params[0]]
        # now send it and wait for a bit
        outlet.push_sample(mysample)
        counter+=1
        time.sleep(0.01)