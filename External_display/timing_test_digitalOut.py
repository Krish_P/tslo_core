from psychopy import visual, core, monitors,event
import os
import numpy as np
import time
import nidaqmx
from psychopy import visual, logging
import parallel64
import customTime

#display options
#1."4k"
#2. "1080"
disp='4k'
if disp=='4k':
    monsize = [2160,3840]
    patch_pos=(0,0)
else:
    monsize=[1920,1080]
    patch_pos=(0,0)
# Display trigger options
# 1. Parallel port (server) : "Pport"
# 2. Digital out via NI card (old machine): "Dout"
trig_opt='Pport'

window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
# check for frame drops
window.recordFrameIntervals = True
window.refreshThreshold = 1/120 + 0.004
logging.console.setLevel(logging.WARNING)
#Create stimuli
patchSize=monsize
patch1=visual.Rect(window,width=patchSize[0],height=patchSize[1],units='pix',fillColor=[-1,-1,-1], lineColor='grey',fillColorSpace='rgb',pos=patch_pos)
patch2=visual.Rect(window,width=patchSize[0],height=patchSize[1],units='pix',fillColor=[1,1,1], lineColor='black',fillColorSpace='rgb',pos=patch_pos)
refreshRate=window.getActualFrameRate()
squareFlicker=False
window.recordFrameIntervals=False
flicker_frequency = 60
current_frame = 0
keepGoing=True

#flicker parameters are set here
#Regular flicker Params
if squareFlicker==True:
    flicker_frequency = 1 #Number of frames with/without stimulus
    current_frame = 1
else:
    stim_dur=0.050 #second
    blank_dur=1#second

if trig_opt=='Dout':
    with nidaqmx.Task() as task:
        task.do_channels.add_do_chan('Dev1/port0/line0')
        while keepGoing:
            if squareFlicker==True:
                # When to draw  stimuli
                if current_frame % (2*flicker_frequency) < flicker_frequency:
                    patch1.draw()
                else:
                    patch2.draw()
                
                # Show whatever has been drawn. Sometimes the stimuli, other times a blank screen. 
                # flip() waits for the next monitor update so the loop is time-locked to the screen here.
                window.flip()
                current_frame += 1  # increment by 1.
            
            else:    
                for frameN in range(int(refreshRate*stim_dur)):
                    patch1.draw()
                    window.flip()
                    startFrame=2
                    endFrame=startFrame+1
                    if frameN==startFrame:
                        task.write(False,timeout=0)
                    elif frameN==endFrame:
                        task.write(True,timeout=0)
                for frameN in range(int(refreshRate*blank_dur)):
                    patch2.draw()
                    window.flip()
            
            keys=event.getKeys()
            
            if len(keys)>0:
                keepGoing=False
elif trig_opt=='Pport':
    # Set up parallel port
    p=parallel64.StandardPort(spp_base_address=0x3efc)
    while keepGoing:
        if squareFlicker==True:
            # When to draw  stimuli
            if current_frame % (2*flicker_frequency) < flicker_frequency:
                patch1.draw()
            else:
                patch2.draw()
            # Show whatever has been drawn. Sometimes the stimuli, other times a blank screen. 
            # flip() waits for the next monitor update so the loop is time-locked to the screen here.
            window.flip()
            current_frame += 1  # increment by 1.
        
        else:    
            startFrame=0
            endFrame=startFrame+1
            for frameN in range(int(refreshRate*stim_dur)):
                patch1.draw()
                #startTime=customTime.millis()
                window.flip()  
                #flipEnd=customTime.millis()
                if frameN==startFrame:
                    p.write_data_register(0)
                    #endTime=customTime.millis()
                elif frameN==endFrame:
                    p.write_data_register(0xff)
            #flipTime=flipEnd-startTime
            #flip2writeTime=startTime-endTime
            #print(flipTime,flip2writeTime)
            for frameN in range(int(refreshRate*blank_dur)):
                patch2.draw()
                window.flip()
        
        keys=event.getKeys()
        
        if len(keys)>0:
            keepGoing=False

window.close()
print('Overall, %i frames were dropped.' % window.nDroppedFrames)
