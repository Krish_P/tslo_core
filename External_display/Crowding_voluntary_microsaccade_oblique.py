from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import customTime
import random
from random import randint
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from PixelConversion import PixelConversion
import pixlet
import online_sac_detect_module
import serial 
from SendDataFunction import TSLOsender 
from SendDataFunction import TSLOsenderlong
from ReceiveDataFunction import TSLOreceiver
from multiprocessing.connection import Listener
from multiprocessing.connection import Client
from make_mask import make_mask
from recvAndPlotImageParams import recvAndPlotImageParams  
import itertools
import socket
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
eye_tracker=False
lsl_connection=False
keep_repeating=True
cam_connection=True
imageParamsLSL=False
no_microsaccade_blocked=False
trial_validity_randomization=True
# Also set individual subject parameters
saccade_latency_param={1:0.350,2:0.700,3:0.320,4:0.315,5:0.260,6:0.350,7:0.350}
target_duration_param={1:20,2:7,3:6,4:12,5:7,6:9,7:5}
# We set the spacing with 80-90% performance based on expt 1 [all 4 flankers]
thresh_spacing={1:3,2:2.25,3:2,4:2,5:2,6:2,7:2}
# We can indicate whether we are using the socket on the same machine or not
# when using multiple machines we only send experiment info to the eye tracker system so we 
# only need a socket to send messages
multiple_machines=True
if multiple_machines==True:
    # We would receive saccade messages over parallel port to improve efficiency
    parallel_port=True
#===================================================================================================================================
## First we set up the parameters to set up hardware

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
    
if eye_tracker==True:
    if multiple_machines==True:
        host='192.168.0.101' #client IP
        port = 4005
        server=('192.168.0.143',4000)
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.bind((host, port))
    else:
        ## Setup listener
        # This would listen to saccade messages
        # Socket connection setup
        listenAddress=('localhost',6000)
        # family is deduced to be 'AF_INET'
        print('Looking for connection')
        listener = Listener(listenAddress, authkey=b'secret password')
        connListener = listener.accept()
        ## Setup Sender
        # This would send stimulus time and final close message to terminate all sockets
        # Set up the parameters for the socket
        sendAddress = ('localhost', 6001)
        while True:
            try:
                connSender = Client(sendAddress, authkey=b'secret password')
                break
            except ConnectionRefusedError:
                time.sleep(0.1)
                continue
        print('Connection established!')

# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
msg_size=pixel_per_degree/5
msg_offset=400
# Specify cue offset as a multiple of number of pixels per degree 
peripheral_cue=True
# Fixation target could be either cross or plus with circle in the middle
# Options: 1: "x" or 2" "+"
fixation_type='x'

if fixation_type=="+":
    fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
elif fixation_type=='x':
    fix_line_UR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
fix_c=visual.Circle(window, size=8, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

# Additionally,we set up the camera feature,so that subjects can visualize the retinal image quality and the PMT gain setting.
# For this to work the webcam would have to be setup such that it faces the retinal image window on ICANDI and the PMT box with the 
# gain setting
if cam_connection==True:
    from ximea import xiapi
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[1600,1600],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()
#===================================================================================================================================
while keep_repeating==True:
    #Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            startup_msg1=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg1.draw()
            startup_msg2.draw()
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                    stim1.draw()
            window.flip()

            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    if cam_connection==True:
                        webcam_state=True                
                        #start data acquisition
                        cam.start_acquisition()
                        while webcam_state==True & cam_connection==True:
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                    stim1.draw()
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()                       
                    elif cam_connection==False and imageParamsLSL==True:
                        while True:
                            imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                            #break when we get a keypress
                            keys=event.getKeys()
                            
                            if len(keys) != 0:
                                cv2.destroyAllWindows()
                                break
    # we stop if escape is pressed
    if 'keys' in locals() and lsl_connection==True:
        if keys[0]=='escape':
            keep_repeating=False
            continue
        # We can also build this list realtime
        sacc_latency_list=[]
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        protocol_packet=TSLOreceiver()
        stimulus_packet=TSLOreceiver()
        # These parameters are provided from TSLO machine
        oblique_cond=stimulus_packet[0]
        flanker_cond=stimulus_packet[1]
        num_repeats=protocol_packet[2] 
        # This would be the case where we are repeating specific conditions
        if num_repeats==1:
            flanker_info=protocol_packet[3]
            #Note for this the subject ID would have to be hardcoded
            subj_id=1
        else:
            # Otherwise 3rd info corresponds to subject id
            subj_id=protocol_packet[3]

        if no_microsaccade_blocked==True:
            # we then decide if we want trials with or w/o microsaccades
            no_microsaccade_status=protocol_packet[1]

    else: 
        # If LSL is false set these parameters locally to their defaults
        #no_microsacc_cond=1 # Sets whether it should stop with or w/o microsaccades
        subj_id=4
        flanker_info=100
        num_repeats=8 # Sets the number of repeats for each flanker spacing at a given ecc
        if no_microsaccade_blocked==True:
            no_microsaccade_status=False
        oblique_cond=0
        #flanker_cond: 0 for both rad/tang flankers; 1: for radial and 2: for tangential
        flanker_cond=0

    # These parameters are set locally (Stimulus Machine)
    eccentricity_deg=0.33
    cue_condition=True
    mask_condition=False
    location_uncertainity=True 
    reverse_flanker_contrast=False
    save_image=True
    foveal_mode=False
    # This would apply for the oblique microsaccade experiment
    neutral_trials=False
    if neutral_trials==True:
        # When we have neutral trials we may need to block microsaccade & no microsaccade
        # trials. Options to set here :
        #1. 'microsaccade'
        #2. 'nomicrosaccade'
        microsaccade_cond_with_neutral_trials='microsaccade'
    unflanked_cond=False
    # for oblique target locations we set whether all target locations would have
    # place holder
    
    behav_output=True
    if flanker_cond==0:
        meridian_anisotropy_cond=False
    else:
        meridian_anisotropy_cond=True
    if meridian_anisotropy_cond==True:
        # Could be radial/R (horizontal) or tangential/T (vertical) flanker locations
        if flanker_cond==1:
            meridian='radial'
        elif flanker_cond==2:
            meridian='tangential'
    #
    # We also get the SOA for individual subject from the dictionary
    saccade_latency_median=saccade_latency_param[subj_id]

    target_type='T'

    if target_type=='T':
        # we can flankers to be either T's or +'s
        flanker_type='+'

    # These parameters would be set at the start of the experiment and ideally would not want to change them 
    # after the start of the experiment
    #Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
    # on the fixation cross for variable duration before stimulus onset
    # Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
    target_presentation=target_duration_param[subj_id] #numFlips

    if num_repeats==1:
        flanker_spacing_list=[float(flanker_info)]
    else:
        if meridian_anisotropy_cond==True and meridian=='tangential' and oblique_cond==False:
            flanker_space=[1.1,1.25,1.75,100] # less crowded so we bring flankers closer
        elif meridian_anisotropy_cond==True and meridian=='radial' and oblique_cond==False:
            flanker_space=[1.25,1.75,2.50,100] # more crowded so we go back to same set of spacings
        elif meridian_anisotropy_cond==True and oblique_cond==True:
            flanker_space=[1.25,2,3,100]
        elif meridian_anisotropy_cond==False:
            flanker_space=[1.25]#[1.25,1.5,2.50,100]
        
    # We also have a bunch of Stimulus onset asynchrony values(SOA) as a
    # list that we loop over. This would determine the offset between
    # saccade cue and stimulus onset. Also would help us get saccades at
    # different time points following stimulus onset
    # Note: Values are in milliseconds 
    SOA_values=[0.025,0.050,0.075]
    # we creat combinations of spacing and SOA
    flanker_SOA_combinations=list(itertools.product(flanker_space,SOA_values))
    # now we put it back into the original lists
    flanker_space=[]
    SOA_values=[]
    for aCombination in flanker_SOA_combinations:
        flanker_space.append(aCombination[0])
        SOA_values.append(aCombination[1])
    if no_microsaccade_blocked==True:
        # This would be no microsaccade block
        if no_microsaccade_status==True:
            # No microsaccade condition
            flanker_spacing_list=np.repeat(flanker_space,num_repeats)
            microsacc_status_list=np.ones(len(flanker_space)*int(num_repeats))
            SOA_list=np.repeat(SOA_values,num_repeats)
        # this would be microsaccade block
        else:
            #Microsaccade condition:
            flanker_spacing_list=np.repeat(flanker_space,num_repeats)
            microsacc_status_list=np.zeros(len(flanker_space)*int(num_repeats))
            SOA_list=np.repeat(SOA_values,num_repeats)
        random.shuffle(flanker_spacing_list)
        random.shuffle(SOA_list)
        flanker_spacing_list=list(flanker_spacing_list)
        microsacc_status_list=list(microsacc_status_list)
        SOA_list=list(SOA_list)
    else:
        # When it is interleaved it can either be with the no microsaccade condition
        # or it can be with neutral trials
        if neutral_trials==True:
            if microsaccade_cond_with_neutral_trials=='microsaccade':
                # The neutral trials would be interleaved with the microsaccade condition
                # Only a small proportion would be neutral trials
                if num_repeats>1:
                    prop_neutral=0.25 # 25% of trials
                    prop_sacc=1- prop_neutral
                    num_repeats_neutral=num_repeats * prop_neutral
                    num_repeats_sacc=num_repeats * prop_sacc
                    if int(num_repeats_neutral)!=num_repeats_neutral: # would help catch cases where num repeats is a float
                        num_repeats_neutral=1
                        num_repeats_sacc=1
                    #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                    # we create separate flanker spacing list for each of the conditions
                    #Microsaccade condition:
                    flanker_spacing_list_sacc=np.repeat(flanker_space,num_repeats_sacc)
                    microsacc_cond=np.zeros(len(flanker_space)*int(num_repeats_sacc))
                    # Neutral condition
                    flanker_spacing_list_neutral=np.repeat(flanker_space,num_repeats_neutral)
                    # We would add two arrays to get two's which would represent the neutral condition
                    neutral_cond=np.ones(len(flanker_space)*int(num_repeats_neutral))+np.ones(len(flanker_space)*int(num_repeats_neutral))
                    # We create a list for the different stimulus onset asynchronies
                    SOA_list=np.repeat(SOA_values,num_repeats)
                    # we would concatenate both lists and shuffle them and this what would be used 
                    # in the for loop of the main experiment
                    flanker_spacing_list=np.concatenate([flanker_spacing_list_sacc,flanker_spacing_list_neutral])
                    microsacc_status_list=np.concatenate([microsacc_cond,neutral_cond])
                    temp_list=list(zip(flanker_spacing_list,microsacc_status_list))
                    random.shuffle(temp_list)
                    flanker_spacing_list,microsacc_status_list=zip(*temp_list)
                    flanker_spacing_list=list(flanker_spacing_list)
                    microsacc_status_list=list(microsacc_status_list)
                    SOA_list=list(SOA_list)
            elif microsaccade_cond_with_neutral_trials=='nomicrosaccade':
                # The no microsaccade condition would be interleaved with the microsaccade condition
                # Only a small proportion of trials would fall under the no microsaccade condition
                if num_repeats>1:
                    prop_neutral=0.25 # 25% of trials
                    prop_nosacc=1-prop_neutral
                    num_repeats_neutral=num_repeats * prop_neutral
                    num_repeats_nosacc=num_repeats
                    if int(num_repeats_neutral)!=num_repeats_neutral: # would help catch cases where num repeats is a float
                        num_repeats_neutral=1
                        num_repeats_nosacc=1
                    #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                    # we create separate flanker spacing list for each of the conditions
                    #Microsaccade condition:
                    flanker_spacing_list_nosacc=np.repeat(flanker_space,num_repeats_nosacc)
                    microsacc_cond=np.ones(len(flanker_space)*int(num_repeats_nosacc))
                    # Neutral condition
                    flanker_spacing_list_neutral=np.repeat(flanker_space,num_repeats_neutral)
                    # We would add two arrays to get two's which would represent the neutral condition
                    neutral_cond=np.ones(len(flanker_space)*int(num_repeats_neutral))+np.ones(len(flanker_space)*int(num_repeats_neutral))
                    # We create a list for the different stimulus onset asynchronies
                    SOA_list=np.repeat(SOA_values,num_repeats)
                    # we would concatenate both lists and shuffle them and this what would be used 
                    # in the for loop of the main experiment
                    flanker_spacing_list=np.concatenate([flanker_spacing_list_nosacc,flanker_spacing_list_neutral])
                    microsacc_status_list=np.concatenate([microsacc_cond,neutral_cond])
                    temp_list=list(zip(flanker_spacing_list,microsacc_status_list))
                    random.shuffle(temp_list)
                    flanker_spacing_list,microsacc_status_list=zip(*temp_list)
                    flanker_spacing_list=list(flanker_spacing_list)
                    microsacc_status_list=list(microsacc_status_list)
                    SOA_list=list(SOA_list)
        else:
            # When not using neutral trials we can interleave microsaccade trials 
            # with no microsaccade trials
            if num_repeats>1:
                prop_nomicrosaccade=0.25 # 25% of trials
                prop_microsaccade= 1- prop_nomicrosaccade # 75% of trials
                num_repeats_nomicrosaccade=num_repeats * prop_nomicrosaccade
                num_repeats_microsaccade=num_repeats * prop_microsaccade
                # we also split based on trial validity
                prop_invalid=0.50 # 50% of saccade trials
                # No microsaccade condition
                num_repeats_invalid_nomicrosaccade=num_repeats_nomicrosaccade * prop_invalid
                num_repeats_valid_nomicrosaccade=num_repeats_nomicrosaccade * (1-prop_invalid)
                # microsaccade condition
                num_repeats_invalid_microsaccade=num_repeats_microsaccade * prop_invalid
                num_repeats_valid_microsaccade=num_repeats_microsaccade * (1-prop_invalid)
                # we would then build a list with the valid & invalid conditions
                # separately for microsaccade & no microsaccade condition
                #no microsaccade condition
                valid_cond_no_microsaccade=np.ones(int(num_repeats_valid_nomicrosaccade) * len(flanker_space))
                invalid_cond_no_microsaccade=np.zeros(int(num_repeats_invalid_nomicrosaccade) * len(flanker_space))
                congruency_cond_list_no_microsaccade=np.concatenate([valid_cond_no_microsaccade,invalid_cond_no_microsaccade])
                # do the same for flanker spacing lists & SOA lists
                flanker_spacing_list_no_microsaccade_valid=np.repeat(flanker_space,num_repeats_valid_nomicrosaccade)
                flanker_spacing_list_no_microsaccade_invalid=np.repeat(flanker_space,num_repeats_invalid_nomicrosaccade)
                flanker_spacing_list_no_microsaccade=np.concatenate([flanker_spacing_list_no_microsaccade_valid,flanker_spacing_list_no_microsaccade_invalid])
                SOA_list_no_microsaccade_valid=np.repeat(SOA_values,num_repeats_valid_nomicrosaccade)
                SOA_list_no_microsaccade_invalid=np.repeat(SOA_values,num_repeats_invalid_nomicrosaccade)
                SOA_list_no_microsaccade=np.concatenate([SOA_list_no_microsaccade_valid,SOA_list_no_microsaccade_invalid])
                # microsaccade condition
                valid_cond_microsaccade=np.ones(int(num_repeats_valid_microsaccade) * len(flanker_space))
                invalid_cond_microsaccade=np.zeros(int(num_repeats_invalid_microsaccade) * len(flanker_space))
                congruency_cond_list_microsaccade=np.concatenate([valid_cond_microsaccade,invalid_cond_microsaccade])
                # do the same for flanker spacing lists & SOA lists
                flanker_spacing_list_microsaccade_valid=np.repeat(flanker_space,num_repeats_valid_microsaccade)
                flanker_spacing_list_microsaccade_invalid=np.repeat(flanker_space,num_repeats_invalid_microsaccade)
                flanker_spacing_list_microsaccade=np.concatenate([flanker_spacing_list_microsaccade_valid,flanker_spacing_list_microsaccade_invalid])
                SOA_list_microsaccade_valid=np.repeat(SOA_values,num_repeats_valid_microsaccade)
                SOA_list_microsaccade_invalid=np.repeat(SOA_values,num_repeats_invalid_microsaccade)
                SOA_list_microsaccade=np.concatenate([SOA_list_microsaccade_valid,SOA_list_microsaccade_invalid])
                #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                # we create separate flanker spacing list for each of the conditions
                #Microsaccade condition:
                microsacc_cond=np.zeros(len(flanker_space)*int(num_repeats_microsaccade))
                nomicrosacc_cond=np.ones(len(flanker_space) * int(num_repeats_nomicrosaccade))
                # We would shuffle each list separately and then concatenate
                # no microsaccade condition
                temp_list_no_microsaccade=list(zip(flanker_spacing_list_no_microsaccade,SOA_list_no_microsaccade,congruency_cond_list_no_microsaccade))
                random.shuffle(temp_list_no_microsaccade)
                flanker_spacing_list_no_microsaccade,SOA_list_no_microsaccade,congruency_cond_list_no_microsaccade=zip(*temp_list_no_microsaccade)
                flanker_spacing_list_no_microsaccade=list(flanker_spacing_list_no_microsaccade)
                congruency_cond_list_no_microsaccade=list(congruency_cond_list_no_microsaccade)
                SOA_list_no_microsaccade=list(SOA_list_no_microsaccade)
                # microsaccade condition
                temp_list_microsaccade=list(zip(flanker_spacing_list_microsaccade,SOA_list_microsaccade,congruency_cond_list_microsaccade))
                random.shuffle(temp_list_microsaccade)
                flanker_spacing_list_microsaccade,SOA_list_microsaccade,congruency_cond_list_microsaccade=zip(*temp_list_microsaccade)
                flanker_spacing_list_microsaccade=list(flanker_spacing_list_microsaccade)
                congruency_cond_list_microsaccade=list(congruency_cond_list_microsaccade)
                SOA_list_microsaccade=list(SOA_list_microsaccade)
    #===================================================================================================================================
    ## Create basic shapes
    # These would be drawn & /or manipulated during the main experiment
    # The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
    if location_uncertainity==True:
        #stim_location_list=['left','right']
        stim_location_list=['upperLeft','upperRight','lowerLeft','lowerRight']
    else:
        stim_location_list=['right']
    fix_xpos=0
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line

    #Create basic stimuli here that could be called multiple times during the course of the experiment
    small_square=visual.Rect(window,width=128,height=128,units='pix',fillColor='white', fillColorSpace='rgb',pos=(975,1625))
    wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start the experiment')
    wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

    # The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
    cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    # Cue for rightward target location
    cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    #cue for leftward target location
    cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    # Cue for voluntary microsaccade
    cue_c=visual.Circle(window, size=4, fillColor=(0,0,0), pos=(0,0), lineColor=(0,0,0) )
    # Cue for target location
    target_c=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    # For oblique targets presented at 1 of 4 locations we have separate cues
    target_c_ul=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_ur=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_ll=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_lr=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )

    #we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
    target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="T",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1],height=pixel_per_degree )
    flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
        
    # This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
    # The letter size is kept constant and is set to 1.25x the threshold from the isolated letter threshold measures.
    # To ensure letter size is supra threshold
    letter_height=(6.25/60) #corresponds to 6.25 arc min or 20/25
#    letter_height=(params[subj_id][eccentricity_deg])*1.25
    letter_height_pix=letter_height*pixel_per_degree

    # we create a mask that would cover both the stimulus and the flankers
    # Note: here we use a noise mask over the number mask, to make it a little easier to do the task/ less effect
    # Here the mask is just a random binary image that would change on every trial
    # Mask parameters
    mask_height=75
    mask_width=75
    # Create binary numpy array
    mask_array=np.random.randint(2,size=(mask_height,mask_width))
    mask_object=visual.ImageStim(window,image=mask_array,units='pix',size=(mask_width,mask_height),colorSpace='rgb')
    #===================================================================================================================================
    ## Experimental sequence:

    #We use the position of the fixation cross center to position the target and flankers
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 
    # Also we would alter the eccentricity for oblique locations
    # otherwise for horizontal locations we would do nothing
    if 'left' not in stim_location_list or 'right' not in stim_location_list:
        # To get the target at 0.33 deg we would need the legs of the triangle (pythagoras theorem)
        # to be set at different points
        eccentricity_deg=np.sqrt(eccentricity_deg**2/2)

    #This is used to provide a audio feedback to the user
    beep_sound_correct=Sound(value=1000,secs=0.25)
    beep_sound_incorrect=Sound(value=250,secs=0.25)

    #Have something that waits for a key press from the subject. This essentially would initiate the sync between the stimulus and 
    #imaging machines and also start the experiment. 
    if lsl_connection==True:
        keepGoing=True
        while keepGoing:
            stream_wait1=visual.TextStim(window,text=('Waiting for TSLO machine'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
            stream_wait2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)

            stream_wait1.draw()
            stream_wait2.draw()
                
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                    stim1.draw()
            window.flip()
                
            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        data_packet=TSLOreceiver()
       
    #set a timer to get stimulus timing
    timerDuration=2 #set for 1 minute for now since trials would be shorter than this
    timer = core.CountdownTimer(timerDuration)

    #===================================================================================================================================
    ## Start of main loop of experiment
    # We would get this from the TSLO machine and would help number the files 
    # consistently across both machines
    if lsl_connection==True:
        repeatCounterStart=data_packet[2]
    else:
        repeatCounterStart=0
    # We block saccade conditions: We either have microsaccade trials first/no microsaccade trials first
    block_status_decider=1#random.randint(0,1)
    for conditionIdx in range(2):
        flanker_counter=1 # would be used to identify the last flanker condition
        trial_validity_counter=1 # this would be used to run through SOA & congruency list
        if block_status_decider==0: # we do microsaccade trials first
            if conditionIdx==0:
                flanker_spacing_list=list(flanker_spacing_list_microsaccade)
                SOA_list=list(SOA_list_microsaccade)
                microsacc_status_list=list(microsacc_cond)
                congruency_cond_list=list(congruency_cond_list_microsaccade)
            else:
                flanker_spacing_list=list(flanker_spacing_list_no_microsaccade)
                SOA_list=list(SOA_list_no_microsaccade)
                microsacc_status_list=list(nomicrosacc_cond)
                congruency_cond_list=list(congruency_cond_list_no_microsaccade) 
        elif block_status_decider==1:
            if conditionIdx==0:
                flanker_spacing_list=list(flanker_spacing_list_no_microsaccade)
                SOA_list=list(SOA_list_no_microsaccade)
                microsacc_status_list=list(nomicrosacc_cond)
                congruency_cond_list=list(congruency_cond_list_no_microsaccade) 
            else:
                flanker_spacing_list=list(flanker_spacing_list_microsaccade)
                SOA_list=list(SOA_list_microsaccade)
                microsacc_status_list=list(microsacc_cond)
                congruency_cond_list=list(congruency_cond_list_microsaccade)
        if conditionIdx!=0:
            # We would show a message when subjects finish the first condition
            breakMsgText=visual.TextStim(window,text=('Condition complete\n Take a short break'),pos=(0.0,0.0),color=[1,1,1],height=msg_size)
            core.wait(0.500) # wait for 500 ms
            keepGoing=True
            while keepGoing:
                breakMsgText.draw()
                window.flip()
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    keepGoing=False
        for aflanker in flanker_spacing_list:
            # For repeats/custom trials we have matlab set the parameters otherwise
            # psychopy controls the set of flankers and microsaccade condition
            if num_repeats>1:
                # We set the microsaccade condition based on the flanker counter index
                no_microsacc_cond=microsacc_status_list[flanker_counter-1]
                if neutral_trials==True:
                    if no_microsacc_cond==2: # This would be neutral trials
                        SOA=np.random.choice(SOA_list)
                        # We would have target congruency to be one always
                        target_congruency_cond=1
                    else: # Would be for saccade and no microsaccade conditions
                        # we set the SOA values based on flanker counter index
                        SOA=SOA_list[trial_validity_counter-1]
                        #Likewise we set the congruency condition based on the flanker counter index
                        target_congruency_cond=congruency_cond_list[trial_validity_counter-1]
                else:
                    # we set the SOA values based on flanker counter index
                    SOA=SOA_list[trial_validity_counter-1]
                    #Likewise we set the congruency condition based on the flanker counter index
                    target_congruency_cond=congruency_cond_list[trial_validity_counter-1]
                
            # Check if escape was pressed and stop loop
            if 'key_pressed' in locals():
                if key_pressed=='escape':
                    keep_repeating=False
                    break
            if 'keys' in locals():
                if keys=='espace':
                    keep_repeating=False
                    break
            # we create a new variable that would hold the value for flanker spacing 
            # to be used for naming the output file
            #if aflanker is a float this would result in a value !=0 else would be 0
            if int(aflanker) % float(aflanker)==0: 
                flanker_val=int(aflanker)
                flanker_val_str='%d'%(flanker_val)
            else:
                flanker_val=float(aflanker)
                flanker_val_str='%.2f'%(flanker_val)
            # based on this we set the validity string to be used to write to output file
            if neutral_trials==True:
                if no_microsacc_cond!=2:
                    if target_congruency_cond==1:
                        trialValidity='valid'
                    elif target_congruency_cond==0:
                        trialValidity='invalid'
                else: # These would be neutral trials
                    trialValidity='neutral'
            else:
                # otherwise it would depend on congryency condition
                if target_congruency_cond==1:
                    trialValidity='valid'
                elif target_congruency_cond==0:
                    trialValidity='invalid'
            # 1. We create the behavioral output file
            # We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
            # accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
            # data
            #output filename
            file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())
            if lsl_connection==True:
                if meridian_anisotropy_cond==True:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_%s_SOA_%.3f_%s'%((data_packet[0]),(data_packet[1]),repeatCounterStart, eccentricity_deg,
                               flanker_val_str,SOA,meridian)
                else:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_%s_SOA_%.3f_%s'%((data_packet[0]),(data_packet[1]),repeatCounterStart, eccentricity_deg,
                               flanker_val_str,SOA,trialValidity)
                # we add an additional component if it is an oblique condition
                if oblique_cond==True:
                    trial_info="%s_oblique"%(trial_info)
                if no_microsacc_cond==1:
                    outfilename = ("results/%s_NoMicrosaccade_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==0:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==2:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                timerDuration=data_packet[3]
            else:
                trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_%s'%(subj_id,2,0,eccentricity_deg, flanker_val_str)
                if no_microsacc_cond==1:
                    outfilename = ("results/%s_NoMicrosaccade_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==0:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
            if behav_output==True:
                outfile = open(outfilename, "a")

            if eye_tracker==True:
                # We send the trial details to the tracker/saccade detection method
                if multiple_machines==True:
                    s.sendto(trial_info.encode('utf-8'),server)
                else:
                    connSender.send(trial_info)
                # Also send a message to indicate the start of the sequence/stream. This 
                # would be used to store data and write to log file
                if multiple_machines==True:
                    message="trialStart"
                    s.sendto(message.encode('utf-8'),server)
                else:
                    connSender.send("trialStart")
                # we get the starting time stamp in ms and this can be used to get relative
                # time stamp for stimulus onset
                startTime=customTime.millis()
                
            if behav_output==True:
                if meridian_anisotropy_cond==True:
                    #Add the header 
                    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
                                 "cueDuration","stim_dur","corr","stimTime","flanker_meridian","Flanker_spacing_not_tested") )
                else:
                    #Add the header 
                    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
                                 "cue_loc","cueDuration","stim_dur","corr","stimTime","trialValidity") )

            event.clearEvents()
            #The letter size is set here
            letter_size_deg=letter_height
            letter_size_pix=letter_size_deg*pixel_per_degree
            target.height=letter_size_pix
            flanker_left.height=letter_size_pix
            flanker_right.height=letter_size_pix
            flanker_up.height=letter_size_pix
            flanker_down.height=letter_size_pix
                
            #THe flanker spacing is set on eacn trial based on the stimulus size
            flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
            
            # The following set of lines set the flanker condition 
            stim_location=np.random.choice(stim_location_list)

            eccentricity_pix=int(eccentricity_deg*pixel_per_degree)
            
            # Can either be left or right
            if stim_location=='left':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentrcitiy_y=0
            elif stim_location=='right':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=0
            # Or can be along 4 diagonal meridians from fixation
            elif stim_location=='upperLeft':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentricity_y=fixation_pos[1]+eccentricity_pix
            elif stim_location=='upperRight':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=fixation_pos[1]+eccentricity_pix
            elif stim_location=='lowerLeft':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix
            elif stim_location=='lowerRight':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix
                
            # We set the position of the target placeholder using the eccentricity provided
            # and the location of the fixation cross
            target_c_ul.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]+eccentricity_pix))
            target_c_ur.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]+eccentricity_pix))
            target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            # we then set the target location randomly based on the different location choices
            # available 
            if foveal_mode==True:
                target_location=(fix_h.start[0],0)
            else:
                target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 

            # We would overwrite this if we want oblique targets
            if oblique_cond==True:
                target_location=(eccentricity,-eccentricity)

            target.pos=target_location
            # We would make sure we offset cue location horizontally and not vertically
            # important when doing oblique conditions
            if stim_location!='left' or stim_location!='right':
                cue_c.pos=target_location            
            else:
                cue_c.pos=(target_location[0],0)
            # we would drop another marker for target location
            if oblique_cond==True:
                target_c.pos=target_location
            
            # First we would reset fixation cross to original color
            fix_line_UL.color=[-1,-1,-1]
            fix_line_UR.color=[-1,-1,-1]
            fix_line_LL.color=[-1,-1,-1]
            fix_line_LR.color=[-1,-1,-1]
            # We also change which part of the fixation cross is highlighted based on where the
            # target would go on to appear
            # can be along 4 diagonal meridians from fixation
            # We would do this based on congruency or validty of trial
            if trial_validity_randomization==True:
                if target_congruency_cond==0: # incongruent/invalid trials
                    if stim_location=='upperLeft':
                        remaining_target_locs=["upperRight","lowerLeft","lowerRight"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    elif stim_location=='upperRight':
                        remaining_target_locs=["upperLeft","lowerLeft","lowerRight"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    elif stim_location=='lowerLeft':
                        remaining_target_locs=["upperRight","upperLeft","lowerRight"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    elif stim_location=='lowerRight':
                        remaining_target_locs=["upperRight","lowerLeft","upperLeft"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    
            # Based on condition we set the cue congruency string
            if target_congruency_cond==1:
                cueLocation=stim_location
            elif target_congruency_cond==0:
                cueLocation=locs_choice
            # We set the flanker spacing based on whether we are testing at all 4 flankers/Expt1 
            # or individually testing radial/tangential condition. In which case we set the meridian
            # not being tested to be far above spacing threshold
            if meridian_anisotropy_cond==False:
                if stim_location!='left' or stim_location!='right':
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1]-flanker_spacing_pix)
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1]+flanker_spacing_pix)
                    flanker_up.pos=(target_location[0]-flanker_spacing_pix,target_location[1]+flanker_spacing_pix)
                    flanker_down.pos=(target_location[0]+flanker_spacing_pix,target_location[1]-flanker_spacing_pix)
                else:
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1])
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1])
                    flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix)
                    flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix)
            else:
                # We also set the flanker spacing for the one not being tested (radial tangntial condition)
                thresh_spacing_subj=thresh_spacing[subj_id]
                flanker_not_tested_deg=thresh_spacing_subj*letter_height
                flanker_not_tested_pix=int(flanker_not_tested_deg*pixel_per_degree)
                if meridian=='radial' and aflanker!=100:# [Radial flankers tested; Tangential flankers not tested]
                    # Flankers being tested are set first
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1])
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1])
                    # Flankers not being tested
                    flanker_up.pos=(target_location[0],target_location[1]+flanker_not_tested_pix)
                    flanker_down.pos=(target_location[0],target_location[1]-flanker_not_tested_pix)
                elif meridian=='tangential' and aflanker!=100: # [Tangential flankers tested;Radial flankers not tested]
                    # flankers being tested are set first
                    flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix)
                    flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix)
                    #flankers not being tested 
                    flanker_left.pos=(target_location[0]-flanker_not_tested_pix,target_location[1])
                    flanker_right.pos=(target_location[0]+flanker_not_tested_pix,target_location[1])
                else: # This would be for the unflanked condition
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1])
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1])
                    flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix)
                    flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix)
                    
            # since mask is normalized we move it based on the screen dimensions
            mask_object.pos=target_location #we center the mask with the stimulus

            #Have something that waits for a key press from the subject. This would send the flanker spacing condition to the TSLO system.
            # We change the color of the central circle of the fixation cross
            # We only have this (cue) for the microsaccade condition other wise we set the color to be the same
            if no_microsacc_cond==0:  
                fix_c.fillColor=[0,1,0]
                fix_c.lineColor=[0,1,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='upperLeft':
                        fix_line_UL.color=[0,1,0]
                    elif cueLocation=='upperRight':
                        fix_line_UR.color=[0,1,0]
                    elif cueLocation=='lowerLeft':
                        fix_line_LL.color=[0,1,0]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[0,1,0]
            elif no_microsacc_cond==1:
                fix_c.fillColor=[1,0,0]
                fix_c.lineColor=[1,0,0]   
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='upperLeft':
                        fix_line_UL.color=[1,0,0]
                    elif cueLocation=='upperRight':
                        fix_line_UR.color=[1,0,0]
                    elif cueLocation=='lowerLeft':
                        fix_line_LL.color=[1,0,0]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[1,0,0]  
            elif no_microsacc_cond==2:
                #We make the entire fixation cross red, so it alerts the subjects
                fix_c.fillColor=[1,1,1]
                fix_c.lineColor=[1,1,1]   
                fix_line_UL.color=[1,0,0]
                fix_line_UR.color=[1,0,0]
                fix_line_LL.color=[1,0,0]
                fix_line_LR.color=[1,0,0]
                
            cue_c.fillColor=[1,1,1]
            cue_c.lineColor=[1,1,1]
            if lsl_connection==True:
                keepGoing=True
                while keepGoing:
                    trialStart1=visual.TextStim(window,text=('Sending flanker spacing condition'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                    trialStart2=visual.TextStim(window,text=('Press any key to start trial'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)

                    trialStart1.draw()
                    trialStart2.draw()
                        
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                    if mask_condition==True:
                        # we draw the mask
                        mask_object.draw()
                    window.flip()
                    keys=event.getKeys()

                    if len(keys)>0 and 'space' not in keys:
                        keepGoing=False
                    elif len(keys)>0:
                        if keys[0]=='space':
                            if cam_connection==True:
                                webcam_state=True                
                                #start data acquisition
                                cam.start_acquisition()
                                while webcam_state==True and cam_connection==True:
                                    #get data and pass them from camera to img
                                    cam.get_image(img)
                                    #create numpy array with data from camera. Dimensions of the array are 
                                    #determined by imgdataformat
                                    camData = img.get_image_data_numpy()
                                    #show acquired image 
                                    newdata=np.subtract(np.divide(camData,128.0),1.0)
                                    cam_image.setImage(newdata)
                                    cam_image.draw()
                                    if fixation_type=='+':
                                        for stim1 in [fix_v, fix_h, fix_c]:
                                            stim1.draw()
                                    elif fixation_type=='x':
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                            stim1.draw()
                                    window.flip()
                                    theKey = event.getKeys()
                                    if len(theKey)!=0:
                                        webcam_state=False
                                        #stop data acquisition
                                        cam.stop_acquisition()
                            elif cam_connection==False and imageParamsLSL==True:
                                while True:
                                    imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                                    #break when we get a keypress
                                    keys=event.getKeys()
                                    
                                    if len(keys) != 0:
                                        cv2.destroyAllWindows()
                                        break
                #TSLO sender keeps sending packets until it is received by the TSLO machine. The second message here
                # is a dummy message
                TSLOsenderlong(flanker_val,repeatCounterStart,no_microsacc_cond,SOA,target_congruency_cond,0)

            else:
                keepGoing=True
                while keepGoing:
                    wait_msg.draw()
                  
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()

                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                        
                    keys=event.getKeys()
                    if len(keys)>0 and 'space' not in keys:
                        keepGoing=False
                    elif len(keys)>0:
                        if keys[0]=='space':
                            if cam_connection==True:
                                webcam_state=True                
                                while webcam_state==True and cam_connection==True:
                                    #start data acquisition
                                    cam.start_acquisition()
                                    #get data and pass them from camera to img
                                    cam.get_image(img)
                                    #create numpy array with data from camera. Dimensions of the array are 
                                    #determined by imgdataformat
                                    camData = img.get_image_data_numpy()
                                    #show acquired image 
                                    newdata=np.subtract(np.divide(camData,128.0),1.0)
                                    cam_image.setImage(newdata)
                                    cam_image.draw()
                                    if fixation_type=='+':
                                        for stim1 in [fix_v, fix_h, fix_c]:
                                            stim1.draw()
                                    elif fixation_type=='x':
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                            stim1.draw()
                                    if mask_condition==True:
                                        # we draw a mask that would cover both the stimulus and flankers
                                        mask_object.draw()
                                    #also draw peripheral saccade cue
                                    window.flip()
                                    theKey = event.getKeys()
                                    if len(theKey)!=0:
                                        webcam_state=False
                                        #stop data acquisition
                                        cam.stop_acquisition()
                            elif cam_connection==False and imageParamsLSL==True:
                                while True:
                                    imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                                    #break when we get a keypress
                                    keys=event.getKeys()
                                    
                                    if len(keys) != 0:
                                        cv2.destroyAllWindows()
                                        break
            if keys[0]=='escape':
                keep_repeating=False
                break
                
            ## 3. Main trial sequence
            if target_type=='bars':
                target_type_list=['h','v']*3
            elif target_type=='T':
                if flanker_type=='+':
                    target_type_list=['+']*5
                elif flanker_type=='T':
                    target_type_list=['u','d','l','r']
                    # Since we need five different stimuli we randomly add a fifth one
                    target_type_list.append(random.choice(target_type_list))
            elif target_type=='numbers':
                target_type_list=['0','1','2','3','4','5','6','7','8','9']
            elif target_type=='E':
                target_type_list=['E','3','e','f']
                # Since we need five different stimuli we randomly add a fifth one
                target_type_list.append(random.choice(target_type_list))
            # We shuffle the list of stimuli before assigning them
            random.shuffle(target_type_list)
            # We first assign target stimuli
            if target_type=='T' and flanker_type=='+': # we set the target to be a T with random orientation
                target_lists=['u','d','l','r']
                target_let=np.random.choice(target_lists)
            elif target_type=='T' and flanker_type=='T':
                target_let=target_type_list[0]
            # We finally assign flanker stimuli
            flanker_let_1=target_type_list[1]
            flanker_let_2=target_type_list[2]
            flanker_let_3=target_type_list[3]
            flanker_let_4=target_type_list[4]

            # we can also switch the contrast between the target and flankers
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]

            # we then set the respective target and flanker characters 
            target.text=target_let
            flanker_left.text=flanker_let_1
            flanker_right.text=flanker_let_2
            flanker_up.text=flanker_let_3
            flanker_down.text=flanker_let_4
            # The stimulus duration is used as a fraction of the frame rate of the display;
            # THis ensures better timing of the target presentations
            target.render()
            flanker_right.render()
            flanker_left.render()
            flanker_up.render()
            flanker_down.render()

            # The following set of code is used to present the stimulus & cue at pre-determined
            # locations and duration
            # The experimental sequence is as follows:
            # 1. Initial delay of 750 to 1250 ms
            # 2. Saccade cue: Circle changes color indicating subjects to make the saccade
            # 3. Variable delay to maximize the number of trials presented across different time
            #    points before stimulus onset
            # 4. Brief Stimulus presentation
            # 5. Post stimulus delay for 200-300 ms to capture saccade events
            
            # we add a delay on the very first trial so each video is padded to prevent 
            # any stimuli from being missed from frame grab/recording
            # We would 
            initialDelay=np.random.choice(np.arange(0.750,1.250,0.100))
            if lsl_connection==True:
                for nflips in range(int(refreshRate*initialDelay)):
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                    wait_msg2.draw()

                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                    # throw away any saccade messages
                    if eye_tracker==True:
                        status=connListener.poll()
                        if status==True:
                            clear_msg=connListener.recv()
            else:
                for nflips in range(int(refreshRate*initialDelay)):
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                                    stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()

                    wait_msg2.draw()
                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    # We can also save the frame as an image when needed
                    if save_image==True and nflips<1:
                        window.getMovieFrame(buffer='back')
                    #also draw peripheral saccade cue
                    window.flip()
            #Use this to save the cue onset image
            if save_image==True:
                window.saveMovieFrames('Cue_Onset'+'.jpg')
                                    
            # The cue would be an exogenous peripheral cue that would on for the whole trial 
            # The observer would only move their eye towards the cue when fixation cross 
            # goes away/ central circle changes color
            clock.reset()
            # Ideally we would want this duration to be less than Saccade Reaction Time (SRT)
            # SRT- Time from cue onset to saccade onset. Hence cue onset must be taken into account
            if eye_tracker==True:
                cueDuration=saccade_latency_median-(SOA)
            else:
                cueDuration=0.200

            # we revert the color of the central circle from the fixation cross otherwise
            # for no microsaccade condition we keep it the same
            if no_microsacc_cond==0:  
                fix_c.fillColor=[0,0,0]
                fix_c.lineColor=[0,0,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='upperLeft':
                        fix_line_UL.color=[-1,-1,-1]
                    elif cueLocation=='upperRight':
                        fix_line_UR.color=[-1,-1,-1]
                    elif cueLocation=='lowerLeft':
                        fix_line_LL.color=[-1,-1,-1]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[-1,-1,-1]
            elif no_microsacc_cond==1:
                fix_c.fillColor=[1,0,0]
                fix_c.lineColor=[1,0,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='upperLeft':
                        fix_line_UL.color=[1,0,0]
                    elif cueLocation=='upperRight':
                        fix_line_UR.color=[1,0,0]
                    elif cueLocation=='lowerLeft':
                        fix_line_LL.color=[1,0,0]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[1,0,0]
            elif no_microsacc_cond==2:
                #just make fixation circle white
                fix_c.fillColor=[1,1,1]
                fix_c.lineColor=[1,1,1]  

            # We have a place holder for saccade time stamp that would be changed when 
            # we get a saccade event
            sacc_cond=0
            sacc_timeStamp=0
            sacc_before_target=0
            sacc_before_target_timeStamp=0
            late_sacc=0
            late_sacc_timeStamp=0
            for nflips in range(int(refreshRate*cueDuration)):
                if oblique_cond==True:
                    for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                        stim1.draw()
                else:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                #Draw the small square for Photodiode to detect and to create a marker in the video
                if nflips<5:
                    small_square.draw()
                # We can also save an image of the cue if needed
                if save_image==True and nflips<1:
                    window.getMovieFrame(buffer='back')
                window.flip()
                # we record time stamp for when the cue came on
                if nflips==0:
                    cueTime=customTime.millis()
                # Ideally we wont get saccades before target onset. But we get cases 
                # where it does occur to get better estimate of latency
                if eye_tracker==True:
                    status=connListener.poll()
                    if status==True and sacc_before_target_timeStamp==0 and sacc_before_target==0: 
                        try:
                            got_one=connListener.recv()
                            sacc_before_target = sacc_before_target | got_one;
                            sacc_before_target_timeStamp=customTime.millis()
                            sacc_latency=sacc_before_target_timeStamp - cueTime
                            sacc_latency_list.append(sacc_latency)
                        except EOFError:
                            print('receive function error')


            #Use this to save the cue offset image
            if save_image==True:
                window.saveMovieFrames('Cue_Offset'+'.jpg')
            # throw away any saccade messages
            if eye_tracker==True:
                status=connListener.poll()
                if status==True:
                    clear_msg=connListener.recv()
            # we clear key events that could occured after the end of the previous trial 
            event.clearEvents()
            # we would only draw the next stimulus only when a saccade was not detected 
            # during the delay period. If saccade did occur we skip to the response window
            for frameN in range(target_presentation):
                if fixation_type=='+':
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                elif fixation_type=='x':
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                        stim1.draw()
                # We continue to have the saccade circle in cases where the target & cue location
                # are incongruent
                if target_congruency_cond==0:
                    if stim_location=='upperLeft':
                        for stim2 in [target_c_ur,target_c_ll,target_c_lr]:
                            stim2.draw()
                    elif stim_location=='upperRight':
                        for stim2 in [target_c_ul,target_c_ll,target_c_lr]:
                            stim2.draw()
                    elif stim_location=='lowerLeft':
                        for stim2 in [target_c_ur,target_c_ul,target_c_lr]:
                            stim2.draw()
                    elif stim_location=='lowerRight':
                        for stim2 in [target_c_ur,target_c_ul,target_c_ll]:
                            stim2.draw()
                #Draw the small square for Photodiode to detect and to create a marker in the video
                small_square.draw()

                target.draw()                
                # we test whether the stimulus would appear on the left/right and based
                # on that we set the inner and outer flankers
                if stim_location=='left' or stim_location=='upperLeft' or stim_location=='lowerLeft':                    
                    inner_flanker=flanker_right
                    outer_flanker=flanker_left
                elif stim_location=='right' or stim_location=='upperRight' or stim_location=='lowerRight':
                    inner_flanker=flanker_left
                    outer_flanker=flanker_right
                
                inner_flanker.draw()
                outer_flanker.draw()
                flanker_up.draw()
                flanker_down.draw()
      
                # We capture fixation target and save to image file if needed
                if save_image==True and frameN<1:
                    window.getMovieFrame(buffer='back')
                flip_time=window.flip()

                if frameN==0:
                    # we would use this to debug missed markers in the retinal video
                    stimTime=timerDuration-timer.getTime() #gives relative time stamps
                    currTime=customTime.millis()

            # we have socket message outside the target presentation to improve performance
            if eye_tracker==True:
                # we send the stimulus time to tracker/saccade method via
                pythonTime=str(currTime-startTime) # gives timestamps based on python clock
                # socket
                if multiple_machines==True:
                    s.sendto(pythonTime.encode('utf-8'),server)
                else:
                    connSender.send(pythonTime)
                # Ideally we wont get saccades before target onset. But we get cases 
                # where it does occur to get better estimate of latency
                status=connListener.poll()
                if status==True and sacc_before_target_timeStamp==0 and sacc_before_target==0: 
                    try:
                        got_one=connListener.recv()
                        sacc_before_target = sacc_before_target | got_one;
                        sacc_before_target_timeStamp=customTime.millis()
                        sacc_latency=sacc_before_target_timeStamp - cueTime
                        sacc_latency_list.append(sacc_latency)
                    except EOFError:
                        print('receive function error')

            #Use this to save the stimulus image
            if save_image==True:
                window.saveMovieFrames('Stimulus'+'.jpg')

            # we have a small window following stimulus offset to capture the saccade 
            # event and make a note of the latency. If we do get a saccade
            # during the cue window or during target presentation we would omit those trials
            if eye_tracker==True :
                nflips=0
                # we bring back the cue_c so subjects can hold fixation
                if no_microsacc_cond==0:  
                    postStimDelay=0.600
                    cue_c.fillColor=[0,1,0]
                    cue_c.lineColor=[0,1,0]
                else:
                    postStimDelay=0.100
                for nflips in range(int(refreshRate*postStimDelay)):
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                    if oblique_cond==True:
                        target_c.draw()
                    wait_msg2.draw()
                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                    # when using the eye tracker we look for a saccade event following stimulus onset
                    # and halt the program when one is detected. 
                    status=connListener.poll()
                    # we would like to the get the saccade time stamp for the very first 
                    # saccade that occurs after stimulus offset
                    # we also skip events from the first few flip to account for latenct of the eye tracking
                    # protocol
                    # We set a limit for when a saccade can occur after target
                    # offset for valid trials
                    saccade_window=0.300
                    if nflips<=int(refreshRate*saccade_window):
                        if status==True and sacc_timeStamp==0 and sacc_cond==0: 
                            try:
                                got_one=connListener.recv()
                                sacc_cond = sacc_cond | got_one;
                                sacc_timeStamp=customTime.millis()
                                sacc_latency=sacc_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
                            except EOFError:
                                print('receive function error')
                    else: # These would be trials with late saccades (which we would still record to get latency)
                        status=connListener.poll()
                        if status==True and late_sacc_timeStamp==0 and late_sacc==0: 
                            try:
                                got_one=connListener.recv()
                                late_sacc = late_sacc | got_one;
                                late_sacc_timeStamp=customTime.millis()
                                sacc_latency=late_sacc_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
                            except EOFError:
                                print('receive function error')
                #VALID
                # only do this when we get a valid saccade event
                if sacc_cond==1 and no_microsacc_cond==0:
                    # This would be a valid trial and hence we would get behav response
                    del_trial=False
                            
                #INVALID
                # When we miss a saccade on a microsaccade trial we would repeat it at the 
                # end of the sequence
                elif sacc_cond==0 and no_microsacc_cond==0:
                    # we would not want to get the response on this trial since its invalid and 
                    # repeat the condition
                    del_trial=True

                #VALID
                # This would be a valid trial for no microsaccade condition hence we get the 
                # behavioral response
                elif sacc_cond==0 and no_microsacc_cond==1:
                    del_trial=False
                    
                #INVALID
                # But having a microsaccade when one is not expected would make it again an
                # invalid trial which would be repeated at the end
                elif sacc_cond==1 and no_microsacc_cond==1:
                    del_trial=True
                
                #VALID
                # No eye movement on neutral trial would make it valid
                elif sacc_cond==0 and no_microsacc_cond==2:
                    del_trial=False
                #INVALID
                # Having microsaccade on neutral trials
                elif sacc_cond==1 and no_microsacc_cond==2:
                    del_trial=True
                
            else:
                del_trial=False

            # We reset eye tracking protocol & drop trial marker to output file
            if lsl_connection==True:
                if multiple_machines==True:
                    message="trialStop"
                    s.sendto(message.encode('utf-8'),server)
                else:
                    connSender.send("trialStop")
                
            keepGoing=True
            while keepGoing==True:
                response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                response_wait.draw()
                if del_trial==True:
                    if sacc_cond==0: # we did not get a saccade after stimulus/failed
                        msg="Moved too slow/early!"
                    else:
                        msg='Eyes moved!'
                    failed_msg=visual.TextStim(window,text=(msg),pos=(0,-msg_offset),color=[-1,-1,-1],height=msg_size)
                    #failed_msg.draw()
                if fixation_type=='+':
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                elif fixation_type=='x':
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                        stim1.draw()
                if oblique_cond==True:
                    target_c.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                flip_time=window.flip()

                key=event.getKeys()
                if len(key)>0:
                    keepGoing=False
                    continue
            
            # we check if there was a keypress made duration the response window otherwise we set a defualt value
            if 'key' not in locals():
                key_pressed='Nil' # arbitary value 
            elif len(key)==0:
                key_pressed='Nil'
            else:
                key_pressed=key[0]
                    
            #An escape key press here terminates the loop
            if key_pressed=='escape':
                keep_repeated=False
                break
            # we assign a response value based on the keypress that was made by the subject
            if target_type=='numbers':
                if key_pressed=='num_0':
                    response_key='0'
                elif key_pressed=='num_1':
                    response_key='1'
                elif key_pressed=='num_2':
                    response_key='2'
                elif key_pressed=='num_3':
                    response_key='3'
                elif key_pressed=='num_4':
                    response_key='4'
                elif key_pressed=='num_5':
                    response_key='5'
                elif key_pressed=='num_6':
                    response_key='6'
                elif key_pressed=='num_7':
                    response_key='7'
                elif key_pressed=='num_8':
                    response_key='8'
                elif key_pressed=='num_9':
                    response_key='9'
                else:
                    response_key='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
            elif target_type=='bars':
                if key_pressed=='left' or key_pressed=='right' or key_pressed=='num_4' or key_pressed=='num_6':
                    response_key='h'
                elif key_pressed=='up' or key_pressed=='down' or key_pressed=='num_8' or key_pressed=='num_2':
                    response_key='v'
                else:
                    response_key='100'
            elif target_type=='T':
                if key_pressed=='up':
                    response_key='u'
                elif key_pressed=='down':
                    response_key='d'
                elif key_pressed=='left':
                    response_key='l'
                elif key_pressed=='right':
                    response_key='r'
                else:
                    response_key='100'
            elif target_type=='E':
                if key_pressed=='right':
                    response_key='E'
                elif key_pressed=='left':
                    response_key='3'
                elif key_pressed=='up':
                    response_key='e' # need to verify this
                elif key_pressed=='down':
                    response_key='f'
                else:
                    response_key='100'
            # we woudl provide feedback based on whether or not the response was correct
            if response_key==target.text:
                beep_sound_correct.play()
                corr=1
            else:
                beep_sound_incorrect.play()
                corr=0
            
            # we would write the response to the output file
            # we convert target presentation to ms from flips before writing to output file
            target_presentation_ms=target_presentation*(1000/refreshRate)
            cueDuration_ms=cueDuration#*(1000/refreshRate)
            if key_pressed!='escape' and behav_output==True:
                if meridian_anisotropy_cond==True:
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%d,%2f,%s,%2f\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                letter_size_deg,eccentricity_deg,aflanker,stim_location,cueDuration_ms,target_presentation_ms,corr,stimTime,meridian,thresh_spacing_subj) )
                else:
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%s,%2f,%2f,%d,%2f,%s\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                letter_size_deg,eccentricity_deg,aflanker,stim_location,cueLocation,cueDuration_ms,target_presentation_ms,corr,stimTime,trialValidity) )
            # This would be a valid trial
            if del_trial==False:
                # This marks the end of the trial so we send this info across to TSLO machine
                trialComplete=1
                if flanker_counter==len(flanker_spacing_list) and conditionIdx!=0:
                    sequenceComplete=1 # last trial from block
                else:
                    sequenceComplete=0 # more trials left in the block
                if lsl_connection==True:
                    TSLOsender(trialComplete,sequenceComplete)

            elif del_trial==True:
                # We would send a different message to matlab to delete the video 
                # associated with invalid trial
                trialComplete=2 # invalid trial message
                if lsl_connection==True:
                    # Since the trial is not valid we would repeat even if it
                    # is the last flanker condition of the sequence
                    sequenceComplete=0 
                    # we would send a message to the TSLO machine
                    TSLOsender(trialComplete,sequenceComplete)
                # append the faulty trial to the end of the list
                flanker_spacing_list.append(aflanker)
                microsacc_status_list.append(no_microsacc_cond)
                SOA_list.append(SOA)
                congruency_cond_list.append(target_congruency_cond)
            # we increment the trial counter / repeat counter only after each trial
            repeatCounterStart+=1
             # we increment the flanker counter after each trial
            flanker_counter+=1   
            # Increment target congruency & soa counter only 
            if neutral_trials==True:
                if no_microsacc_cond==2: # this would be for neutral trials
                    pass
                else: # otherwise we increment this counter
                    trial_validity_counter+=1
            else: 
                trial_validity_counter+=1
            #close the output file here
            if behav_output==True:
                outfile.close()   

    # We would print out a message for the subject and wait for them to hit a key before restarting the program
    loop_msg1=visual.TextStim(window,text='Block Complete!',pos=(0,0),height=msg_size)
    loop_msg2=visual.TextStim(window,text='Press any key to continue!',pos=(0,-150),height=msg_size)
    loop_msg1.draw()
    loop_msg2.draw()
    window.flip()
    # We print real time saccade latency measures
    if eye_tracker==True:
        print(sacc_latency_list)
        print(np.median(sacc_latency_list),np.std(sacc_latency_list),flush=True)
    event.waitKeys()

#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

# we close the different programs/processes that were opened
window.close()
core.quit()

if eye_tracker==True:
    # we close the socket program
    connListener.close()
    listener.close()
    connSender.close()
    print('connection closed')
