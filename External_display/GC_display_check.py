from psychopy import visual, core, monitors,event
import os
import numpy as np
import time
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
from psychopy import logging
logging.console.setLevel(logging.CRITICAL)

import numpy as np

image_dir=r'D:\KSP\Study\Houston\Projects\TSLO Reading Experiment\TSLO_core\Sample Code\External display'
#
#text_images= []
#for file in os.listdir(image_dir):
#    if file.lower().endswith(".bmp"):
#        text_images.append(file)
        

        
try:
    os.remove('et_data.EDF')#nm: remove edf file from the previous trial... in ideal case should save directly with another name... have not figured out yet.
except OSError:
    pass

class ExperimentRuntime(ioHubExperimentRuntime):
    def run(self,*args):

        monsize =  [1920, 1080]
        calibrate=True

        mouse=self.hub.devices.mouse
        clock = core.Clock()

        tracker=self.hub.devices.tracker

        #monitorsetting.setSizePix(monsize)
        tracker.runSetupProcedure()
        window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[-1,-1,-1], screen=1)
        window.recordFrameIntervals=False

        self.hub.clearEvents('all')
        tracker.setRecordingState(False)


        #marks the start of eye tracking
        clock.reset()
        self.hub.clearEvents('all')
        tracker.setRecordingState(True)
        #This denotes the beginning of the experiment and useful especially when one is parsing the data
        tracker.sendMessage("CLAB START EXPT")
        
        done=False
        while done==False:
            keys=event.getKeys()
            if 'q' in keys:
                done=True
                continue
            gpos=tracker.getPosition()
            if type(gpos) in [tuple,list]:
                gc_circle=visual.Circle(window,pos=(gpos[0],gpos[1]),radius=25)
            else:
                gc_circle=visual.Circle(window,pos=(0,0),radius=25)
            gc_circle.setFillColor(color=[1,1,1])
            gc_circle.draw()
            window.flip()

                # Marks the end of eye tracking
        window.recordFrameIntervals=False
        tracker.setRecordingState(False)
        tracker.setConnectionState(False)
        window.flip()
        core.wait(2)
        window.close()
        core.quit()

if __name__=="__main__":
    runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
    runtime.start()