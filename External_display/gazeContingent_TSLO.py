from psychopy import core, visual, event, monitors, data, gui
import serial 
import time
import numpy as np
import pandas as pd
import ctypes
import os
import random
from psychopy.sound import Sound
from psychopy import prefs
prefs.general['audioLib'] = ['PTB']
from multiprocessing.connection import Listener
from PixelConversion import PixelConversion
from multiprocessing.connection import Client
from ximea import xiapi
import nidaqmx
from nidaqmx.constants import TerminalConfiguration
from nidaqmx.constants import AcquisitionType
from SendDataFunction import stimMachineSender
from pylsl import StreamInfo, StreamOutlet
import parallel64
import customTime
import socket

gaze_contingent=False 
# We can also set direction criteria for saccade detection and we would 
# do this when gaze contingent mode is turned off
if gaze_contingent==False:
    stimLocationCond=False
cam_connection=True
multiple_machines=True

# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
refreshRate=window.getActualFrameRate()

#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 165cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# we setup camera
if cam_connection==True:
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[1600,1600],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()

# Stimulus design setup
num_deg=0.33
xoff=pixel_per_degree*(num_deg)
# Draw basic shapes which will be draw later in the loop
fix_xpos=0
step_size=20
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=15, fillColor=(1,1,1), pos=(0,0), lineColor=(0,0,0) )
patch_pos=(975,1625)
black_patch=visual.Rect(window,width=128,height=128,units='pix',fillColor='black', lineColor='black',fillColorSpace='rgb',pos=patch_pos)
#peripheral saccade cue:
cue_c=visual.Circle(window, size=6, fillColor=(0,1,0), pos=(xoff,0), lineColor=(0,1,0) )
gaze_c=visual.Circle(window, size=50, fillColor=(0,1,0), pos=(0,0), lineColor=(1,1,1) )
vert_line_l=visual.Line(window, start=(fix_xpos-xoff,0-step_size), end=(fix_xpos-xoff,0+step_size), lineColor=[-1,-1,-1], lineWidth=4)
vert_line_r=visual.Line(window, start=(fix_xpos+xoff,0-step_size), end=(fix_xpos+xoff,0+step_size), lineColor=[-1,-1,-1], lineWidth=4)
small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
saccade_message=visual.TextStim(window,text='Saccade Detected!!',units='pix',pos=(0,200),color='white',alignText='center')
start_message=visual.TextStim(window,text='Test started!',units='pix',pos=(0,-200),color='white',alignText='center')

num_seconds=15
num_flips=refreshRate*num_seconds
beep_sound_saccade=Sound(value=200,secs=0.100)

while True:
    for stim1 in [fix_v, fix_h, fix_c]:
        stim1.draw()
    window.flip()
    keys=event.getKeys()
    if len(keys)>0 and 'space' not in keys:
        break
    elif len(keys)>0:
        if keys[0]=='space':
            webcam_state=True                
            #start data acquisition
            cam.start_acquisition()
            while webcam_state==True & cam_connection==True:
                #get data and pass them from camera to img
                cam.get_image(img)
                #create numpy array with data from camera. Dimensions of the array are 
                #determined by imgdataformat
                camData = img.get_image_data_numpy()
                #show acquired image 
                newdata=np.subtract(np.divide(camData,128.0),1.0)
                cam_image.setImage(newdata)
                cam_image.draw()
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw() # overlays fixation cross
                window.flip()
                theKey = event.getKeys()
                if len(theKey)!=0:
                    webcam_state=False
                    #stop data acquisition
                    cam.stop_acquisition()
# Can only run GC system when using a single machine
if gaze_contingent==1 and multiple_machines==False:
    nflips=0
    # we keep drawing the circle based on the raw x/y pos from TSLO
    with nidaqmx.Task() as task:
        # We setup the ADC card using the nidaqmx library
        task.ai_channels.add_ai_voltage_chan("Dev1/ai0",terminal_config=TerminalConfiguration.RSE)
        task.timing.cfg_samp_clk_timing(1000,"",sample_mode=AcquisitionType.HW_TIMED_SINGLE_POINT,samps_per_chan=1)
        while nflips<num_flips:
            # We would read in the x position from the TSLO
            xpos_raw= task.read(number_of_samples_per_channel=1)[-1]       
            #we use the raw eye position value from the TSLO to change the position of circle
            gaze_c.pos=(-960,(xpos_raw*500)-1200)
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            gaze_c.draw()
            black_patch.draw()
            window.flip()
            nflips+=1
            # look for keys and exit
            key_pressed=event.getKeys()
            if 'escape' in key_pressed:
                break
else:
    ## Setup listener
    # This would listen to saccade messages
    # Socket connection setup
    if multiple_machines==True:
        print('Looking for eye tracker!')
        # We setup LSL once
        info = StreamInfo('ASUS', 'Server', 1, 100, 'string', 'myuid34234')
        # next make an outlet
        outlet = StreamOutlet(info)
        print('Connection established!')
        # Also set up parallel port
        p=parallel64.StandardPort(spp_base_address=0x3efc)

        #Also set up socket for trial status messages
        # The stimulus sytem would be the client
        host='192.168.0.101' #system ip
        port=4005
        server=('192.168.0.143',4000)
        s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        s.bind((host,port))
        s.setblocking(0)
    else:
        listenAddress=('localhost',6000)
        # family is deduced to be 'AF_INET'
        print('Looking for connection')
        listener = Listener(listenAddress, authkey=b'secret password')
        connListener = listener.accept()
        ## Setup Sender
        # This would send stimulus time and final close message to terminate all sockets
        # Set up the parameters for the socket
        sendAddress = ('localhost', 6001)
        while True:
            try:
                connSender = Client(sendAddress, authkey=b'secret password')
                break
            except ConnectionRefusedError:
                time.sleep(0.1)
                continue
        print('Connection established!')

    file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())
    trial_info='gaze_contingent_S01_%s'%(file_timestamp)
    # We send the trial details to the tracker/saccade detection method
    if multiple_machines==True:
        stimMachineSender(trial_info,info,outlet,commSetup=False)
    else:
        connSender.send(trial_info)
    # Also send a message to indicate the start of the sequence/stream. This 
    # would be used to store data and write to log file
    if multiple_machines==True:
        stimMachineSender("trialStart",info,outlet,commSetup=False)
    else:
        connSender.send("trialStart")
    nflips=0
    if stimLocationCond==True:
        if multiple_machines==True:
            stimMachineSender('stimLeft',info,outlet,commSetup=False)
        else:
            connSender.send('stimLeft')
    while nflips<num_flips:
        beep_sound_saccade=Sound(value=200,secs=0.100)
        for stim1 in [fix_v, fix_h, fix_c,cue_c]:
            stim1.draw()
        start_message.draw()
#        vert_line_l.draw()
#        vert_line_r.draw()
        # we add a message to the display when a saccade is detected
        # When indexing into the position list we reduce counter by one since it would 
        # have been incremented in the for loop to get the recent info
        if multiple_machines==True:
            raw_msg=p.read_status_register()
            if raw_msg & 64==0:
                sacc_status=False
            else:
                sacc_status=True
        else:
            status=connListener.poll()
            if status==True:
                try:
                    sacc_status=int(connListener.recv())
                except EOFError:
                    print('receive function error')
                    sacc_status=0
        if sacc_status==True:
            timer=core.CountdownTimer(0.500)
            beep_sound_saccade.play()
        if 'timer' in locals() and timer.getTime()>0:
            saccade_message.draw()
        window.flip()
        nflips+=1
        # look for keys and exit
        key_pressed=event.getKeys()
        if 'escape' in key_pressed:
            if multiple_machines==True:
                while True:
                    try:
                        # Keeps sending till eye tracker picks it
                        message='q'
                        s.sendto(message.encode('utf-8'),server)
                        # recv feedback msg
                        (data,addr)=s.recvfrom(1024)
                        data=data.decode('utf-8')
                        if data=='q':
                            break
                    except BlockingIOError:
                        pass         
            break
    if multiple_machines==True and 'escape' not in key_pressed:
        while True:
            try:
                # Keeps sending till eye tracker picks it
                message='trialStop&End'
                s.sendto(message.encode('utf-8'),server)
                # recv feedback msg
                (data,addr)=s.recvfrom(1024)
                data=data.decode('utf-8')
                if data=='q':
                    break
            except BlockingIOError:
                continue  
    elif multiple_machines==False and 'escape' not in key_pressed:
        connSender.send("trialStop") 
s.close()
# Remove eye pos file if it already exists
try:
    os.remove('.\OnlineSaccadeDetection\eyepos_rubbereye_saccade_monitorSync.csv')
except FileNotFoundError:
    print('File not there')
# write out the new pandas csv file

if gaze_contingent==False:
    # we close the socket program
    if multiple_machines==False:
        connListener.close()
        listener.close()
        connSender.close()
        print('connection closed')

window.close()
core.quit()
