from dataclasses import asdict
from platform import machine
from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import customTime
import random
from random import randint
from psychopy import logging
from psychopy.sound import Sound
from psychopy import prefs
prefs.general['audioLib'] = ['pygame']
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from PixelConversion import PixelConversion
import pixlet
import online_sac_detect_module
import serial 
from SendDataFunction import TSLOsender 
from SendDataFunction import TSLOsenderlong
from ReceiveDataFunction import TSLOreceiver
from multiprocessing.connection import Listener
from multiprocessing.connection import Client
from make_mask import make_mask
from recvAndPlotImageParams import recvAndPlotImageParams  
import itertools
import parallel64
import socket
from SendDataFunction import stimMachineSender
from pylsl import StreamInfo, StreamOutlet
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
eye_tracker=False
lsl_connection=False
keep_repeating=True
cam_connection=True
imageParamsLSL=False
no_microsaccade_blocked=False
trial_validity_randomization=True
# Also set individual subject parameters
saccade_latency_param={1:0.300,2:0.190,3:0.400,4:0.200,5:0.230,6:0.200,7:0.225}
target_duration_param={1:4,2:6,3:5,4:8,5:7,6:6,7:5}
# We set the spacing with 80-90% performance based on expt 1 [all 4 flankers]
thresh_spacing={1:3,2:2.25,3:2,4:2,5:2,6:2,7:2}
# Display trigger options
# 1. Parallel port (server) : "Pport"
# 2. Digital out via NI card (old machine): "Dout"
# 3. Display triggers via photocell: "photocell"
trig_opt='Pport'
# Saccade trigger options
# 1. Parallel port (server) : "Pport"
# 2. Socket connection between programs(old machine): "socket"
saccade_trigger_comm='Pport'
# We can indicate whether we are using the socket on the same machine or not
# when using multiple machines we only send experiment info to the eye tracker system so we 
# only need a socket to send messages
multiple_machines=True
if multiple_machines==True:
    # We would receive saccade messages over parallel port to improve efficiency
    parallel_port=True
    # We would initially have an active LSL connection between machines
    machine_connection=True
    # We can decide whether or not to send stimulus location info to the eye tracker code
    stimLocationCond=False
else:
    parallel_port=False
    machine_connection=False
    stimLocationCond=False
#===================================================================================================================================
## First we set up the parameters to set up hardware

#clock = psychopy.core.Clock()
# Equipment setup
monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
    
if eye_tracker==True:
    if multiple_machines==False:
        ## Setup listener
        # This would listen to saccade messages
        # Socket connection setup
        listenAddress=('localhost',6000)
        # family is deduced to be 'AF_INET'
        print('Looking for connection')
        listener = Listener(listenAddress, authkey=b'secret password')
        connListener = listener.accept()
        ## Setup Sender
        # This would send stimulus time and final close message to terminate all sockets
        # Set up the parameters for the socket
        sendAddress = ('localhost', 6001)
        while True:
            try:
                connSender = Client(sendAddress, authkey=b'secret password')
                break
            except ConnectionRefusedError:
                time.sleep(0.1)
                continue
        print('Connection established!')
    else:
        print('Looking for eye tracker!')
        # We setup LSL once
        info = StreamInfo('ASUS', 'Server', 1, 100, 'string', 'myuid34234')
        # next make an outlet
        outlet = StreamOutlet(info)
        print('Connection established!')

        #Also set up socket for trial status messages
        # The stimulus sytem would be the client
        host='192.168.0.101' #system ip
        port=4005
        server=('192.168.0.143',4000)
        s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        s.bind((host,port))
        s.setblocking(0)
# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we the get the pixel conversion value
pixel_per_degree,pixel_per_cm=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
msg_size=pixel_per_degree/5
msg_offset=400

# Fixation target could be either cross or plus with circle in the middle
# Options: 1: "x" or 2" "+"
fixation_type='x'

if fixation_type=="+":
    fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
elif fixation_type=='x':
    fix_line_UR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
fix_c=visual.Circle(window, size=8, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(-1,-1,-1) )

# Additionally,we set up the camera feature,so that subjects can visualize the retinal image quality and the PMT gain setting.
# For this to work the webcam would have to be setup such that it faces the retinal image window on ICANDI and the PMT box with the 
# gain setting
if cam_connection==True:
    from ximea import xiapi
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[1600,1600],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()
#===================================================================================================================================
# We also open the parallel port connection
p=parallel64.StandardPort(spp_base_address=0x3efc)
while keep_repeating==True:
    #Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            startup_msg1=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)
            startup_msg1.draw()
            startup_msg2.draw()
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                    stim1.draw()
            window.flip()

            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    if cam_connection==True:
                        webcam_state=True                
                        #start data acquisition
                        cam.start_acquisition()
                        while webcam_state==True & cam_connection==True:
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                    stim1.draw()
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()                       
                    elif cam_connection==False and imageParamsLSL==True:
                        while True:
                            imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                            #break when we get a keypress
                            keys=event.getKeys()
                            
                            if len(keys) != 0:
                                cv2.destroyAllWindows()
                                break
    # We can also build this list realtime
    sacc_latency_list=[]
    # we stop if escape is pressed
    if 'keys' in locals() and lsl_connection==True:
        if keys[0]=='escape':
            while True:
                try:
                    # Keeps sending till eye tracker picks it
                    message='q'
                    s.sendto(message.encode('utf-8'),server)
                    # recv feedback msg
                    (data,addr)=s.recvfrom(1024)
                    data=data.decode('utf-8')
                    if data=='q':
                        # set a variable to false to indicate the LSL connection is closed
                        machine_connection=False
                        break
                except BlockingIOError:
                    pass  
            keep_repeating=False
            continue
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        protocol_packet=TSLOreceiver()
        stimulus_packet=TSLOreceiver()
        # These parameters are provided from TSLO machine
        oblique_cond=stimulus_packet[0]
        flanker_cond=stimulus_packet[1]
        num_repeats=protocol_packet[2] 
        # This would be the case where we are repeating specific conditions
        if num_repeats==1:
            flanker_info=protocol_packet[3]
            #Note for this the subject ID would have to be hardcoded
            subj_id=1
        else:
            # Otherwise 3rd info corresponds to subject id
            subj_id=protocol_packet[3]

        if no_microsaccade_blocked==True:
            # we then decide if we want trials with or w/o microsaccades
            no_microsaccade_status=protocol_packet[1]
        horizontal_sacc=protocol_packet[0]

    else: 
        # If LSL is false set these parameters locally to their defaults
        #no_microsacc_cond=1 # Sets whether it should stop with or w/o microsaccades
        subj_id=3
        flanker_info=100
        num_repeats=5 # Sets the number of repeats for each flanker spacing at a given ecc
        if no_microsaccade_blocked==True:
            no_microsaccade_status=False
        oblique_cond=0
        #flanker_cond: 0 for both rad/tang flankers; 1: for radial and 2: for tangential
        flanker_cond=0
        # Horizontal sacc: 0 for similar target and saccade location; 1 for different locations
        horizontal_sacc=True

    # These parameters are set locally (Stimulus Machine)
    eccentricity_deg=0.33  #0.53#0.33
    cue_condition=True
    mask_condition=False
    location_uncertainity=True 
    reverse_flanker_contrast=False
    save_image=True
    foveal_mode=False
    # Testing other spacings for no sacc/control condition
    other_spacing_nosacc=False

    # Specify whether or not a peripheral cue needs to be displayed
    if foveal_mode==True:
        peripheral_cue=False
    else:
        peripheral_cue=True
    # This would apply for the oblique microsaccade experiment
    neutral_trials=False
    if neutral_trials==True:
        # When we have neutral trials we may need to block microsaccade & no microsaccade
        # trials. Options to set here :
        #1. 'microsaccade'
        #2. 'nomicrosaccade'
        microsaccade_cond_with_neutral_trials='microsaccade'
    # for oblique target locations we set whether all target locations would have
    # place holder
    
    behav_output=True
    if flanker_cond==0:
        meridian_anisotropy_cond=False
    else:
        meridian_anisotropy_cond=True
    if meridian_anisotropy_cond==True:
        # Could be radial/R (horizontal) or tangential/T (vertical) flanker locations
        if flanker_cond==1:
            meridian='radial'
        elif flanker_cond==2:
            meridian='tangential'
    #
    # We also get the SOA for individual subject from the dictionary
    saccade_latency_median=saccade_latency_param[subj_id]

    target_type='T'

    if target_type=='T':
        # we can flankers to be either T's or +'s
        flanker_type='+'

    # These parameters would be set at the start of the experiment and ideally would not want to change them 
    # after the start of the experiment
    #Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
    # on the fixation cross for variable duration before stimulus onset
    # Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
    target_presentation=target_duration_param[subj_id] #numFlips

    if foveal_mode==True:
        flanker_space=['oblique1_horizontal','oblique2_vertical'] #'radial','tangential']
    else:
        flanker_space=['radial','tangential']#,'oblique1_horizontal','oblique2_vertical']
        
    # We also have a bunch of Stimulus onset asynchrony values(SOA) as a
    # list that we loop over. This would determine the offset between
    # saccade cue and stimulus onset. Also would help us get saccades at
    # different time points following stimulus onset
    # Note: Values are in milliseconds 
    if other_spacing_nosacc==True:
        flanker_values=[1.5,2.5]
        flanker_meridian_spacing_combinations=list(itertools.product(flanker_space,flanker_values))
        # now we put it back into the original lists
        flanker_meridian=[]
        flanker_space=[]
        for acombination in flanker_meridian_spacing_combinations:
            flanker_meridian.append(acombination[0])
            flanker_space.append(acombination[1])
        flanker_meridian.append('unflanked')
        flanker_space.append(100)
        # We also set SOA values
        SOA_values=[0.025,0.050,0.075]
    else:
        SOA_values=[0.025,0.050,0.075]
        # we creat combinations of spacing and SOA
        flanker_SOA_combinations=list(itertools.product(flanker_space,SOA_values))

        # now we put it back into the original lists
        flanker_space=[]
        SOA_values=[]
        for aCombination in flanker_SOA_combinations:
            flanker_space.append(aCombination[0])
            SOA_values.append(aCombination[1])
    if no_microsaccade_blocked==True:
        # This would be no microsaccade block
        if no_microsaccade_status==True:
            # No microsaccade condition
            flanker_spacing_list=np.repeat(flanker_space,num_repeats)
            microsacc_status_list=np.ones(len(flanker_space)*int(num_repeats))
            SOA_list=np.repeat(SOA_values,num_repeats)
        # this would be microsaccade block
        else:
            #Microsaccade condition:
            flanker_spacing_list=np.repeat(flanker_space,num_repeats)
            microsacc_status_list=np.zeros(len(flanker_space)*int(num_repeats))
            SOA_list=np.repeat(SOA_values,num_repeats)
        random.shuffle(flanker_spacing_list)
        random.shuffle(SOA_list)
        flanker_spacing_list=list(flanker_spacing_list)
        microsacc_status_list=list(microsacc_status_list)
        SOA_list=list(SOA_list)
    else:
        # When it is interleaved it can either be with the no microsaccade condition
        # or it can be with neutral trials
        if neutral_trials==True:
            if microsaccade_cond_with_neutral_trials=='microsaccade':
                # The neutral trials would be interleaved with the microsaccade condition
                # Only a small proportion would be neutral trials
                if num_repeats>1:
                    prop_neutral=0.25 # 25% of trials
                    prop_sacc=1- prop_neutral
                    num_repeats_neutral=num_repeats * prop_neutral
                    num_repeats_sacc=num_repeats * prop_sacc
                    if int(num_repeats_neutral)!=num_repeats_neutral: # would help catch cases where num repeats is a float
                        num_repeats_neutral=1
                        num_repeats_sacc=1
                    #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                    # we create separate flanker spacing list for each of the conditions
                    #Microsaccade condition:
                    flanker_spacing_list_sacc=np.repeat(flanker_space,num_repeats_sacc)
                    microsacc_cond=np.zeros(len(flanker_space)*int(num_repeats_sacc))
                    # Neutral condition
                    flanker_spacing_list_neutral=np.repeat(flanker_space,num_repeats_neutral)
                    # We would add two arrays to get two's which would represent the neutral condition
                    neutral_cond=np.ones(len(flanker_space)*int(num_repeats_neutral))+np.ones(len(flanker_space)*int(num_repeats_neutral))
                    # We create a list for the different stimulus onset asynchronies
                    SOA_list=np.repeat(SOA_values,num_repeats)
                    # we would concatenate both lists and shuffle them and this what would be used 
                    # in the for loop of the main experiment
                    flanker_spacing_list=np.concatenate([flanker_spacing_list_sacc,flanker_spacing_list_neutral])
                    microsacc_status_list=np.concatenate([microsacc_cond,neutral_cond])
                    temp_list=list(zip(flanker_spacing_list,microsacc_status_list))
                    random.shuffle(temp_list)
                    flanker_spacing_list,microsacc_status_list=zip(*temp_list)
                    flanker_spacing_list=list(flanker_spacing_list)
                    microsacc_status_list=list(microsacc_status_list)
                    SOA_list=list(SOA_list)
            elif microsaccade_cond_with_neutral_trials=='nomicrosaccade':
                # The no microsaccade condition would be interleaved with the microsaccade condition
                # Only a small proportion of trials would fall under the no microsaccade condition
                if num_repeats>1:
                    prop_neutral=0.25 # 25% of trials
                    prop_nosacc=1-prop_neutral
                    num_repeats_neutral=num_repeats * prop_neutral
                    num_repeats_nosacc=num_repeats
                    if int(num_repeats_neutral)!=num_repeats_neutral: # would help catch cases where num repeats is a float
                        num_repeats_neutral=1
                        num_repeats_nosacc=1
                    #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                    # we create separate flanker spacing list f
                    # 5
                    # 5or each of the conditions
                    #Microsaccade condition:
                    flanker_spacing_list_nosacc=np.repeat(flanker_space,num_repeats_nosacc)
                    microsacc_cond=np.ones(len(flanker_space)*int(num_repeats_nosacc))
                    # Neutral condition
                    flanker_spacing_list_neutral=np.repeat(flanker_space,num_repeats_neutral)
                    # We would add two arrays to get two's which would represent the neutral condition
                    neutral_cond=np.ones(len(flanker_space)*int(num_repeats_neutral))+np.ones(len(flanker_space)*int(num_repeats_neutral))
                    # We create a list for the different stimulus onset asynchronies
                    SOA_list=np.repeat(SOA_values,num_repeats)
                    # we would concatenate both lists and shuffle them and this what would be used 
                    # in the for loop of the main experiment
                    flanker_spacing_list=np.concatenate([flanker_spacing_list_nosacc,flanker_spacing_list_neutral])
                    microsacc_status_list=np.concatenate([microsacc_cond,neutral_cond])
                    temp_list=list(zip(flanker_spacing_list,microsacc_status_list))
                    random.shuffle(temp_list)
                    flanker_spacing_list,microsacc_status_list=zip(*temp_list)
                    flanker_spacing_list=list(flanker_spacing_list)
                    microsacc_status_list=list(microsacc_status_list)
                    SOA_list=list(SOA_list)
        else:
            # When not using neutral trials we can interleave microsaccade trials 
            # with no microsaccade trials
            if num_repeats>1:
                if foveal_mode==True:
                    prop_nomicrosaccade=1 # Only no microsaccade trials
                elif horizontal_sacc==True:
                    prop_nomicrosaccade=0 # Only microsaccade trials
                elif other_spacing_nosacc==True:
                    prop_nomicrosaccade=1 # only no microsaccade trials
                else:
                    prop_nomicrosaccade=0.25 # 50 % of trials / 0.25 # 25% of trials
                prop_microsaccade= 1- prop_nomicrosaccade # 75% of trials
                num_repeats_nomicrosaccade=num_repeats * prop_nomicrosaccade
                num_repeats_microsaccade=num_repeats * prop_microsaccade
                # we also split based on trial validity
                prop_invalid=0#/0.50 # 50% of saccade trials
                # No microsaccade condition
                num_repeats_invalid_nomicrosaccade=num_repeats_nomicrosaccade * prop_invalid
                num_repeats_valid_nomicrosaccade=num_repeats_nomicrosaccade * (1-prop_invalid)
                # microsaccade condition
                num_repeats_invalid_microsaccade=num_repeats_microsaccade * prop_invalid
                num_repeats_valid_microsaccade=num_repeats_microsaccade * (1-prop_invalid)
                # we would then build a list with the valid & invalid conditions
                # separately for microsaccade & no microsaccade condition
                #no microsaccade condition
                valid_cond_no_microsaccade=np.ones(int(num_repeats_valid_nomicrosaccade) * len(flanker_space))
                invalid_cond_no_microsaccade=np.zeros(int(num_repeats_invalid_nomicrosaccade) * len(flanker_space))
                congruency_cond_list_no_microsaccade=np.concatenate([valid_cond_no_microsaccade,invalid_cond_no_microsaccade])
                # do the same for flanker spacing lists & SOA lists
                flanker_spacing_list_no_microsaccade_valid=np.repeat(flanker_space,num_repeats_valid_nomicrosaccade)
                flanker_spacing_list_no_microsaccade_invalid=np.repeat(flanker_space,num_repeats_invalid_nomicrosaccade)
                flanker_spacing_list_no_microsaccade=np.concatenate([flanker_spacing_list_no_microsaccade_valid,flanker_spacing_list_no_microsaccade_invalid])
                if other_spacing_nosacc==True:
                    flanker_meridian_no_microsaccade_valid=np.repeat(flanker_meridian,num_repeats_valid_nomicrosaccade)
                    flanker_meridian_no_microsaccade_invalid=np.repeat(flanker_meridian,num_repeats_invalid_nomicrosaccade)
                    flanker_meridian_no_microsaccade=np.concatenate([flanker_meridian_no_microsaccade_valid,flanker_meridian_no_microsaccade_invalid])
                else:
                    SOA_list_no_microsaccade_valid=np.repeat(SOA_values,num_repeats_valid_nomicrosaccade)
                    SOA_list_no_microsaccade_invalid=np.repeat(SOA_values,num_repeats_invalid_nomicrosaccade)
                    SOA_list_no_microsaccade=np.concatenate([SOA_list_no_microsaccade_valid,SOA_list_no_microsaccade_invalid])
                # microsaccade condition
                valid_cond_microsaccade=np.ones(int(num_repeats_valid_microsaccade) * len(flanker_space))
                invalid_cond_microsaccade=np.zeros(int(num_repeats_invalid_microsaccade) * len(flanker_space))
                congruency_cond_list_microsaccade=np.concatenate([valid_cond_microsaccade,invalid_cond_microsaccade])
                # do the same for flanker spacing lists & SOA lists
                flanker_spacing_list_microsaccade_valid=np.repeat(flanker_space,num_repeats_valid_microsaccade)
                flanker_spacing_list_microsaccade_invalid=np.repeat(flanker_space,num_repeats_invalid_microsaccade)
                flanker_spacing_list_microsaccade=np.concatenate([flanker_spacing_list_microsaccade_valid,flanker_spacing_list_microsaccade_invalid])
                if other_spacing_nosacc==True:
                    flanker_meridian_microsaccade_valid=np.repeat(flanker_meridian,num_repeats_valid_microsaccade)
                    flanker_meridian_microsaccade_invalid=np.repeat(flanker_meridian,num_repeats_invalid_microsaccade)
                    flanker_meridian_microsaccade=np.concatenate([flanker_meridian_microsaccade_valid,flanker_meridian_microsaccade_invalid])
                else:
                    SOA_list_microsaccade_valid=np.repeat(SOA_values,num_repeats_valid_microsaccade)
                    SOA_list_microsaccade_invalid=np.repeat(SOA_values,num_repeats_invalid_microsaccade)
                    SOA_list_microsaccade=np.concatenate([SOA_list_microsaccade_valid,SOA_list_microsaccade_invalid])
                #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
                # we create separate flanker spacing list for each of the conditions
                #Microsaccade condition:
                microsacc_cond=np.zeros(len(flanker_space)*int(num_repeats_microsaccade))
                nomicrosacc_cond=np.ones(len(flanker_space) * int(num_repeats_nomicrosaccade))
                # We would shuffle each list separately and then concatenate
                # no microsaccade condition
                try:
                    if other_spacing_nosacc==True:
                        temp_list_no_microsaccade=list(zip(flanker_spacing_list_no_microsaccade,flanker_meridian_no_microsaccade,congruency_cond_list_no_microsaccade))
                        random.shuffle(temp_list_no_microsaccade)
                        flanker_spacing_list_no_microsaccade,flanker_meridian_no_microsaccade,congruency_cond_list_no_microsaccade=zip(*temp_list_no_microsaccade)
                        flanker_spacing_list_no_microsaccade=list(flanker_spacing_list_no_microsaccade)
                        congruency_cond_list_no_microsaccade=list(congruency_cond_list_no_microsaccade)
                        flanker_meridian_no_microsaccade=list(flanker_meridian_no_microsaccade)
                    else:
                        temp_list_no_microsaccade=list(zip(flanker_spacing_list_no_microsaccade,SOA_list_no_microsaccade,congruency_cond_list_no_microsaccade))
                        random.shuffle(temp_list_no_microsaccade)
                        flanker_spacing_list_no_microsaccade,SOA_list_no_microsaccade,congruency_cond_list_no_microsaccade=zip(*temp_list_no_microsaccade)
                        flanker_spacing_list_no_microsaccade=list(flanker_spacing_list_no_microsaccade)
                        congruency_cond_list_no_microsaccade=list(congruency_cond_list_no_microsaccade)
                        SOA_list_no_microsaccade=list(SOA_list_no_microsaccade)
                except ValueError:# this would happen for horizontal saccade block without no microsaccade trials
                    pass
                # microsaccade condition
                try:
                    if other_spacing_nosacc==True:
                        temp_list_microsaccade=list(zip(flanker_spacing_list_microsaccade,flanker_meridian_microsaccade,congruency_cond_list_microsaccade))
                        random.shuffle(temp_list_microsaccade)
                        flanker_spacing_list_microsaccade,flanker_meridian_microsaccade,congruency_cond_list_microsaccade=zip(*temp_list_microsaccade)
                        flanker_spacing_list_microsaccade=list(flanker_spacing_list_microsaccade)
                        congruency_cond_list_microsaccade=list(congruency_cond_list_microsaccade)
                        flanker_meridian_microsaccade=list(flanker_meridian_microsaccade)
                    else:
                        temp_list_microsaccade=list(zip(flanker_spacing_list_microsaccade,SOA_list_microsaccade,congruency_cond_list_microsaccade))
                        random.shuffle(temp_list_microsaccade)
                        flanker_spacing_list_microsaccade,SOA_list_microsaccade,congruency_cond_list_microsaccade=zip(*temp_list_microsaccade)
                        flanker_spacing_list_microsaccade=list(flanker_spacing_list_microsaccade)
                        congruency_cond_list_microsaccade=list(congruency_cond_list_microsaccade)
                        SOA_list_microsaccade=list(SOA_list_microsaccade)
                except ValueError:# this would happen for foveal mode without microsaccade condition
                    pass
    #===================================================================================================================================
    ## Create basic shapes
    # These would be drawn & /or manipulated during the main experiment
    # The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
    if location_uncertainity==True:
        #stim_location_list=['left','right']
        stim_location_list=['lowerLeft']#,'lowerRight']
    else:
        stim_location_list=['right']
    fix_xpos=0
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line

    #Create basic stimuli here that could be called multiple times during the course of the experiment
    small_square=visual.Rect(window,width=128,height=128,units='pix',fillColor='white', fillColorSpace='rgb',pos=(975,1625))
    wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start the experiment')
    wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

    # The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
    cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    # Cue for rightward target location
    cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    #cue for leftward target location
    cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    # Cue for voluntary microsaccade
    cue_c=visual.Circle(window, size=4, fillColor=(0,0,0), pos=(0,0), lineColor=(0,0,0) )
    # Cue for target location
    target_c=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    # For oblique targets presented at 1 of 4 locations we have separate cues
    target_c_ll=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_lr=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_fov=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    # We additionally have another saccade landing site that is different from target location
    saccade_c_h=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )

    #we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
    target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="T",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
    flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1],height=pixel_per_degree )
    flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="F",color=[-1,-1,-1] ,height=pixel_per_degree)
        
    # This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
    # The letter size is kept constant and is set to 1.25x the threshold from the isolated letter threshold measures.
    # To ensure letter size is supra threshold
    letter_height=6.25/60#(6.25/60) #corresponds to 6.25 arc min or 20/25
#    letter_height=(params[subj_id][eccentricity_deg])*1.25
    letter_height_pix=letter_height*pixel_per_degree

    # we create a mask that would cover both the stimulus and the flankers
    # Note: here we use a noise mask over the number mask, to make it a little easier to do the task/ less effect
    # Here the mask is just a random binary image that would change on every trial
    # Mask parameters
    mask_height=75
    mask_width=75
    # Create binary numpy array
    mask_array=np.random.randint(2,size=(mask_height,mask_width))
    mask_object=visual.ImageStim(window,image=mask_array,units='pix',size=(mask_width,mask_height),colorSpace='rgb')
    #===================================================================================================================================
    ## Experimental sequence:

    #We use the position of the fixation cross center to position the target and flankers
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 
    # Also we would alter the eccentricity for oblique locations
    # otherwise for horizontal locations we would do nothing
    if 'left' not in stim_location_list or 'right' not in stim_location_list:
        # To get the target at 0.33 deg we would need the legs of the triangle (pythagoras theorem)
        # to be set at different points
        eccentricity_deg=np.sqrt(eccentricity_deg**2/2)
        # In case of a separate saccade target along the horizontal meridian we would want it located at the same 
        # effective location from the target but on the other side
        # For eg: if we have the target located to the lower left from fixation, we would have the separate saccade target site 
        # located to right from fixation/upper right from target at same eccentricity from the target
        # So would be twice the base of the triangle drawn between the fixation point and target, with target ecc along the hypotenuse
        eccentricity_deg_saccade_c_h=eccentricity_deg*2

    #Have something that waits for a key press from the subject. This essentially would initiate the sync between the stimulus and 
    #imaging machines and also start the experiment. 
    if lsl_connection==True:
        keepGoing=True
        while keepGoing:
            stream_wait1=visual.TextStim(window,text=('Waiting for TSLO machine'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
            stream_wait2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)

            stream_wait1.draw()
            stream_wait2.draw()
                
            if fixation_type=='+':
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            elif fixation_type=='x':
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                    stim1.draw()
            window.flip()
                
            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        data_packet=TSLOreceiver()
       
    #set a timer to get stimulus timing
    timerDuration=2 #set for 1 minute for now since trials would be shorter than this
    timer = core.CountdownTimer(timerDuration)

    #===================================================================================================================================
    ## Start of main loop of experiment
    # We would get this from the TSLO machine and would help number the files 
    # consistently across both machines
    if lsl_connection==True:
        repeatCounterStart=data_packet[2]
    else:
        repeatCounterStart=0
    # We block saccade conditions: We either have microsaccade trials first/no microsaccade trials first
    if foveal_mode==True:
        block_status_decider=1
        num_loop=1
    elif horizontal_sacc==True:
        block_status_decider=0
        num_loop=1
    elif other_spacing_nosacc==True:
        block_status_decider=1
        num_loop=1
    else:
        block_status_decider=random.randint(0,1)
        num_loop=2

    for conditionIdx in range(num_loop):
        flanker_counter=1 # would be used to identify the last flanker condition
        trial_validity_counter=1 # this would be used to run through SOA & congruency list
        if block_status_decider==0: # we do microsaccade trials first
            if conditionIdx==0:
                flanker_spacing_list=list(flanker_spacing_list_microsaccade)
                SOA_list=list(SOA_list_microsaccade)
                microsacc_status_list=list(microsacc_cond)
                congruency_cond_list=list(congruency_cond_list_microsaccade)
            else:
                flanker_spacing_list=list(flanker_spacing_list_no_microsaccade)
                SOA_list=list(SOA_list_no_microsaccade)
                microsacc_status_list=list(nomicrosacc_cond)
                congruency_cond_list=list(congruency_cond_list_no_microsaccade) 
            flanker_loop_list=flanker_spacing_list
        elif block_status_decider==1:
            if conditionIdx==0:
                flanker_spacing_list=list(flanker_spacing_list_no_microsaccade)
                microsacc_status_list=list(nomicrosacc_cond)
                congruency_cond_list=list(congruency_cond_list_no_microsaccade) 
                if other_spacing_nosacc==True:
                    flanker_meridian_list=list(flanker_meridian_no_microsaccade)
                    flanker_loop_list=flanker_meridian_list
                else:
                    SOA_list=list(SOA_list_no_microsaccade)
                    flanker_loop_list=flanker_spacing_list
            else:
                flanker_spacing_list=list(flanker_spacing_list_microsaccade)
                flanker_loop_list=flanker_spacing_list
                SOA_list=list(SOA_list_microsaccade)
                microsacc_status_list=list(microsacc_cond)
                congruency_cond_list=list(congruency_cond_list_microsaccade)
        if conditionIdx!=0:
            # We would show a message when subjects finish the first condition
            breakMsgText=visual.TextStim(window,text=('Condition complete\n Take a short break'),pos=(0.0,0.0),color=[1,1,1],height=msg_size)
            core.wait(0.500) # wait for 500 ms
            keepGoing=True
            while keepGoing:
                breakMsgText.draw()
                window.flip()
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    while True:
                        try:
                            # Keeps sending till eye tracker picks it
                            message='q'
                            s.sendto(message.encode('utf-8'),server)
                            # recv feedback msg
                            (data,addr)=s.recvfrom(1024)
                            data=data.decode('utf-8')
                            if data=='q':
                                # set a variable to false to indicate the LSL connection is closed
                                machine_connection=False
                                break
                        except Exception as e:
                            print (e)
                            break  
                    keepGoing=False
                    keep_repeating=False
        for aflanker in flanker_loop_list:
            #This is used to provide a audio feedback to the user
            beep_sound_correct=Sound(value=1000,secs=0.25)
            beep_sound_incorrect=Sound(value=250,secs=0.25)
            # For repeats/custom trials we have matlab set the parameters otherwise
            # psychopy controls the set of flankers and microsaccade condition
            if num_repeats>1:
                # We set the microsaccade condition based on the flanker counter index
                no_microsacc_cond=microsacc_status_list[flanker_counter-1]
                if neutral_trials==True:
                    if no_microsacc_cond==2: # This would be neutral trials
                        SOA=np.random.choice(SOA_list)
                        # We would have target congruency to be one always
                        target_congruency_cond=1
                    else: # Would be for saccade and no microsaccade conditions
                        # we set the SOA values based on flanker counter index
                        SOA=SOA_list[trial_validity_counter-1]
                        #Likewise we set the congruency condition based on the flanker counter index
                        target_congruency_cond=congruency_cond_list[trial_validity_counter-1]
                else:
                    # we set the SOA values based on flanker counter index
                    if other_spacing_nosacc==True:
                        flanker_spacing=flanker_spacing_list[trial_validity_counter-1]
                        SOA=np.random.choice(SOA_values)
                    else:
                        SOA=SOA_list[trial_validity_counter-1]
                    #Likewise we set the congruency condition based on the flanker counter index
                    target_congruency_cond=congruency_cond_list[trial_validity_counter-1]
                
            # Check if escape was pressed and stop loop
            if 'key_pressed' in locals():
                if key_pressed=='escape':
                    while True:
                        try:
                            # Keeps sending till eye tracker picks it
                            message='q'
                            s.sendto(message.encode('utf-8'),server)
                            # recv feedback msg
                            (data,addr)=s.recvfrom(1024)
                            data=data.decode('utf-8')
                            if data=='q':
                                # set a variable to false to indicate the LSL connection is closed
                                machine_connection=False
                                break
                        except BlockingIOError:
                            pass  
                    keep_repeating=False
                    break

            # we create a new variable that would hold the value for flanker spacing 
            #condition
            flanker_val_str=aflanker
            # If testing with saccade target elsewhere we add that to filename/spacing string
            if horizontal_sacc==True:
                flanker_val_str="%s_horizontal_saccade"%(flanker_val_str)
            # If testing other flanker spacings we create another string for spacing value
            if other_spacing_nosacc==True:
                if int(flanker_spacing) % float(flanker_spacing)==0: 
                    flanker_spacing_val=int(flanker_spacing)
                    flanker_spacing_str='%d'%(flanker_spacing_val)
                else:
                    flanker_spacing_val=float(flanker_spacing)
                    flanker_spacing_str='%.2f'%(flanker_spacing_val)
            # based on this we set the validity string to be used to write to output file
            if neutral_trials==True:
                if no_microsacc_cond!=2:
                    if target_congruency_cond==1:
                        trialValidity='valid'
                    elif target_congruency_cond==0:
                        trialValidity='invalid'
                else: # These would be neutral trials
                    trialValidity='neutral'
            else:
                # otherwise it would depend on congryency condition
                if target_congruency_cond==1:
                    trialValidity='valid'
                elif target_congruency_cond==0:
                    trialValidity='invalid'

            # 1. We create the behavioral output file
            # We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
            # accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
            # data
            #output filename
            file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())
            if lsl_connection==True:
                if other_spacing_nosacc==True:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_%s_condition_%s_SOA_%.3f_%s'%((data_packet[0]),(data_packet[1]),repeatCounterStart, eccentricity_deg,
                                flanker_spacing_str,flanker_val_str,SOA,trialValidity)
                else:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_condition_%s_SOA_%.3f_%s'%((data_packet[0]),(data_packet[1]),repeatCounterStart, eccentricity_deg,
                                flanker_val_str,SOA,trialValidity)
                # we add an additional component if it is an oblique condition
                if oblique_cond==True:
                    trial_info="%s_oblique"%(trial_info)
                if no_microsacc_cond==1:
                    outfilename = ("results/%s_NoMicrosaccade_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==0:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==2:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                timerDuration=data_packet[3]
            else:
                if other_spacing_nosacc==True:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_%s_condition_%s'%(subj_id,2,0,eccentricity_deg, flanker_spacing_str,flanker_val_str)
                else:
                    trial_info='S0%d_S%d_T00%d_ecc_%.2f_spacing_condition_%s'%(subj_id,2,0,eccentricity_deg,flanker_val_str)
                if no_microsacc_cond==1:
                    outfilename = ("results/%s_NoMicrosaccade_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
                elif no_microsacc_cond==0:
                    outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
            if behav_output==True:
                outfile = open(outfilename, "a")
                
            if behav_output==True:
                #Add the header 
                if other_spacing_nosacc==True:
                    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","spacing_value","stim_loc",
                                    "cue_loc","cueDuration","stim_dur","corr","stimTime","trialValidity") )
                else:
                    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
                                 "cue_loc","cueDuration","stim_dur","corr","stimTime","trialValidity") )

            event.clearEvents()
            #The letter size is set here
            letter_size_deg=letter_height
            letter_size_pix=letter_size_deg*pixel_per_degree
            target.height=letter_size_pix
            flanker_left.height=letter_size_pix
            flanker_right.height=letter_size_pix
            flanker_up.height=letter_size_pix
            flanker_down.height=letter_size_pix
            
            # The following set of lines set the flanker condition 
            stim_location=np.random.choice(stim_location_list)
            eccentricity_pix=int(eccentricity_deg*pixel_per_degree)
            
            # Can either be left or right
            if stim_location=='left':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentrcitiy_y=0
            elif stim_location=='right':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=0
            # Or can be along 4 diagonal meridians from fixation
            elif stim_location=='lowerLeft':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix
            elif stim_location=='lowerRight':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix

            #We would send stimulus location to the eye tracking code
            if eye_tracker==True and stimLocationCond==True:
                if stim_location=='lowerLeft':
                    if multiple_machines==True:
                        if machine_connection==True:
                            message="stimLeft"
                            stimMachineSender(message,info,outlet,commSetup=False)
                    else:
                        connSender("stimLeft")
                elif stim_location=='lowerRight':
                    if multiple_machines==True:
                        if machine_connection==True:
                            message="stimRight"
                            stimMachineSender(message,info,outlet,commSetup=False)
                    else:
                        connSender("stimRight")   
            # We set the position of the target placeholder using the eccentricity provided
            # and the location of the fixation cross
            target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            # If testing at same location just overlap target circles
            #target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            target_c_lr.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            # When having subjects to a different location we set the position of the saccade landing site wrt to the stimulus
            if horizontal_sacc==True:
                eccentricity_pix_saccade_c_h=int(eccentricity_deg_saccade_c_h*pixel_per_degree)
                saccade_c_h.pos=((fixation_pos[0]-eccentricity_pix_saccade_c_h),0)
            # we then set the target location randomly based on the different location choices
            # available 
            if foveal_mode==True:
                target_location=(0,0)
            else:
                target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 

            # We would overwrite this if we want oblique targets
            if oblique_cond==True:
                target_location=(eccentricity_x,-eccentricity_y)

            target.pos=target_location
            # We would make sure we offset cue location horizontally and not vertically
            # important when doing oblique conditions
            if stim_location!='left' or stim_location!='right':
                cue_c.pos=target_location            
            else:
                cue_c.pos=(target_location[0],0)
            # we would drop another marker for target location
            if oblique_cond==True:
                target_c.pos=target_location
            
            # First we would reset fixation cross to original color
            fix_line_UL.color=[-1,-1,-1]
            fix_line_UR.color=[-1,-1,-1]
            fix_line_LL.color=[-1,-1,-1]
            fix_line_LR.color=[-1,-1,-1]
            # We also change which part of the fixation cross is highlighted based on where the
            # target would go on to appear
            # can be along 4 diagonal meridians from fixation
            # We would do this based on congruency or validty of trial
            if trial_validity_randomization==True:
                if target_congruency_cond==0: # incongruent/invalid trials
                    if stim_location=='lowerLeft':
                        remaining_target_locs=["lowerRight"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    elif stim_location=='lowerRight':
                        remaining_target_locs=["lowerLeft"]
                        locs_choice=np.random.choice(remaining_target_locs)
                    
            # Based on condition we set the cue congruency string
            if target_congruency_cond==1:
                cueLocation=stim_location
            elif target_congruency_cond==0:
                cueLocation=locs_choice
                
            #THe flanker spacing is set on eacn trial based on the stimulus size
            # We would have separate spacings for testing meridian and opposite meridian
            if other_spacing_nosacc==True:
                flanker_spacing_testing_meridian=flanker_spacing
            else:
                flanker_spacing_testing_meridian=1.25
            flanker_deg_testing_meridian=flanker_spacing_testing_meridian*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix_testing_meridian=int(flanker_deg_testing_meridian*pixel_per_degree)
            # Set the same for the opposite meridian
            flanker_spacing_opp_meridian=100
            flanker_deg_opp_meridian=flanker_spacing_opp_meridian*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix_opp_meridian=int(flanker_deg_opp_meridian*pixel_per_degree)
            if aflanker=='radial': #[Radial flankers tested; Tangential flankers not tested]
                if stim_location=='lowerLeft':
                    # Flankers being tested are set first
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
                    # Flankers not being tested
                    flanker_up.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
                    flanker_down.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
                elif stim_location=='lowerRight':
                    flanker_up.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
                    flanker_down.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
                    #flankers not being tested 
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
            elif aflanker=='tangential': # [Tangential flankers tested;Radial flankers not tested]
                if stim_location=='lowerLeft':
                    # flankers being tested are set first
                    flanker_up.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
                    flanker_down.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
                    #flankers not being tested 
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
                elif stim_location=='lowerRight':
                    # Flankers being tested are set first
                    flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1]-flanker_spacing_pix_testing_meridian)
                    flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1]+flanker_spacing_pix_testing_meridian)
                    # Flankers not being tested
                    flanker_up.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1]+flanker_spacing_pix_opp_meridian)
                    flanker_down.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1]-flanker_spacing_pix_opp_meridian)
            elif aflanker=='oblique1_horizontal': # This would test horizontal meridian
                #flankers being tested
                flanker_left.pos=(target_location[0]-flanker_spacing_pix_testing_meridian,target_location[1])
                flanker_right.pos=(target_location[0]+flanker_spacing_pix_testing_meridian,target_location[1])
                #flankers not being tested 
                flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_opp_meridian)
                flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_opp_meridian)
            elif aflanker=='oblique2_vertical': # This would test along vertical meridian
                # Flankers being tested
                flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_testing_meridian)
                flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_testing_meridian)
                # Flankers not being tested
                flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1])
                flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1])
            elif aflanker=='unflanked':
                # Flankers being tested
                flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix_opp_meridian)
                flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix_opp_meridian)
                # Flankers not being tested
                flanker_left.pos=(target_location[0]-flanker_spacing_pix_opp_meridian,target_location[1])
                flanker_right.pos=(target_location[0]+flanker_spacing_pix_opp_meridian,target_location[1])
            # since mask is normalized we move it based on the screen dimensions
            mask_object.pos=target_location #we center the mask with the stimulus

            #Have something that waits for a key press from the subject. This would send the flanker spacing condition to the TSLO system.
            # We change the color of the central circle of the fixation cross
            # We only have this (cue) for the microsaccade condition other wise we set the color to be the same
            if no_microsacc_cond==0:  
#                fix_c.fillColor=[0,1,0]
#                fix_c.lineColor=[0,1,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='lowerLeft':
                        fix_line_LL.color=[0,1,0]
                        if horizontal_sacc==True:
                            fix_line_UL.color=[0,1,0]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[0,1,0]
            elif no_microsacc_cond==1:
#                fix_c.fillColor=[1,0,0]
#                fix_c.lineColor=[1,0,0]   
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    # Have the cue change color only when foveal mode is off
                    if foveal_mode==False:
                        if cueLocation=='lowerLeft':
                            fix_line_LL.color=[1,0,0]
                        elif cueLocation=='lowerRight':
                            fix_line_LR.color=[1,0,0]  
            elif no_microsacc_cond==2:
                #We make the entire fixation cross red, so it alerts the subjects
#                fix_c.fillColor=[1,1,1]
#                fix_c.lineColor=[1,1,1]   
                fix_line_UL.color=[1,0,0]
                fix_line_UR.color=[1,0,0]
                fix_line_LL.color=[1,0,0]
                fix_line_LR.color=[1,0,0]
                
            cue_c.fillColor=[1,1,1]
            cue_c.lineColor=[1,1,1]
            if lsl_connection==True:
                keepGoing=True
                while keepGoing:
                    trialStart1=visual.TextStim(window,text=('Sending flanker spacing condition'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                    trialStart2=visual.TextStim(window,text=('Press any key to start trial'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=msg_size)

                    trialStart1.draw()
                    trialStart2.draw()
                        
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                if horizontal_sacc==True:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                        stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                        stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                    if mask_condition==True:
                        # we draw the mask
                        mask_object.draw()
                    window.flip()
                    keys=event.getKeys()

                    if len(keys)>0 and 'space' not in keys:
                        keepGoing=False
                    elif len(keys)>0:
                        if keys[0]=='space':
                            if cam_connection==True:
                                webcam_state=True                
                                #start data acquisition
                                cam.start_acquisition()
                                while webcam_state==True and cam_connection==True:
                                    #get data and pass them from camera to img
                                    cam.get_image(img)
                                    #create numpy array with data from camera. Dimensions of the array are 
                                    #determined by imgdataformat
                                    camData = img.get_image_data_numpy()
                                    #show acquired image 
                                    newdata=np.subtract(np.divide(camData,128.0),1.0)
                                    cam_image.setImage(newdata)
                                    cam_image.draw()
                                    if fixation_type=='+':
                                        for stim1 in [fix_v, fix_h, fix_c]:
                                            stim1.draw()
                                    elif fixation_type=='x':
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                            stim1.draw()
                                    window.flip()
                                    theKey = event.getKeys()
                                    if len(theKey)!=0:
                                        webcam_state=False
                                        #stop data acquisition
                                        cam.stop_acquisition()
                            elif cam_connection==False and imageParamsLSL==True:
                                while True:
                                    imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                                    #break when we get a keypress
                                    keys=event.getKeys()
                                    
                                    if len(keys) != 0:
                                        cv2.destroyAllWindows()
                                        break
                # We send the start message once the subject is ready/hits key
                if eye_tracker==True:
                    # We send the trial details to the tracker/saccade detection method
                    if multiple_machines==True:
                        #while True:
                        if machine_connection==True:
                            stimMachineSender(trial_info,info,outlet,commSetup=False)
                    else:
                        connSender.send(trial_info)
                    # Also send a message to indicate the start of the sequence/stream. This 
                    # would be used to store data and write to log file
                    if multiple_machines==True:
                        if machine_connection==True:
                            message="trialStart"
                            stimMachineSender(message,info,outlet,commSetup=False)
                    else:
                        connSender.send("trialStart")
                # we get the starting time stamp in ms and this can be used to get relative
                # time stamp for stimulus onset
                startTime=customTime.millis()
                #TSLO sender keeps sending packets until it is received by the TSLO machine. The second message here
                # is a dummy message
                if aflanker=='radial':
                    flanker_val_msg=723425
                elif aflanker=='tangential':
                    flanker_val_msg=8264368425
                elif aflanker=='unflanked':
                    flanker_val_msg=863526533
                if other_spacing_nosacc==True:
                    TSLOsenderlong(flanker_val_msg,repeatCounterStart,no_microsacc_cond,SOA,target_congruency_cond,flanker_spacing_val)
                else:
                    TSLOsenderlong(flanker_val_msg,repeatCounterStart,no_microsacc_cond,SOA,target_congruency_cond,0)

            else:
                if machine_connection==False:
                    keepGoing=False
                    break
                else:
                    keepGoing=True
                while keepGoing:
                    wait_msg.draw()
                  
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                if foveal_mode==True:
                                    for stim1 in [target_c_fov]:#[fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                        stim1.draw()
                                else:
                                    if horizontal_sacc==True:
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                            stim1.draw()
                                    else:
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                            stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            if foveal_mode==True:
                                for stim1 in [target_c_fov]:#[fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                    stim1.draw()
                            else:
                                if horizontal_sacc==True:
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,saccade_c_h]:
                                            stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                        stim1.draw()

                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                        
                    keys=event.getKeys()
                    if len(keys)>0 and 'space' not in keys:
                        keepGoing=False
                    elif len(keys)>0:
                        if keys[0]=='space':
                            if cam_connection==True:
                                webcam_state=True                
                                while webcam_state==True and cam_connection==True:
                                    #start data acquisition
                                    cam.start_acquisition()
                                    #get data and pass them from camera to img
                                    cam.get_image(img)
                                    #create numpy array with data from camera. Dimensions of the array are 
                                    #determined by imgdataformat
                                    camData = img.get_image_data_numpy()
                                    #show acquired image 
                                    newdata=np.subtract(np.divide(camData,128.0),1.0)
                                    cam_image.setImage(newdata)
                                    cam_image.draw()
                                    if fixation_type=='+':
                                        for stim1 in [fix_v, fix_h, fix_c]:
                                            stim1.draw()
                                    elif fixation_type=='x':
                                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                            stim1.draw()
                                    if mask_condition==True:
                                        # we draw a mask that would cover both the stimulus and flankers
                                        mask_object.draw()
                                    #also draw peripheral saccade cue
                                    window.flip()
                                    theKey = event.getKeys()
                                    if len(theKey)!=0:
                                        webcam_state=False
                                        #stop data acquisition
                                        cam.stop_acquisition()
                            elif cam_connection==False and imageParamsLSL==True:
                                while True:
                                    imgMean,imgSD=recvAndPlotImageParams(window,plot_dist=False)
                                    #break when we get a keypress
                                    keys=event.getKeys()
                                    
                                    if len(keys) != 0:
                                        cv2.destroyAllWindows()
                                        break
            if keys[0]=='escape':
                # We send a message here to indicate the program has been closed
                if multiple_machines==True:
                    if 'escape' in key_pressed:
                        while True:
                            try:
                                # Keeps sending till eye tracker picks it
                                message='q'
                                s.sendto(message.encode('utf-8'),server)
                                # recv feedback msg
                                (data,addr)=s.recvfrom(1024)
                                data=data.decode('utf-8')
                                if data=='q':
                                    # set a variable to false to indicate the LSL connection is closed
                                    machine_connection=False
                                    break
                            except BlockingIOError:
                                pass  
                                
                keep_repeating=False
                break
                
            ## 3. Main trial sequence
            if target_type=='bars':
                target_type_list=['h','v']*3
            elif target_type=='T':
                if flanker_type=='+':
                    target_type_list=['+']*5
                elif flanker_type=='T':
                    target_type_list=['u','d','l','r']
                    # Since we need five different stimuli we randomly add a fifth one
                    target_type_list.append(random.choice(target_type_list))
            elif target_type=='numbers':
                target_type_list=['0','1','2','3','4','5','6','7','8','9']
            elif target_type=='E':
                target_type_list=['E','3','e','f']
                # Since we need five different stimuli we randomly add a fifth one
                target_type_list.append(random.choice(target_type_list))
            # We shuffle the list of stimuli before assigning them
            random.shuffle(target_type_list)
            # We first assign target stimuli
            if target_type=='T' and flanker_type=='+': # we set the target to be a T with random orientation
                target_lists=['u','d','l','r']
                target_let=np.random.choice(target_lists)
            elif target_type=='T' and flanker_type=='T':
                target_let=target_type_list[0]
            # We finally assign flanker stimuli
            flanker_let_1=target_type_list[1]
            flanker_let_2=target_type_list[2]
            flanker_let_3=target_type_list[3]
            flanker_let_4=target_type_list[4]

            # we can also switch the contrast between the target and flankers
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]

            # we then set the respective target and flanker characters 
            target.text=target_let
            flanker_left.text=flanker_let_1
            flanker_right.text=flanker_let_2
            flanker_up.text=flanker_let_3
            flanker_down.text=flanker_let_4
            # The stimulus duration is used as a fraction of the frame rate of the display;
            # THis ensures better timing of the target presentations
            target.render()
            flanker_right.render()
            flanker_left.render()
            flanker_up.render()
            flanker_down.render()

            # The following set of code is used to present the stimulus & cue at pre-determined
            # locations and duration
            # The experimental sequence is as follows:
            # 1. Initial delay of 750 to 1250 ms
            # 2. Saccade cue: Circle changes color indicating subjects to make the saccade
            # 3. Variable delay to maximize the number of trials presented across different time
            #    points before stimulus onset
            # 4. Brief Stimulus presentation
            # 5. Post stimulus delay for 200-300 ms to capture saccade events
            
            # we add a delay on the very first trial so each video is padded to prevent 
            # any stimuli from being missed from frame grab/recording
            # We would 
            initialDelay=np.random.choice(np.arange(0.750,1.250,0.100))
            if lsl_connection==True:
                for nflips in range(int(refreshRate*initialDelay)):
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                if horizontal_sacc==True:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                        stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                        stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            if foveal_mode==True:
                                for stim1 in [target_c_fov]:
                                    stim1.draw()
                            else:
                                if horizontal_sacc==True:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,saccade_c_h]:
                                        stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                        stim1.draw()
                    wait_msg2.draw()

                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                    # throw away any saccade messages
                    if eye_tracker==True:
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()
            else:
                for nflips in range(int(refreshRate*initialDelay)):
                    if peripheral_cue==True:
                        if oblique_cond==True:
                            for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                                stim1.draw()
                        else:
                            if fixation_type=='+':
                                for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                                    stim1.draw()
                            elif fixation_type=='x':
                                if horizontal_sacc==True:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                        stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                        stim1.draw()
                    else:
                        if fixation_type=='+':
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw()
                        elif fixation_type=='x':
                            if foveal_mode==True:
                                for stim1 in [target_c_fov]:
                                    stim1.draw()
                            else:
                                if horizontal_sacc==True:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,saccade_c_h]:
                                        stim1.draw()
                                else:
                                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                        stim1.draw()

                    wait_msg2.draw()
                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    # We can also save the frame as an image when needed
                    if save_image==True and nflips<1:
                        window.getMovieFrame(buffer='back')
                    #also draw peripheral saccade cue
                    window.flip()
            #Use this to save the cue onset image
            if save_image==True:
                window.saveMovieFrames('Cue_Onset'+'.jpg')
                                    
            # The cue would be an exogenous peripheral cue that would on for the whole trial 
            # The observer would only move their eye towards the cue when fixation cross 
            # goes away/ central circle changes color
            #clock.reset()
            # Ideally we would want this duration to be less than Saccade Reaction Time (SRT)
            # SRT- Time from cue onset to saccade onset. Hence cue onset must be taken into account
            if eye_tracker==True:
                cueDuration=saccade_latency_median-(SOA)
            else:
                cueDuration=0.200

            # we revert the color of the central circle from the fixation cross otherwise
            # for no microsaccade condition we keep it the same
            if no_microsacc_cond==0:  
#                fix_c.fillColor=[0,0,0]
#                fix_c.lineColor=[0,0,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if cueLocation=='lowerLeft':
                        fix_line_LL.color=[-1,-1,-1]
                        if horizontal_sacc==True:
                            fix_line_UL.color=[-1,-1,-1]
                    elif cueLocation=='lowerRight':
                        fix_line_LR.color=[-1,-1,-1]
            elif no_microsacc_cond==1:
#                fix_c.fillColor=[1,0,0]
#                fix_c.lineColor=[1,0,0]
                # Also change the color of a portion of the  fixation cross
                if fixation_type=="x":
                    if foveal_mode==False:
                        if cueLocation=='lowerLeft':
                            fix_line_LL.color=[1,0,0]
                        elif cueLocation=='lowerRight':
                            fix_line_LR.color=[1,0,0]
            elif no_microsacc_cond==2:
                #just make fixation circle white
#                fix_c.fillColor=[1,1,1]
#                fix_c.lineColor=[1,1,1] 
                pass

            # We have a place holder for saccade time stamp that would be changed when 
            # we get a saccade event
            sacc_cond=0
            sacc_timeStamp=0
            sacc_before_target=0
            sacc_during_target=0
            sacc_before_target_timeStamp=0
            sacc_during_target_timeStamp=0
            for nflips in range(int(refreshRate*cueDuration)):
                if oblique_cond==True:
                    for stim1 in [fix_v, fix_h, fix_c,cue_c,target_c]:
                        stim1.draw()
                else:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c,cue_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        if foveal_mode==True:
                            for stim1 in [target_c_fov]:#[fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                        else:
                            if horizontal_sacc==True:
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                    stim1.draw()
                            else:
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                    stim1.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                #Draw the small square for Photodiode to detect and to create a marker in the video
                if trig_opt=='photocell':
                    if nflips<5:
                        small_square.draw()
                # We can also save an image of the cue if needed
                if save_image==True and nflips<1:
                    window.getMovieFrame(buffer='back')
                window.flip()
                # we record time stamp for when the cue came on
                if nflips==0:
                    cueTime=customTime.millis()
                # We can also send a message via Pport
                if trig_opt=='Pport':
                    startFrame=2
                    endFrame=startFrame+1
                    if nflips==startFrame:
                        p.write_data_register(0)
                    elif nflips==endFrame:
                        p.write_data_register(0xff)
                # Ideally we wont get saccades before target onset. But we get cases 
                # where it does occur to get better estimate of latency
                if eye_tracker==True:
                    if saccade_trigger_comm=='socket':
                        status=connListener.poll()
                        if status==True and sacc_before_target_timeStamp==0 and sacc_before_target==0: 
                            try:
                                got_one=connListener.recv()
                                sacc_before_target = sacc_before_target | got_one
                                sacc_before_target_timeStamp=customTime.millis()
                                sacc_latency=sacc_before_target_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
                            except EOFError:
                                print('receive function error')
                    elif saccade_trigger_comm=='Pport':
                        raw_msg=p.read_status_register()
                        # TODO: need to figure out which is 1
                        if raw_msg & 64==0:
                            got_one=0
                        else:
                            got_one=1
                        # we would want to make sure that we only get ones in response to cue
                        # so give atleast 200 ms
                        if sacc_before_target==0 and got_one==1 and nflips>25:
                            sacc_before_target = 1
                            sacc_before_target_timeStamp=customTime.millis()
                            sacc_latency=sacc_before_target_timeStamp - cueTime
                            sacc_latency_list.append(sacc_latency)

            #Use this to save the cue offset image
            if save_image==True:
                window.saveMovieFrames('Cue_Offset'+'.jpg')
            # throw away any saccade messages
            if eye_tracker==True:
                if saccade_trigger_comm=='socket':
                    status=connListener.poll()
                    if status==True:
                        clear_msg=connListener.recv()
                elif saccade_trigger_comm=='Pport':
                    clear_msg=p.read_status_register()
            # we clear key events that could occured after the end of the previous trial 
            event.clearEvents()
            # we would only draw the next stimulus only when a saccade was not detected 
            # during the delay period. If saccade did occur we skip to the response window
            for frameN in range(target_presentation):
                #Only draw fixation cross when presenting peripheral stimuli
                # For foveal condition we replace the fixation target with stimulus
                if foveal_mode==False:
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        if horizontal_sacc==True:
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,saccade_c_h]:
                                stim1.draw()
                        else:
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                # We continue to have the saccade circle in cases where the target & cue location
                # are incongruent
                if target_congruency_cond==0:
                    if stim_location=='lowerLeft':
                        for stim2 in [target_c_lr]:
                            stim2.draw()
                    elif stim_location=='lowerRight':
                        for stim2 in [target_c_ll]:
                            stim2.draw()
                if trig_opt=='photocell':
                    #Draw the small square for Photodiode to detect and to create a marker in the video
                    small_square.draw()
                target.draw()                
                # we test whether the stimulus would appear on the left/right and based
                # on that we set the inner and outer flankers
                if stim_location=='left' or stim_location=='lowerLeft':                    
                    inner_flanker=flanker_right
                    outer_flanker=flanker_left
                elif stim_location=='right' or stim_location=='lowerRight':
                    inner_flanker=flanker_left
                    outer_flanker=flanker_right
                
                inner_flanker.draw()
                outer_flanker.draw()
                flanker_up.draw()
                flanker_down.draw()
      
                # We capture fixation target and save to image file if needed
                if save_image==True and frameN<1:
                    window.getMovieFrame(buffer='back')
                flip_time=window.flip()

                if frameN==0:
                    # we would use this to debug missed markers in the retinal video
                    stimTime=timerDuration-timer.getTime() #gives relative time stamps
                    currTime=customTime.millis()
                
                if trig_opt=='Pport':
                    startFrame=0
                    endFrame=startFrame+1
                    if frameN==startFrame:
                        p.write_data_register(0)
                    elif frameN==endFrame:
                        p.write_data_register(0xff)

                # we have socket message outside the target presentation to improve performance
                if eye_tracker==True:
                    # we send the stimulus time to tracker/saccade method via
                    pythonTime=str(currTime-startTime) # gives timestamps based on python clock
                    if saccade_trigger_comm=='socket':
                        # Ideally we wont get saccades before target onset. But we get cases 
                        # where it does occur to get better estimate of latency
                        status=connListener.poll()
                        if status==True and sacc_before_target_timeStamp==0 and sacc_before_target==0: 
                            try:
                                got_one=connListener.recv()
                                sacc_during_target = sacc_during_target | got_one;
                                sacc_during_target_timeStamp=customTime.millis()
                                sacc_latency=sacc_during_target_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
                            except EOFError:
                                print('receive function error')
                    elif saccade_trigger_comm=='Pport':
                            raw_msg=p.read_status_register()
                            if raw_msg & 64==0:
                                got_one=0
                            else:
                                got_one=1
                            if sacc_during_target==0 and got_one==1:
                                sacc_during_target = 1
                                sacc_during_target_timeStamp=customTime.millis()
                                sacc_latency=sacc_during_target_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
            #Use this to save the stimulus image
            if save_image==True:
                window.saveMovieFrames('Stimulus'+'.jpg')

            # we have a small window following stimulus offset to capture the saccade 
            # event and make a note of the latency. If we do get a saccade
            # during the cue window or during target presentation we would omit those trials
            if eye_tracker==True :
                nflips=0
                # we bring back the cue_c so subjects can hold fixation
                if no_microsacc_cond==0:  
                    postStimDelay=0.500
                    cue_c.fillColor=[0,1,0]
                    cue_c.lineColor=[0,1,0]
                else:
                    postStimDelay=0.200
                for nflips in range(int(refreshRate*postStimDelay)):
                    if fixation_type=='+':
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                    elif fixation_type=='x':
                        if foveal_mode==True:
                            for stim1 in [target_c_fov]:#[fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                                stim1.draw()
                        else:
                            if horizontal_sacc==True:
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                    stim1.draw()
                            else:
                                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                    stim1.draw()
                    if oblique_cond==True:
                        target_c.draw()
                    wait_msg2.draw()
                    if mask_condition==True:
                        # we draw a mask that would cover both the stimulus and flankers
                        mask_object.draw()
                    #also draw peripheral saccade cue
                    window.flip()
                    # when using the eye tracker we look for a saccade event following stimulus onset
                    # and halt the program when one is detected. 
                    if saccade_trigger_comm=='socket':
                        status=connListener.poll()
                        # we would like to the get the saccade time stamp for the very first 
                        # saccade that occurs after stimulus offset
                        # we also skip events from the first few flip to account for latenct of the eye tracking
                        # protocol
                        if status==True and sacc_timeStamp==0 and sacc_cond==0: 
                            try:
                                got_one=connListener.recv()
                                sacc_cond = sacc_cond | got_one
                                sacc_timeStamp=customTime.millis()
                                sacc_latency=sacc_timeStamp - cueTime
                                sacc_latency_list.append(sacc_latency)
                            except EOFError:
                                print('receive function error')
                    elif saccade_trigger_comm=='Pport':
                        raw_msg=p.read_status_register()
                        if raw_msg & 64==0:
                            got_one=0
                        else:
                            got_one=1
                        if sacc_cond==0 and got_one==1 and sacc_during_target==0:# and sacc_before_target==0:
                            sacc_cond = 1
                            sacc_timeStamp=customTime.millis()
                            sacc_latency=sacc_timeStamp - cueTime
                            sacc_latency_list.append(sacc_latency)
                #VALID
                # only do this when we get a valid saccade event
                if sacc_cond==1 and no_microsacc_cond==0:
                    # This would be a valid trial and hence we would get behav response
                    del_trial=False
                            
                #INVALID
                # When we miss a saccade on a microsaccade trial we would repeat it at the 
                # end of the sequence
                elif sacc_cond==0 and no_microsacc_cond==0:
                    # we would not want to get the response on this trial since its invalid and 
                    # repeat the condition
                    del_trial=True

                #VALID
                # This would be a valid trial for no microsaccade condition hence we get the 
                # behavioral response
                elif sacc_cond==0 and no_microsacc_cond==1:
                    del_trial=False
                    
                #INVALID
                # But having a microsaccade when one is not expected would make it again an
                # invalid trial which would be repeated at the end
                elif sacc_cond==1 and no_microsacc_cond==1:
                    del_trial=True
                
                #VALID
                # No eye movement on neutral trial would make it valid
                elif sacc_cond==0 and no_microsacc_cond==2:
                    del_trial=False
                #INVALID
                # Having microsaccade on neutral trials
                elif sacc_cond==1 and no_microsacc_cond==2:
                    del_trial=True
                
            else:
                del_trial=False

            # We reset eye tracking protocol & drop trial marker to output file
            if eye_tracker==True:
                if multiple_machines==True:
                    if machine_connection==True:
                        while True:
                            try:
                                # Keeps sending till eye tracker picks it
                                if flanker_counter==len(flanker_spacing_list) and conditionIdx!=0 and del_trial==False and horizontal_sacc==False and other_spacing_nosacc==False:
                                    message='trialStop&End'
                                elif flanker_counter==len(flanker_spacing_list) and conditionIdx==0 and del_trial==False and horizontal_sacc==True and other_spacing_nosacc==False:
                                    message='trialStop&End'
                                elif flanker_counter==len(flanker_spacing_list) and conditionIdx==0 and del_trial==False and horizontal_sacc==False and other_spacing_nosacc==True:
                                    message='trialStop&End'
                                else:
                                    message='trialStop'
                                s.sendto(message.encode('utf-8'),server)
                                # recv feedback msg
                                (data,addr)=s.recvfrom(1024)
                                data=data.decode('utf-8')
                                if data=='q':
                                    break
                            except BlockingIOError:
                                        pass 
                else:
                    connSender.send("trialStop")
                
            keepGoing=True
            while keepGoing==True:
                response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                response_wait.draw()
                if del_trial==True:
                    if sacc_before_target==1 and sacc_cond==0: # eyes moved too quickly 
                        msg="Moved too early!"
                    elif sacc_before_target==0 and sacc_cond==0:# eyes never moved
                        msg='Moved too late!'
                    elif sacc_before_target==0 and sacc_cond==1:# perfect timing
                        msg='Perfect Timing!'
                    else: # Should never get here (theoretically!)
                        msg='Please keep still!'
                    failed_msg=visual.TextStim(window,text=(msg),pos=(0,-msg_offset),color=[-1,-1,-1],height=msg_size)
                    #failed_msg.draw()
                if fixation_type=='+':
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                elif fixation_type=='x':
                    if foveal_mode==True:
                        for stim1 in [target_c_fov]:#[fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                            stim1.draw() 
                    else:
                        if horizontal_sacc==True:
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr,saccade_c_h]:
                                stim1.draw() 
                        else:
                            for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                                stim1.draw() 
                if oblique_cond==True:
                    target_c.draw()
                if mask_condition==True:
                    # we draw a mask that would cover both the stimulus and flankers
                    mask_object.draw()
                flip_time=window.flip()

                key=event.getKeys()
                if len(key)>0:
                    keepGoing=False
                    continue
            
            # we check if there was a keypress made duration the response window otherwise we set a defualt value
            if 'key' not in locals():
                key_pressed='Nil' # arbitary value 
            elif len(key)==0:
                key_pressed='Nil'
            else:
                key_pressed=key[0]
                    
            #An escape key press here terminates the loop
            if key_pressed=='escape':
                keep_repeated=False
                while True:
                    try:
                        # Keeps sending till eye tracker picks it
                        message='q'
                        s.sendto(message.encode('utf-8'),server)
                        # recv feedback msg
                        (data,addr)=s.recvfrom(1024)
                        data=data.decode('utf-8')
                        if data=='q':
                            # set a variable to false to indicate the LSL connection is closed
                            machine_connection=False
                            break
                    except BlockingIOError as e:
                        pass  
                break
            # we assign a response value based on the keypress that was made by the subject
            if target_type=='numbers':
                if key_pressed=='num_0':
                    response_key='0'
                elif key_pressed=='num_1':
                    response_key='1'
                elif key_pressed=='num_2':
                    response_key='2'
                elif key_pressed=='num_3':
                    response_key='3'
                elif key_pressed=='num_4':
                    response_key='4'
                elif key_pressed=='num_5':
                    response_key='5'
                elif key_pressed=='num_6':
                    response_key='6'
                elif key_pressed=='num_7':
                    response_key='7'
                elif key_pressed=='num_8':
                    response_key='8'
                elif key_pressed=='num_9':
                    response_key='9'
                else:
                    response_key='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
            elif target_type=='bars':
                if key_pressed=='left' or key_pressed=='right' or key_pressed=='num_4' or key_pressed=='num_6':
                    response_key='h'
                elif key_pressed=='up' or key_pressed=='down' or key_pressed=='num_8' or key_pressed=='num_2':
                    response_key='v'
                else:
                    response_key='100'
            elif target_type=='T':
                if key_pressed=='up':
                    response_key='u'
                elif key_pressed=='down':
                    response_key='d'
                elif key_pressed=='left':
                    response_key='l'
                elif key_pressed=='right':
                    response_key='r'
                else:
                    response_key='100'
            elif target_type=='E':
                if key_pressed=='right':
                    response_key='E'
                elif key_pressed=='left':
                    response_key='3'
                elif key_pressed=='up':
                    response_key='e' # need to verify this
                elif key_pressed=='down':
                    response_key='f'
                else:
                    response_key='100'
            # we woudl provide feedback based on whether or not the response was correct
            if response_key==target.text:
                beep_sound_correct.play()
                corr=1
            else:
                beep_sound_incorrect.play()
                corr=0
            
            # we would write the response to the output file
            # we convert target presentation to ms from flips before writing to output file
            target_presentation_ms=target_presentation*(1000/refreshRate)
            cueDuration_ms=cueDuration#*(1000/refreshRate)
            if key_pressed!='escape' and behav_output==True:
                if other_spacing_nosacc==True:
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%s,%.2f,%s,%s,%2f,%2f,%d,%2f,%s\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,flanker_spacing,stim_location,cueLocation,cueDuration_ms,target_presentation_ms,corr,stimTime,trialValidity) )
                else:
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%s,%s,%s,%2f,%2f,%d,%2f,%s\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,cueLocation,cueDuration_ms,target_presentation_ms,corr,stimTime,trialValidity) )
            elif key_pressed!='escape':
                while True:
                    try:
                        # Keeps sending till eye tracker picks it
                        message='q'
                        s.sendto(message.encode('utf-8'),server)
                        # recv feedback msg
                        (data,addr)=s.recvfrom(1024)
                        data=data.decode('utf-8')
                        if data=='q':
                            # set a variable to false to indicate the LSL connection is closed
                            machine_connection=False
                            break
                    except BlockingIOError:
                        pass 
            # This would be a valid trial
            if del_trial==False:
                # This marks the end of the trial so we send this info across to TSLO machine
                trialComplete=1
                if flanker_counter==len(flanker_loop_list) and conditionIdx!=0 and horizontal_sacc==False and other_spacing_nosacc==False:
                    sequenceComplete=1 # last trial from block
                elif flanker_counter==len(flanker_loop_list) and conditionIdx==0 and horizontal_sacc==True and other_spacing_nosacc==False:
                    sequenceComplete=1 # last trial from block
                elif flanker_counter==len(flanker_loop_list) and conditionIdx==0 and horizontal_sacc==False and other_spacing_nosacc==True:
                    sequenceComplete=1 # last trial from block
                else:
                    sequenceComplete=0 # more trials left in the block
                if lsl_connection==True:
                    TSLOsender(trialComplete,sequenceComplete)

            elif del_trial==True:
                # We would send a different message to matlab to delete the video 
                # associated with invalid trial
                trialComplete=2 # invalid trial message
                if lsl_connection==True:
                    # Since the trial is not valid we would repeat even if it
                    # is the last flanker condition of the sequence
                    sequenceComplete=0 
                    # we would send a message to the TSLO machine
                    TSLOsender(trialComplete,sequenceComplete)
                # append the faulty trial to the end of the list
                if other_spacing_nosacc==True:
                    flanker_spacing_list.append(flanker_spacing)
                    flanker_meridian_list.append(aflanker)
                else:
                    flanker_spacing_list.append(aflanker)
                    SOA_list.append(SOA)
                microsacc_status_list.append(no_microsacc_cond)
                congruency_cond_list.append(target_congruency_cond)
                
            # we increment the trial counter / repeat counter only after each trial
            repeatCounterStart+=1
             # we increment the flanker counter after each trial
            flanker_counter+=1   
            # Increment target congruency & soa counter only 
            if neutral_trials==True:
                if no_microsacc_cond==2: # this would be for neutral trials
                    pass
                else: # otherwise we increment this counter
                    trial_validity_counter+=1
            else: 
                trial_validity_counter+=1
            #close the output file here
            if behav_output==True:
                outfile.close()   

    # We would print out a message for the subject and wait for them to hit a key before restarting the program
    loop_msg1=visual.TextStim(window,text='Block Complete!',pos=(0,0),height=msg_size)
    loop_msg2=visual.TextStim(window,text='Press any key to continue!',pos=(0,-150),height=msg_size)
    loop_msg1.draw()
    loop_msg2.draw()
    window.flip()
    # We print real time saccade latency measures
    if eye_tracker==True:
        print(sacc_latency_list)
        print(np.median(sacc_latency_list),np.std(sacc_latency_list),flush=True)
    event.waitKeys()
    break

#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

if eye_tracker==True:
    # we close the socket program
    if multiple_machines==False:
        connListener.close()
        listener.close()
        connSender.close()
        print('connection closed')

# we close the different programs/processes that were opened
window.close()
core.quit()