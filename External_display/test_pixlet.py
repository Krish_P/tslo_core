from psychopy import visual, core, event
import pixlet
import random
import customTime

win = visual.Window(size=[900,900],monitor="Predator 4k",units='pix',allowGUI=True,fullscr=False, color=[0,0,0], screen=0)
height=45 # Height is rounded to a multiple of 5 (stroke*5)
# Here we specify the test type
# 1. Trigram # trigram
# 2. Pixlet pos test #pos_test
stim_pos='trigram'

if stim_pos=='trigram':
    letCenter=pixlet.PixLet(win,height=height,text='5',color=[-1,-1,-1],pos=(0,0)) # default pos is (0,0)
    letCenter.render()

    letLeft=pixlet.PixLet(win=win,height=height)
    letLeft.text='6' 
    letLeft.pos=(-height*1.2,0)
    letLeft.render()

    letRight=pixlet.PixLet(win=win,height=height)
    letRight.text='3' 
    letRight.pos=(height*1.2,0)
    letRight.render()
elif stim_pos=='pos_test':
    numStim=10
    xpos=-100
    step_size1=height+5
    step_size2=height+4.5
    T_list=['u','d','l','r']
    for stimIdx in range(numStim):
        letStim=pixlet.PixLet(win,height=height,text='u',pos=(xpos,0))
        letStim.render()
        letStim.draw()
        xpos+=step_size1
    xpos=-100
    for stimIdx in range(numStim):
        if stimIdx==0:
            msg_str=str(step_size2)
            cond_msg=visual.TextStim(win,height,text=msg_str,pos=(-200,height*1.5))
            cond_msg.draw()
        letStim=pixlet.PixLet(win,height=height,text='u',pos=(xpos,height*1.5))
        letStim.render()
        letStim.draw()
        xpos+=step_size2
        
event.clearEvents()
counter=0
counter_msg=visual.TextStim(win,text=str(counter),pos=(0,150),color=[1,1,1])
while True:
    counter_msg.text=str(counter)
    for i in range(100):
        counter_msg.draw()
        win.flip()
    startTime=customTime.millis()
    for nFlips in range(2):
        if stim_pos=='trigram':
            letCenter.draw()
            letLeft.draw()
            letRight.draw()
        win.flip()
    loopTime=customTime.millis()- startTime
    print(loopTime)
    counter+=1
    for i in range(100):
        win.flip()
    # keep running until key press
    keys=event.getKeys()
    if len(keys)>0 and keys[0]=='space':
        break
win.close()
