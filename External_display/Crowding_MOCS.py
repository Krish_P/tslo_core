from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import random
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from ximea import xiapi
import pixlet

from ReceiveDataFunction import TSLOreceiver #This would be used to communicate with the TSLO machine
from PixelConversion import PixelConversion
from make_mask import make_mask  
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
lsl_connection=False 
cam_connection=False
keep_repeating=True
#===================================================================================================================================

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [1920, 1080]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)
window.setMouseVisible(False)
# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=1920
vert_res=1080
diagonal_size=62.5 #diagonal screen size
dist_to_screen= 180 #cm (for the mirror setup with 165cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=6, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

# Additionally,we set up the camera feature,so that subjects can visualize the retinal image quality and the PMT gain setting.
# For this to work the webcam would have to be setup such that it faces the retinal image window on ICANDI and the PMT box with the 
# gain setting
if cam_connection==True:
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[500,500],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()
#===================================================================================================================================
## Experimental Parameters

#ONLY PART TO BE CHANGED AT THE START OF EXPERIMENT:
#The following set of variables control the main set of parameters for the experiment 
# The following are the list of parameters that can be altered and brief description of what it does to
# the protocol:
    # eccentricity_deg: eccentricity of the target in degrees visual angle
    # location_uncertainity: This determines whether the stimulus would change in position across the vertical midline b/w trials
    # spacing_condition: will be used to determine if we need to run all spacings or just the one specified locally (0: Locally specified spacing; 1: All)
    # inner_flanker: We would want to remove the inner flanker to prevent overlap with the fixation target at close eccentricities
    # cue_condition: This defines whether a cue would be used in the experimental sequence or not
    # reverse_flanker_contrast: changes the polarity of the flankers
    # foveal_mode: the targets would be presented at the point of fixation
    # save_image: To save stimulus frame as image
while keep_repeating==True:
    #Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            startup_msg1=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,150),color=[-1,-1,-1],height=pixel_per_degree/4)
            startup_msg2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-150),color=[-1,-1,-1],height=pixel_per_degree/3)
            startup_msg1.draw()
            startup_msg2.draw()
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            window.flip()

            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    webcam_state=True                
                    #start data acquisition
                    cam.start_acquisition()
                    while webcam_state==True & cam_connection==True:
                        #get data and pass them from camera to img
                        cam.get_image(img)
                        #create numpy array with data from camera. Dimensions of the array are 
                        #determined by imgdataformat
                        camData = img.get_image_data_numpy()
                        #show acquired image 
                        newdata=np.subtract(np.divide(camData,128.0),1.0)
                        cam_image.setImage(newdata)
                        cam_image.draw()
                  
                        window.flip()
                        theKey = event.getKeys()
                        if len(theKey)!=0:
                            webcam_state=False
                            #stop data acquisition
                            cam.stop_acquisition()

        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        protocol_packet=TSLOreceiver()

        # These parameters are provided from TSLO machine
        eccentricity_deg=protocol_packet[0]
        spacing_condition=protocol_packet[1]
        cue_condition=protocol_packet[2] 
        subj_id=protocol_packet[3]

    else:
        # If LSL is false set these parameters locally to their defaults
        eccentricity_deg=1
        spacing_condition=1
        cue_condition=False
        subj_id=2

    # These parameters are set locally (Stimulus Machine)
    num_repeats=10 # Sets the number of repeats for each flanker spacing at a given ecc
    mask_condition=False
    location_uncertainity=True 
    inner_flanker=False
    reverse_flanker_contrast=False
    save_image=False 
    foveal_mode=False
    # We would draw stimuli based on target type, currently this can be one of the following:
    # 1. bars
    # 2. E
    # 3. Numbers
    target_type='numbers'

    # These parameters would be set at the start of the experiment and ideally would not want to change them 
    # after the start of the experiment
    #Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
    # on the fixation cross for variable duration before stimulus onset
    # Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
    target_presentation=0.500 #sec
    cue_presentation= 0.050 #sec
    fixation_duration=0.450 #sec 
    mask_duration=0.200 #sec
    response_window=2.00#sec

    # This creates a list of different flanker spacings to be used which would be multiplied
    # with the letter height to given spacings as a function of letter height : nominal spacings
    # We also randomize the order of the flanker spacing on each run of the script
    # Depending on the spacing condition that is set at the start of the experiment we would run all spacings otherwise
    # we would run just the ones specified manually below
    if spacing_condition==1:
        flanker_space=[1.6,2,3,5,100]
    elif spacing_condition==0:
        # Set the spacing you want to test; Useful in testing missed conditions/repeats
        flanker_space=[100]
    timing_list=np.arange(0.250,1.5,0.25)

    #We would then generate multiply the each element of the flanker spacing list based on the number of repeates required at each level
    flanker_spacing_list=np.repeat(flanker_space,num_repeats)
    random.shuffle(flanker_spacing_list)


    # We create a dictionary that contains the starting letter size for a particular flanker spacing at a certain eccentricity. We would 
    # generally want this size to be supra threshold, so as to ensure that the subjects are more likely to get correct responses on the 
    # first set of trials
    # Here we have a nested dictionary with the eccentricity as the first layer
    # NOTE: These values would be set based on the isolated letter recognition threshold from QUEST
    # The first key indicates the subject ID and the second inner one represents the eccentricity
    params={1:{1:0.14,2:0.19,3:0.33,4:0.43,5:0.53},
            2:{1:0.19,2:0.27,3:0.36,4:0.34,5:0.40},
            3:{1:0.22,2:0.36,3:0.43,4:0.46,5:0.66},
            4:{1:0.27,2:0.36,3:0.48,4:0.40,5:0.75},
            5:{1:0.17,2:0.36,3:0.39,4:0.56,5:0.80} }
    #===================================================================================================================================
    ## Experiment Stimuli: Here we create the different stimuli that would later be used in the experiment

    # The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
    if location_uncertainity==True:
        stim_location_list=['left','right']
        fix_xpos=0
        msg_offset=150
        step_size=20 #pixels
        fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line
    else:
        stim_location_list=['right'] #This would make all stimuli appear to the right or left of the fixation target
        fix_xpos=0
        msg_offset=150
        step_size=20 #pixels
        fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line


    #Create basic stimuli here that could be called multiple times during the course of the experiment
    small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
    wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Press any key to start the experiment')
    wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Please wait...')   

    # The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
    cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    # Cue for rightward target location
    cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    #cue for leftward target location
    cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)

    #we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
    # Here we would draw it based on the target type that was selected earlier
    if target_type=='E' or target_type=='bars':
        target = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_left = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_right = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_up = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
        flanker_down = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        target.font="Sloan"
        flanker_left.font="Sloan"
        flanker_right.font="Sloan"
        flanker_up.font="Sloan"
        flanker_down.font="Sloan"
    elif target_type=='numbers':
        target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
        flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        
    # This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
    # The letter size is kept constant and is set to 1.5x the threshold from the isolated letter threshold measures.
    # To ensure letter size is supra threshold
    letter_height=(params[subj_id][eccentricity_deg])*1.5
    letter_height_pix=letter_height*pixel_per_degree

    # We also create a set of post masks for different letter sizes that might be used in the experiment. 
    # Note: The main advantage of pre-rendered masks is to ensure smooth operation of the script 
    prog_bar = visual.Rect(win=window, pos=(0,0), size=(200,30), lineColor='black',fillColor='black')
    render_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Rendering Masks...')
    target_location_msg=visual.TextStim(window, pos=(0,-msg_offset),color=[-1,-1,-1],height=pixel_per_degree/4,text=("Stimulus eccentricity is %d degrees"%(eccentricity_deg)))
    target_location_msg.draw()
    render_msg.draw()
    window.flip()
    
    # Mask parameters
    mask_height=250
    mask_width=250
    mask_density=0.4
    num_mask=5 #4 masks for flankers and one for the target
    
    prog_bar.size=(200*(6)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
    
    # we then create the masks
    mask_target=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(5)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
    
    mask_inner_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(4)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
    
    mask_outer_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(3)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
    
    mask_up_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(2)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()
    
    mask_lower_flanker=make_mask(win=window,mask_size=(mask_width,mask_height),letter_height=letter_height_pix,density=mask_density,numbers=True) 
    prog_bar.size=(200*(1)/num_mask,50)
    target_location_msg.draw()
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    #===================================================================================================================================
    ## Experimental sequence:

    #We use the position of the fixation cross center to position the target and flankers
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 

    # The following set of lines set the flanker condition 
    eccentricity_pix=eccentricity_deg*pixel_per_degree
    eccentricity=fixation_pos[0]+eccentricity_pix
    if foveal_mode==True:
        target_location=(fix_h.start[0],0)
    else:
        target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
    target.pos=target_location
                                  
    #This is used to provide a audio feedback to the user 
    beep_feedback=core.CountdownTimer()
    beep_sound_correct=Sound(value=1000,secs=0.25)
    beep_sound_incorrect=Sound(value=250,secs=0.25)

    #Have something that waits for a key press from the subject. This essentially would initiate the sync between the stimulus and 
    #imaging machines and also start the experiment. 
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            stream_wait1=visual.TextStim(window,text=('Waiting for TSLO machine'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/4)
            stream_wait2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=pixel_per_degree/3)

            stream_wait1.draw()
            stream_wait2.draw()
                
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            window.flip()
                
            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    webcam_state=True                
                    #start data acquisition
                    cam.start_acquisition()
                    while webcam_state==True and cam_connection==True:
                        #get data and pass them from camera to img
                        cam.get_image(img)
                        #create numpy array with data from camera. Dimensions of the array are 
                        #determined by imgdataformat
                        camData = img.get_image_data_numpy()
                        #show acquired image 
                        newdata=np.subtract(np.divide(camData,128.0),1.0)
                        cam_image.setImage(newdata)
                        cam_image.draw()
                  
                        window.flip()
                        theKey = event.getKeys()
                        if len(theKey)!=0:
                            webcam_state=False
                            #stop data acquisition
                            cam.stop_acquisition()
                
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        data_packet=TSLOreceiver()
        
    # We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
    # accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
    # data
    if lsl_connection==True:
        outfilename = ("results/S0%d_S%d_T%03d_ecc_%d_Crowding_%s.csv" % ((data_packet[0]),(data_packet[1]),(data_packet[2]), eccentricity_deg,
                    time.strftime("%d%m%Y", time.localtime() ) ) )
        timerDuration=data_packet[3]
    else:
        outfilename = ("results/S0%s_S%s_T%03d_ecc_%d_%s_%s.csv" % ('0','0',0,eccentricity_deg, 'Crowding',
                    time.strftime("%m%d%Y", time.localtime() ) ) )
        timerDuration=1
    outfile = open(outfilename, "a")
    #Add the header row 
    outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,% s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc","fix_dur",
                    "cue_condition","ISI","stim_dur","RT","corr","stimTime") )
    #Draw a fixation cross and wait for the start of the experiment
    keepGoing=True
    while keepGoing:
        for stim1 in [fix_v, fix_h, fix_c]:
            stim1.draw()

        wait_msg.draw()
        window.flip()
        keys=event.getKeys()

        if len(keys)>0:
            keepGoing=False
            
    #set a timer so that the program waits for video to finish
    timer = core.CountdownTimer(timerDuration)
    #===================================================================================================================================
    ## Start of main loop of experiment
    for aflanker in flanker_spacing_list:           
        #An escape key press here terminates the loop
        if keys[0]=='escape':
            runExpt=False
            break
        # The following set of lines set the flanker condition 
        # Additionally the horizontal position of the target is randomized
        
        eccentricity_pix=eccentricity_deg*pixel_per_degree
        stim_location=np.random.choice(stim_location_list)
            
        if stim_location=='left':
            eccentricity=fixation_pos[0]-eccentricity_pix
        elif stim_location=='right':
            eccentricity=fixation_pos[0]+eccentricity_pix
        
        #We also have a condition where the target can be presented at the foveally
        # replacing the fixation cross
        if foveal_mode==True:
            target_location=(fix_h.start[0],0)
        else:
            target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
        target.pos=target_location
        if target_type=='bars':
            target_let='e'
            target_orientation=[0,270]
        elif target_type=='E':
            target_let='E'
            target_orientation=[0,90,180,270]
        elif target_type=='numbers':
            target_nums=['0','1','2','3','4','5','6','7','8','9']
            random.shuffle(target_nums)
            target_let=target_nums[0]
            flanker_let_1=target_nums[1]
            flanker_let_2=target_nums[2]
            flanker_let_3=target_nums[3]
            flanker_let_4=target_nums[4]

        #The letter size is set here
        letter_size_deg=letter_height
        letter_size_pix=letter_size_deg*pixel_per_degree
        target.height=letter_size_pix
        flanker_left.height=letter_size_pix
        flanker_right.height=letter_size_pix
        flanker_up.height=letter_size_pix
        flanker_down.height=letter_size_pix
            
        #THe flanker spacing is set on eacn trial based on the stimulus size
        flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
        flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
        flanker_left.pos=(target_location[0]-flanker_spacing_pix,0)
        flanker_right.pos=(target_location[0]+flanker_spacing_pix,0)
        flanker_up.pos=(target_location[0],flanker_spacing_pix)
        flanker_down.pos=(target_location[0],-flanker_spacing_pix)
                
        # Now that we have the stimulus size and flanker spacing we rotate the target letter and 
        # flankers independently 
        # Note: This would only have to be done in the case of tumbling E's and bars 
        if target_type=='E' or target_type=='bars':
            letter_orientation=([(np.random.choice(target_orientation)) for n in [0,1,2,3,4] ])
            target_orientation=letter_orientation[0]
            target.ori=letter_orientation[0]
            flanker_left.ori=letter_orientation[1]
            flanker_right.ori=letter_orientation[2]
            flanker_up.ori=letter_orientation[3]
            flanker_down.ori=letter_orientation[4]

        # we then set the respective target and flanker characters
        target.text=target_let
        flanker_left.text=flanker_let_1
        flanker_right.text=flanker_let_2
        flanker_up.text=flanker_let_3
        flanker_down.text=flanker_let_4

        # we can also switch the contrast between the target and flankers
        if reverse_flanker_contrast==True:
            flanker_left.color=[1,1,1]
            flanker_right.color=[1,1,1]
            flanker_up.color=[1,1,1]
            flanker_down.color=[1,1,1]

        # MOCS: to prevent the same mask from being used on every trial we shuffle the order of masks
        # to switch between the masks used for target and flankers
        masks=[mask_target,mask_inner_flanker,mask_outer_flanker,mask_up_flanker,mask_lower_flanker]
        random.shuffle(masks)
        target_mask=masks[0]
        left_flanker_mask=masks[1]
        right_flanker_mask=masks[2]
        up_flanker_mask=masks[3]
        lower_flanker_mask=masks[4]
            
        target_mask.pos=target_location #we center the mask with the stimulus
        left_flanker_mask.pos=flanker_left.pos
        right_flanker_mask.pos=flanker_right.pos
        up_flanker_mask.pos=flanker_up.pos
        lower_flanker_mask.pos=flanker_down.pos

        # The following set of code is used to present the stimulus & cue at pre-determined
        # locations and duration
        # The experimental sequence is as follows:
        # 1. Fixation cross for 450 msec
        # 2. Cue presentation for 50 msec (if cue_condition is set to True)
        # 3. Variable delay b/w 750-2000 msec 
        # 4. Stimulus presentation for 100 msec
        # 5. Post-mask for 200 msec
        # 6. Response window following mask offset and reaction time is measured in relation 
        #       to stimulus onset

        # the fixation period before each trial is set to a constant value
        clock.reset()
                
        for frameN in range(int(refreshRate*fixation_duration)):
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()  
            # We capture fixation target and save to image file if needed
            if save_image==True:# & frameN==0:
                window.getMovieFrame(buffer='back')
            window.flip()

        #The following set of lines is used to present the cue (neutral/valid) cue as a fraction
        # of the frame rate of the display
        for frameN in range(int(refreshRate*cue_presentation)):
            if cue_condition==True:
                if stim_location=='left':
                    for cue1 in [cue_target_h,cue_target_v_left1,cue_target_v_left2]:
                        cue1.draw()
                elif stim_location=='right':
                     for cue1 in [cue_target_h,cue_target_v_right1,cue_target_v_right2]:
                        cue1.draw() 
                
                # We capture the cue buffer and save to image file if needed
                if save_image==True:# & frameN==0:
                    window.getMovieFrame(buffer='back')
                    
                window.flip()
            else:
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()  
                window.flip()

        #we wait for a pseudo-random time before stimulus onset
        #to prevent anticipation
        clock.reset()
        ISI=np.random.choice(timing_list)
        while clock.getTime()< ISI:
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()            
            window.flip()
            
                    
        # The stimulus duration is used as a fraction of the frame rate of the display;
        # THis ensures better timing of the target presentations
        #we also the reset the kb clock to the reaction time in relation to stimulus onset
        kb_timer = core.CountdownTimer(timerDuration)
        # we clear key events that could occured after the end of the previous trial 
        event.clearEvents()
        if target_type=='numbers':
            target.render()
            flanker_right.render()
            flanker_left.render()
            flanker_up.render()
            flanker_down.render()
        for frameN in range(int(refreshRate*target_presentation)):
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw() 
            target.draw()                
            # we test whether the stimulus would appear on the left/right and based
            # on that we set the inner and outer flankers
            if stim_location=='left':                    
                inner_flanker=flanker_right
                outer_flanker=flanker_left
            elif stim_location=='right':
                inner_flanker=flanker_left
                outer_flanker=flanker_right
            # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
            if inner_flanker==True or foveal_mode==True:
                inner_flanker.draw()
            outer_flanker.draw()
            flanker_up.draw()
            flanker_down.draw()
                                                   
            # We only draw the fixation cross for the peripheral condition and not for the foveal one
            if foveal_mode==False:
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
            # we capture the stimulus on the buffer before adding in the square marker
            # This can later be saved if needed
            if save_image==True:# & frameN==0:
                window.getMovieFrame(buffer='back')

            #Draw the small square for Photodiode to detect and to create a marker in the video
            small_square.draw()

            flip_time=window.flip()
            if frameN==0:
                # we would use this to debug missed markers in the retinal video
                stimTime=timerDuration-timer.getTime()
                        
        # Followin the stimulus offset we have a post-mask presented immediately after the same
        if mask_condition==True:
            for frameN in range(int(refreshRate*mask_duration)):
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                # we would set the inner and outer flanker masks based on the position of the target/stimulus
                if stim_location=='left':
                    inner_flanker_mask=right_flanker_mask
                    outer_flanker_mask=left_flanker_mask
                elif stim_location=='right':
                    inner_flanker_mask=left_flanker_mask
                    outer_flanker_mask=right_flanker_mask
                # We would draw the inner flanker mask only when the inner flanker is also used otherwise we would skip this
                if inner_flanker==True or foveal_mode==True:
                    inner_flanker_mask.draw()
                # we would draw separate masks for target and flankers
                target_mask.draw()
                outer_flanker_mask.draw()
                up_flanker_mask.draw()
                lower_flanker_mask.draw()
                # we capture the mask buffer and save to image file when needed
                if save_image==True:# & frameN==0:
                    window.getMovieFrame(buffer='back')

                window.flip()
        #Use this to save the stimulus image
        if save_image==True:
            window.saveMovieFrames('Stimulus_'+str(np.abs(eccentricity_deg))+'_'+str(flanker_deg)+'_'+str(stim_location)+'.jpg')

                
        # we wait for the behavioral response from the subject. The waiting duration is set at the start of the experiment
        # The duration is mainly to ensure that the timing of each trial is pre-determined and can be used to estimate the overall
        # duration of the corresponding retinal video recording on the TSLO machine
        reaction_timer = core.CountdownTimer(response_window)
        keepGoing=True
        while reaction_timer.getTime()>0 and keepGoing==True:
            response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2)
            response_wait.draw()
                
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
                
            flip_time=window.flip()

            key=event.getKeys()
            if len(key)>0:
                response_time=kb_timer.getTime()
                keepGoing=False
            else:
                response_time=timerDuration
                        
        # we check if there was a keypress made duration the response window otherwise we set a defualt value
        if 'key' not in locals():
            key_pressed='Nil' # arbitary value 
        elif len(key)==0:
            key_pressed='Nil'
        else:
            key_pressed=key[0]
                
        #An escape key press here terminates the loop
        if key_pressed=='escape':
            break
                
        #we also store the reaction time
        RT=timerDuration-response_time
        # For the response keys we have separate conditions for when using tumbling e, 3 bar and numbers conditions
        if target_type=='E' or target_type=='bars':
            if key_pressed=='num_6' or key_pressed=='right' :
                response_dir=0
            elif key_pressed=='num_2' or key_pressed=='down':
                response_dir=90
            elif key_pressed=='num_4' or key_pressed=='left':
                response_dir=180
            elif key_pressed=='num_8' or key_pressed=='up':
                response_dir=270
            else:
                response_dir=45 #arbitary value to prevent the program from crashing
        elif target_type=='bars':
            if key_pressed=='right' or key_pressed=='left' or key_pressed=='num_6' or key_pressed=='num_4':
                response_dir=0
            elif key_pressed=='down' or key_pressed=='up' or key_pressed=='num_2' or key_pressed=='num_8':
                response_dir=270
            else:
                response_dir=45 #arbitary value to prevent the program from crashing
        elif target_type=='numbers':
            if key_pressed=='num_0':
                response_num='0'
            elif key_pressed=='num_1':
                response_num='1'
            elif key_pressed=='num_2':
                response_num='2'
            elif key_pressed=='num_3':
                response_num='3'
            elif key_pressed=='num_4':
                response_num='4'
            elif key_pressed=='num_5':
                response_num='5'
            elif key_pressed=='num_6':
                response_num='6'
            elif key_pressed=='num_7':
                response_num='7'
            elif key_pressed=='num_8':
                response_num='8'
            elif key_pressed=='num_9':
                response_num='9'
            else:
                response_num='100'#arbitary value to prevent the program from crashing; happens when no key is pressed

        if target_type=='E' or target_type=='bars':
            if response_dir==target_orientation:
                beep_sound_correct.play()
                beep_feedback.reset(1)
                corr=1
            else:
                corr=0
                beep_sound_incorrect.play()
                beep_feedback.reset(1)
        elif target_type=='numbers':
            if response_num==target_let:
                beep_sound_correct.play()
                beep_feedback.reset(1)
                corr=1
            else:
                beep_sound_incorrect.play()
                beep_feedback.reset(1)
                corr=0
        # we write the responses to the output file     
        if key_pressed!='escape':
            if target_type=='E' or target_type=='bars':
                outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%2f\n"%(response_dir,target.ori,inner_flanker.ori,outer_flanker.ori,flanker_up.ori,flanker_down.ori,
                letter_size_deg,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT,corr,stimTime) )

            elif target_type=='numbers':
                outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%s,%2f,%2f,%2f,%d,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                letter_size_deg,eccentricity_deg,aflanker,stim_location,fixation_duration,cue_condition,ISI,target_presentation,RT,corr,stimTime) )
        
        # Would indicate to the TSLO machine that this is not the last trial
        if lsl_connection==True:
            sequenceComplete=0
            TSLOsender(sequenceComplete,0)
            
    outfile.close()

    # The program waits here for until the recording duration finishes
    # Note: We would generally have the TSLO record the retinal videos for a little bit longer after
    # the presentation of the last stimulus so as to ensure that the recording does not stop abruptly 
    # If the subjects finish sooner than the end of the video recording, the program would wait at this point
    # and provide a fixation target for the subjects to fixate upon during this time.
    while timer.getTime()>0:
        for stim1 in [fix_v, fix_h, fix_c]:
            stim1.draw()
        wait_msg2.draw()
        window.flip()
    
    #In case the TSLO machine is not connected we would stop after one run of MOCS otherwise we restart the program
    if lsl_connection==False:
        keep_repeating=False

    # We would print out a message for the subject and wait for them to hit a key before restarting the program
    loop_msg1=visual.TextStim(window,text='Block Complete!',pos=(0,0),height=pixel_per_degree/4)
    loop_msg2=visual.TextStim(window,text='Press any key to continue!',pos=(0,-150),height=pixel_per_degree/4)
    loop_msg1.draw()
    loop_msg2.draw()
    window.flip()
    event.waitKeys()

    
#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=pixel_per_degree)
end_msg.draw()
window.flip()
event.waitKeys()

window.close()
core.quit()