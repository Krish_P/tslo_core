import numpy as np
import psychopy.visual as visual

# Include these for debugging (e.g. to imshow the pixlets)
#import matplotlib.pyplot as plt
#import matplotlib.cm as cm

RANDOM_LETTER=chr(200)
RANDOM_NUMBER=chr(201)
RANDOM_T=chr(202)
RANDOM_BAR=chr(203)
RANDOM_E=chr(204)

# Codes for 'LCD' segments:
# 000
# 3 5
# 111
# 4 6
# 222
#
# 7: Vertical stem in middle taking whole col.
charbits={
    'A': [ 1,1,0,1,1,1,1,0 ], #A, 0  *
    'B': [ 0,0,0,0,0,0,0,0 ], #B, 1
    'C': [ 1,0,1,1,1,0,0,0 ], #C, 2  *
    'D': [ 0,0,0,0,0,0,0,0 ], #D, 3
    'E': [ 1,1,1,1,1,0,0,0 ], #E, 4  *
    'F': [ 1,1,0,1,1,0,0,0 ], #F, 5  *
    'G': [ 0,0,0,0,0,0,0,0 ], #G, 6
    'H': [ 0,1,0,1,1,1,1,0 ], #H, 7  *
    'I': [ 1,0,1,0,0,0,0,1 ], #I,
    'J': [ 0,0,1,0,1,1,1,0 ], #J, 9  *
    'K': [ 0,0,0,0,0,0,0,0 ], #K, 10
    'L': [ 0,0,1,1,1,0,0,0 ], #L, 11 *
    'M': [1,0,0,1,1,1,1,1],#E rot: line on top
    'N': [ 0,0,0,0,0,0,0,0 ], #N, 13
    'O': [ 1,0,1,1,1,1,1,0 ], #O, 14 *
    'P': [ 1,1,0,1,1,1,0,0 ], #P, 15 *
    'Q': [ 0,0,0,0,0,0,0,0 ], #Q, 16
    'R': [ 0,0,0,0,0,0,0,0 ], #R, 17
    'S': [ 1,1,1,1,0,0,1,0 ], #S, 18 *
    'T': [ 1,0,0,0,0,0,0,1 ], #T, *
    'U': [ 0,0,1,1,1,1,1,0 ], #U, 20 *
    'V': [ 0,0,0,0,0,0,0,0 ], #V, 21
    'W': [0,0,1,1,1,1,1,1],#E_rot: line on btm
    'X': [ 0,0,0,0,0,0,0,0 ], #X, 23
    'Y': [ 0,0,0,0,0,0,0,0 ], #Y, 24
    'Z': [ 0,0,0,0,0,0,0,0 ], #Z, 25

    '0': [ 1,0,1,1,1,1,1,0 ], #0
    '1': [ 0,0,0,0,0,0,0,1 ], #1
    '2': [ 1,1,1,0,1,1,0,0 ], #2
    '3': [ 1,1,1,0,0,1,1,0 ], #3
    '4': [ 0,1,0,1,0,1,1,0 ], #4
    '5': [ 1,1,1,1,0,0,1,0 ], #5
    '6': [ 1,1,1,1,1,0,1,0 ], #6
    '7': [ 1,0,0,0,0,1,1,0 ], #7
    '8': [ 1,1,1,1,1,1,1,0 ], #8
    '9': [ 1,1,1,1,0,1,1,0 ], #9

    'u': [ 1,0,0,0,0,0,0,1 ], #T ('upright')
    'd': [ 0,0,1,0,0,0,0,1 ], #T rot 180 deg CCW ('down')
    'l': [ 0,1,0,1,1,0,0,0 ], #T rot  90 deg CCW ('left')
    'r': [ 0,1,0,0,0,1,1,0 ], #T rot 270 deg CCW ('right')

    'x': [ 1,1,1,1,1,1,1,1 ],   # grid-like figure

    'e': [1,0,0,1,1,1,1,1],     #E rot: line on top ('pointed' down), clockwise 90
    'f': [0,0,1,1,1,1,1,1],     #E_rot: line on btm

    'h': [1,1,1,0,0,0,0,0],# horizontal 'grating'
    'v': [0,0,0,1,1,1,1,1],# vertical 'grating'

    '+': [0,1,0,0,0,0,0,1],# plus
}

set_numbers=np.array(['0','1','2','3','4','5','6','7','8','9'])
set_letters=np.array(['A','C','E','F','H','I', 'J','L','O','P','S','T','U'])
set_T=np.array(['u','d','l','r'])
set_bar=np.array(['v','h'])
set_E=np.array(['E','3','e','f'])

def charset(which):
    if which==RANDOM_LETTER:
        whichset=set_letters
    elif which==RANDOM_NUMBER:
        whichset=set_numbers
    elif which==RANDOM_T:
        whichset=set_T
    elif which==RANDOM_BAR:
        whichset=set_bar
    elif which==RANDOM_E:
        whichset=set_E

    return whichset

def charn(whichset,which):
    thechar=whichset[which]
    return thechar

def random_char(which):
    whichset=charset(which)
    whichidx=np.random.randint(len(whichset))
    thechar=charn(whichset,whichidx)
    return thechar

def make_let(whichlet, size=10, buf=None, size_buf=None, loc_x=0, loc_y=0, size_bar=0, spacing=10):
    try:
        if buf==None:
            if size_buf==None:
                size_buf=(size*5,size*5)
            buf = np.zeros( size_buf )
    except ValueError:
        pass

    # Identify coordinates of interest (coordinates inside buffer)
    midl_x = size_buf[1]//2-size//2+loc_x
    midl_y = size_buf[0]//2-size//2+loc_y
    tl_x = midl_x-size*2
    tl_y = midl_y-size*2
    br_x = midl_x+size*2 # inner edge
    br_y = midl_y+size*2 # inner edge
    br2_x = br_x+size #outer edge
    br2_y = br_y+size #outer edge

    if ord(whichlet) >= ord(RANDOM_LETTER): # anything large is random
        whichlet=random_char(whichlet)
        stim_def=charbits[whichlet]
    else:
        stim_def = charbits[whichlet]

    # Set bits based on features in character
    if stim_def[0]: #L
        buf[tl_y:tl_y+size,tl_x:br2_x]=1
    if stim_def[1]: #R
        buf[midl_y:midl_y+size,tl_x:br2_x]=1
    if stim_def[2]:
        buf[br_y:br2_y,tl_x:br2_x]=1
    if stim_def[3]:
        buf[tl_y:midl_y,tl_x:tl_x+size]=1
    if stim_def[4]:
        buf[midl_y:br2_y,tl_x:tl_x+size]=1
    if stim_def[5]:
        buf[tl_y:midl_y,br_x:br2_x]=1
    if stim_def[6]:
        buf[midl_y:br2_y,br_x:br2_x]=1
    if stim_def[7]:
        buf[tl_y:br2_y,midl_x:midl_x+size]=1

    # bar flankers, if requested
    if size_bar>0:
        spacingee=spacing-size*5
        buf[br2_y+spacingee:br2_y+spacingee+size_bar,tl_x:br2_x]=1 # bottom
        buf[tl_y-spacingee-size_bar:tl_y-spacingee,tl_x:br2_x]=1 # top
        buf[tl_y:br2_y,tl_x-spacingee-size_bar:tl_x-spacingee]=1 # left
        buf[tl_y:br2_y,br2_x+spacingee:br2_x+spacingee+size_bar]=1 # right

    return buf,whichlet

def bufshow(buf):
    plt.imshow( buf, interpolation='Nearest', cmap=cm.bone)
    plt.show()

def plotlet(whichlet, size=10, size_buf=(100,100), loc_x=0, loc_y=0, size_bar=0, spacing=10):
    buf,whichlet = make_let(whichlet, size=size, size_buf=size_buf, size_bar=size_bar, spacing=spacing)
    bufshow(buf)

def quad(target, flankers, cc_spacing, size=10, size_buf=(100,100),bitvals=[0,1],size_bar=0):
    buf=np.zeros( size_buf )
    _,let_t=make_let(target, buf=buf, size=size, loc_x=0, loc_y=0, size_bar=size_bar,spacing=cc_spacing)

    # The following odd line allows bars w/ or w/o flankers
    if size_bar < 100: #==0:
        _,let_f1=make_let(flankers, buf=buf, size=size, loc_x=-cc_spacing, size_bar=0)
        _,let_f2=make_let(flankers, buf=buf, size=size, loc_x=+cc_spacing, size_bar=0)
        _,let_f3=make_let(flankers, buf=buf, size=size, loc_y=-cc_spacing, size_bar=0)
        _,let_f4=make_let(flankers, buf=buf, size=size, loc_y=+cc_spacing, size_bar=0)
    else:
    	let_f1='-'
    	let_f2='-'
    	let_f3='-'
    	let_f4='-'

    if bitvals != [0,1]:
        buf[buf==1]=99 # sentinel
        buf[buf==0]=bitvals[0]
        buf[buf==99]=bitvals[1]

    return buf,[let_t,let_f1,let_f2,let_f3,let_f4]

################################################
# Object-oriented class for generic display
################################################
class PixStim(object):
    # Pixel-buf object
    def __init__(self, win=None, params=None):
        self.params=params
        self.win=win
        if win:
            self._stim = visual.ImageStim( win, texRes=1, mask="none", pos=(0,0), units='pix',
                                      size=params.size_buf, colorSpace='rgb', color=[1,1,1] )
        else:
            self._stim=None

    # In drawing loop. Should be fast.
    def draw(self):
        if not(self._stim==None):
            self._stim.draw()
    def __repr__(self):
        return "Stim({0.params!r})".format(self)
    # For trials, update things
    def update(self, spac, trial_num,size=10):
        # Store generated image directly in PsychoPy widget buffer
        self.bits,self.rands=quad(
            self.params.quad_target, self.params.quad_flanker,
                spac,size,
                self.params.size_buf, bitvals=self.params.bitvals, size_bar=self.params.size_bar)
        if self._stim:
            if self.params.flipud:
                self._stim.image=np.flipud(self.bits)
            else:
                self._stim.image=np.flipud(self.bits)


class PixLet(object):
    def __init__(self, win, **kwargs):
        self.win=win
        self.pos=kwargs.get('pos',(0,0))
        self.ori=kwargs.get('ori',0)
        self.height=kwargs.get('height',0)
        self.text=kwargs.get('text','E')        # Letter X is blank (but not an error)
        color=kwargs.get('color',(-1,-1,-1))

        # Most things specified when drawn:
        self._stim = visual.ImageStim( win, units='pix',colorSpace='rgb', color=color)
#        buf,_=make_let(self.text,int(self.height//5))        # First bits directly into ImageStim buffer
#        self.buf=buf
#        self.size_buf=np.shape(buf)
    def draw(self):
        self._stim.draw()                           # Draw the widget

    def render(self):
        # Draw only needs a letter (in self.text) and a size (height)
        # size (or height) should be the entire size of the letter, which should be divisible by 5
#        make_let(self.text,int(self.height//5),self.buf,self.size_buf)        # First bits directly into ImageStim buffer
        buf,_=make_let(self.text,int(self.height//5))
        self._stim.image = np.flipud( buf )         # Seems necessary to do vertical flip 
        self._stim.size = (self.height,self.height) # For more recent version of PsychoPy, size= needs to go AFTER image= (which clobbers size)
        self._stim.pos=self.pos                     # Copy any position into the widget
        self._stim.ori=self.ori
