from ximea import xiapi
import cv2
import time
from psychopy import visual, core, monitors,event
import numpy as np

monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
# Draw basic shapes which will be draw later in the loop
fix_xpos=0
step_size=20
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=15, fillColor=(1,1,1), pos=(0,0), lineColor=(0,0,0) )

image=visual.ImageStim(win=window,pos=[0,0],size=[1000,1000],opacity=1.0)

#create instance for first connected camera 
cam = xiapi.Camera()

#start communication
cam.open_device()

#settings
cam.set_exposure(10000)

#create instance of Image to store image data and metadata
img = xiapi.Image()

#start data acquisition
cam.start_acquisition()

try:
    print('Starting video')
    t0 = time.time()
    while True:
        #get data and pass them from camera to img
        cam.get_image(img)

        #create numpy array with data from camera. Dimensions of the array are 
        #determined by imgdataformat
        data = img.get_image_data_numpy()
        #show acquired image 
        newdata=np.subtract(np.divide(data,128.0),1.0)
        image.setImage(newdata)
        image.draw()
        for stim1 in [fix_v, fix_h, fix_c]:
            stim1.draw()
  
        window.flip()
        
        #break when we get a keypress
        keys=event.getKeys()
        
        if len(keys) != 0:
            break
        
except KeyboardInterrupt:
    cv2.destroyAllWindows()

#stop data acquisition
print('Stopping acquisition...')
cam.stop_acquisition()

#stop communication
cam.close_device()

print('Done.')
