#This function calculates the pixel to degree conversion
import numpy as np
def PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen):
    #Pixel Calculations: Shows the pixel to degree conversion

    diagonal_pixel=np.sqrt(((horz_res)**2)+((vert_res)**2))
    pixel_per_cm=diagonal_pixel/diagonal_size
    
    # Visual angle calculation: We use the above info to calculate number of pixels
    #that fall in a degree of visual angle
    cm_per_degree=dist_to_screen*np.pi/180
    pixels_per_degree= cm_per_degree * pixel_per_cm
    
    return pixels_per_degree,pixel_per_cm

if __name__=="__main__":
    ppd,ppc=PixelConversion(2160,3840,68.58,245)
    print(ppd,ppc)