from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import random
from psychopy.sound import Sound
from psychopy import logging
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
import pixlet
from ReceiveDataFunction import TSLOreceiver #This would be used to communicate with the TSLO machine
from PixelConversion import PixelConversion
from make_mask import make_mask
import math
clock = psychopy.core.Clock()
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
letter_size=(6.25/60)
subj_id=2
session_num=1
trial_num=1
#===================================================================================================================================
# Equipment setup
#We enter the monitor parameters here and calculate pixels per cm
horz_res=2160
vert_res=3840
monsize =  [horz_res, vert_res]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=1)
window.setMouseVisible(False)
# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
# We round up the frame rate
refreshRate=(window.getActualFrameRate())
diagonal_size=68.58 #diagonal screen size
dist_to_screen= 245 #cm (for the mirror setup with 230cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree,ppc=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
fixation_type='x'
onlyInfHemifield=True # show stimuli only below fixation

if fixation_type=="+":
    fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
elif fixation_type=='x':
    fix_line_UR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(fix_xpos,0), end=(fix_xpos-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
fix_c=visual.Circle(window, size=8, fillColor=(1,1,1), pos=(fix_xpos,0), lineColor=(1,1,1) )


# Set experimental parameters here
eccentricity_deg=0.33
flanker_space=[100]#[2.50]
random.shuffle(flanker_space)
cue_condition=False   
target_type='T'

if target_type=='T':
    # we can flankers to be either T's or +'s
    flanker_type='+'


# These parameters are set locally (Stimulus Machine)
num_repeats=1 # sets the number of times the quest is repeated for each flanker spacing
mask_condition=False
location_uncertainity=True 
inner_flanker_condition=True
reverse_flanker_contrast=False

# These parameters would be set at the start of the experiment and ideally would not want to change them 
# after the start of the experiment
#Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
# on the fixation cross for variable duration before stimulus onset
# Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
cue_presentation= 0.050 #sec
fixation_duration=np.random.choice(np.arange(0.500,0.80,0.100)) #sec 
mask_duration=0.200 #sec
response_window=2.00#sec
#===================================================================================================================================
## Experiment Stimuli: Here we create the different stimuli that would later be used in the experiment

# The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
if location_uncertainity==True:
    #stim_location_list=['left','right']
    if onlyInfHemifield==True:
        stim_location_list=['lowerLeft','lowerRight']
    else:
        stim_location_list=['upperLeft','upperRight','lowerLeft','lowerRight']
else:
    stim_location_list=['right']
fix_xpos=0
msg_offset=400
msg_size=pixel_per_degree//6
step_size=20 #pixels
fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line

#Create basic stimuli here that could be called multiple times during the course of the experiment
small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Press any key to start')
wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Please wait...')   

# The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
# Cue for rightward target location
cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
#cue for leftward target location
cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
# Cue for target location
target_c=visual.Circle(window, size=2, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
# For oblique targets presented at 1 of 4 locations we have separate cues
target_c_ul=visual.Circle(window, size=2, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
target_c_ur=visual.Circle(window, size=2, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
target_c_ll=visual.Circle(window, size=2, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
target_c_lr=visual.Circle(window, size=2, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )

#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)

# We also create a set of post masks for different letter sizes that might be used in the experiment. 
# Note: The main advantage of pre-rendered masks is to ensure smooth operation of the script 
# Currently we generate 6 different masks with letter sizes set based on the starting point of the subsequent quest method
if mask_condition==True:
    prog_bar = visual.Rect(win=window, pos=(0,0), size=(200,30), lineColor='black',fillColor='black')
    render_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=msg_size,text='Rendering Masks...')
    render_msg.draw()
    window.flip()

    mask_sizes=[30,40,50,60,70,80]
    mask_density=0.4

    mask0=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[0],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(6)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask1=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[1],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(5)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask2=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[2],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(4)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask3=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[3],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(3)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()
    window.flip()

    mask4=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[4],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(2)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()  
    window.flip()

    mask5=make_mask(win=window,mask_size=(400,400),letter_height=mask_sizes[5],fontname='Sloan',density=mask_density,tumbling_e=True) 
    prog_bar.size=(200*(1)/len(mask_sizes),50)
    render_msg.draw()
    prog_bar.draw()
    window.flip()

#===================================================================================================================================
## Experimental sequence:

#We use the position of the fixation cross center to position the target and flankers
fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 
                              

# This is the outer bigger loop which controls the different flanker spacing/letter height and in the inner loop the other parameter is 
# tested.  
#For eg: when the main outer loop (such as the part below) is looping over the different flanker spacing, the inner loop would test/QUEST 
# over the different letter sizes for that flanker spacing.
for aflanker in flanker_space:
    #The flanker spacing & stimulus size  is kept constant and the stimulus duration is adjusted in the QUEST paradigm
    # QUEST parameters are set at this point which would then be used in a loop
    # to run the staircase method
    # we get the letter size from the dictionary created earlier
    starting_stim_duration=12 # in terms of flips or 100 ms, when running at 120Hz
    log_starting_dur=np.log10(starting_stim_duration)
    staircase = data.QuestHandler(log_starting_dur,  0.5, pThreshold=0.75, gamma=0.5,beta=3.5,nTrials=32,
                                    minVal=0.30)# min val corresponds to 2 flips @ 120Hz stimulus
    
    outfilename = ("results/S0%s_S%s_T%03d_%s.csv" % (subj_id,session_num,trial_num, 'Stim_Duration' ) )
    threshold_filename=("results/S0%d_S%d_T%03d_CrowdingThresh.csv" % ((subj_id),(session_num),(trial_num)) )
    #Add the header row 
    if os.path.exists(outfilename):
        outfile = open(outfilename, "a")
    else:
        outfile = open(outfilename, "a")
        outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,% s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
                    "cue_location","stim_dur","RT","corr","ntrial") )
                    
    if os.path.exists(threshold_filename):
        thresh_file=open(threshold_filename,"a")
    else:
        thresh_file=open(threshold_filename,"a")
        thresh_file.write("%s,%s,%s"%("ecc","duration_ms","duration_flips\n"))
    
    timerDuration=1
    #Draw a fixation cross and wait for the start of the experiment
    keepGoing=True
    while keepGoing:
        if fixation_type=='+':
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
        elif fixation_type=='x':
            if onlyInfHemifield==True:
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                    stim1.draw()
            else:
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                    stim1.draw()
        wait_msg.draw()
        window.flip()
        keys=event.getKeys()

        if len(keys)>0:
            keepGoing=False
    
    for repeat in range(num_repeats):
        #This is where the QUEST method begins
        #set a timer so that the program waits for video to finish
        timer = core.CountdownTimer(timerDuration)
        # In order to ensure that the quest ends after N number of trials we set the number of trials 
        # needed which is tested on every iteration of the loop/experiment
        questCounter=1
        for stim_dur in staircase:
            #This is used to provide a audio feedback to the user 
            beep_feedback=core.CountdownTimer()
            beep_sound_correct=Sound(value=1000,secs=0.25)
            beep_sound_incorrect=Sound(value=250,secs=0.25)
            # We set the letter height here. The letter height would be a multiple of letter size(which corresponds)
            # to threshold measures from pilot data
            letter_height = letter_size
            # We convert duration from log scale
            stim_dur=int(10**stim_dur)
            #An escape key press here terminates the loop
            if keys[0]=='escape':
                runExpt=False
                break
            # The following set of lines set the flanker condition 
            stim_location=np.random.choice(stim_location_list)

            eccentricity_pix=int(eccentricity_deg*pixel_per_degree)
            
            # Can either be left or right
            if stim_location=='left':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentrcitiy_y=0
            elif stim_location=='right':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=0
            # Or can be along 4 diagonal meridians from fixation
            elif stim_location=='upperLeft':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentricity_y=fixation_pos[1]+eccentricity_pix
            elif stim_location=='upperRight':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=fixation_pos[1]+eccentricity_pix
            elif stim_location=='lowerLeft':
                eccentricity_x=fixation_pos[0]-eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix
            elif stim_location=='lowerRight':
                eccentricity_x=fixation_pos[0]+eccentricity_pix
                eccentricity_y=fixation_pos[1]-eccentricity_pix
            # we then set the target location randomly based on the different location choices
            # available 
            target_location=(eccentricity_x,eccentricity_y) # Center (pixels) of letter or trigram to identify 
            
            target.pos=target_location
            # We set the position of the target placeholder using the eccentricity provided
            # and the location of the fixation cross
            target_c_ul.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]+eccentricity_pix))
            target_c_ur.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]+eccentricity_pix))
            target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
            
            if target_type=='bars':
                target_type_list=['h','v']*3
            elif target_type=='T':
                if flanker_type=='+':
                    target_type_list=['+']*5
                elif flanker_type=='T':
                    target_type_list=['u','d','l','r']
                    # Since we need five different stimuli we randomly add a fifth one
                    target_type_list.append(random.choice(target_type_list))
            elif target_type=='numbers':
                target_type_list=['0','1','2','3','4','5','6','7','8','9']
            elif target_type=='E':
                target_type_list=['E','3','e','f']
                # Since we need five different stimuli we randomly add a fifth one
                target_type_list.append(random.choice(target_type_list))
            # We shuffle the list of stimuli before assigning them
            random.shuffle(target_type_list)
            # We first assign target stimuli
            if target_type=='T' and flanker_type=='+': # we set the target to be a T with random orientation
                target_lists=['u','d','l','r']
                target_let=np.random.choice(target_lists)
            elif target_type=='T' and flanker_type=='T':
                target_let=target_type_list[0]
                
            # First we would reset fixation cross to original color
            fix_line_UL.color=[-1,-1,-1]
            fix_line_UR.color=[-1,-1,-1]
            fix_line_LL.color=[-1,-1,-1]
            fix_line_LR.color=[-1,-1,-1]
#            if stim_location=='upperLeft':
#                fix_line_UL.color=[1,1,1]
#            elif stim_location=='upperRight':
#                fix_line_UR.color=[1,1,1]
#            elif stim_location=='lowerLeft':
#                fix_line_LL.color=[1,1,1]
#            elif stim_location=='lowerRight':
#                fix_line_LR.color=[1,1,1]
            # We finally assign flanker stimuli
            flanker_let_1=target_type_list[1]
            flanker_let_2=target_type_list[2]
            flanker_let_3=target_type_list[3]
            flanker_let_4=target_type_list[4]

            #The letter size is set on each run by the QUEST method 
            letter_size_deg=letter_height
            letter_size_pix=letter_size_deg*pixel_per_degree
            target.height=letter_size_pix
            flanker_left.height=letter_size_pix
            flanker_right.height=letter_size_pix
            flanker_up.height=letter_size_pix
            flanker_down.height=letter_size_pix
            
            #THe flanker spacing is set on eacn trial based on the stimulus size
            flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
            
            if stim_location!='left' or stim_location!='right':
                flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1]-flanker_spacing_pix)
                flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1]+flanker_spacing_pix)
                flanker_up.pos=(target_location[0]-flanker_spacing_pix,target_location[1]+flanker_spacing_pix)
                flanker_down.pos=(target_location[0]+flanker_spacing_pix,target_location[1]-flanker_spacing_pix)
            else:
                flanker_left.pos=(target_location[0]-flanker_spacing_pix,target_location[1])
                flanker_right.pos=(target_location[0]+flanker_spacing_pix,target_location[1])
                flanker_up.pos=(target_location[0],target_location[1]+flanker_spacing_pix)
                flanker_down.pos=(target_location[0],target_location[1]-flanker_spacing_pix)

            # we then set the respective target and flanker characters
            target.text=target_let
            flanker_left.text=flanker_let_1
            flanker_right.text=flanker_let_2
            flanker_up.text=flanker_let_3
            flanker_down.text=flanker_let_4
        
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]
            # we decide which mask to use based on the size of the letter in the current trial in order to ensure that
            # the letter size in the mask matches with the target size (maximize the effect)
            if mask_condition==True:
                if letter_size_pix<35:
                    mask=mask0
                elif 35<letter_size_pix<45:
                    mask=mask1
                elif 45<letter_size_pix<55:
                    mask=mask2
                elif 55<letter_size_pix<65:
                    mask=mask3
                elif 65<letter_size_pix<75:
                    mask=mask4
                elif 75<letter_size_pix  :
                     mask=mask5
                
                mask.pos=target_location #we center the mask with the stimulus
            
            target_presentation=stim_dur
    
            # the fixation period before each trial is set to a constant value
            clock.reset()
            
            while clock.getTime()< fixation_duration:
                if onlyInfHemifield==True:
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                        stim1.draw()
                else:
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                        stim1.draw()
          
                window.flip()

            #The following set of lines is used to present the cue (neutral/valid) cue as a fraction
            # of the frame rate of the display
            if cue_condition==True:
                for frameN in range(int(refreshRate*cue_presentation)):
                    if stim_location=='left':
                        for cue1 in [cue_target_h,cue_target_v_left1,cue_target_v_left2]:
                            cue1.draw()
                    elif stim_location=='right':
                         for cue1 in [cue_target_h,cue_target_v_right1,cue_target_v_right2]:
                            cue1.draw() 
                    else:
                        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                            stim1.draw()
                    window.flip()
                
            # The stimulus duration is used as a fraction of the frame rate of the display;
            # we first render the target and flankers prior to presentation
            target.render()
            flanker_right.render()
            flanker_left.render()
            flanker_up.render()
            flanker_down.render()
            # THis ensures better timing of the target presentations
            #we also the reset the kb clock to the reaction time in relation to stimulus onset
            event.clearEvents()
            kb_timer = core.CountdownTimer(timerDuration)
            for frameN in range(target_presentation):
                target.draw()                
                # we test whether the stimulus would appear on the left/right and based
                # on that we set the inner and outer flankers
                if stim_location=='left' or stim_location=='upperLeft' or stim_location=='lowerLeft':                    
                    inner_flanker=flanker_right
                    outer_flanker=flanker_left
                elif stim_location=='right' or stim_location=='upperRight' or stim_location=='lowerRight':
                    inner_flanker=flanker_left
                    outer_flanker=flanker_right
                # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
                inner_flanker.draw()
                outer_flanker.draw()
                flanker_up.draw()
                flanker_down.draw()
                #Draw the small square for Photodiode to detect and to create a marker in the video
                #small_square.draw()
                for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                    stim1.draw()    
                flip_time=window.flip()
            
            # Followin the stimulus offset we have a post-mask presented immediately after the same
            if mask_condition==True:
                for frameN in range(int(refreshRate*mask_duration)):
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c]:
                        stim1.draw()    
                    mask.draw()
                    window.flip()


            
            # we wait for the behavioral response from the subject. The waiting duration is set at the start of the experiment
            # The duration is mainly to ensure that the timing of each trial is pre-determined and can be used to estimate the overall
            # duration of the corresponding retinal video recording on the TSLO machine
            reaction_timer = core.CountdownTimer(response_window)
            keepGoing=True
            while reaction_timer.getTime()>0 and keepGoing==True:
                response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=msg_size)
                response_wait.draw()
                
                if onlyInfHemifield==True:
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
                        stim1.draw()
                else:
                    for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ul,target_c_ur,target_c_ll,target_c_lr]:
                        stim1.draw()
                
                flip_time=window.flip()

                key=event.getKeys()
                if len(key)>0:
                    response_time=kb_timer.getTime()
                    keepGoing=False
                else:
                    response_time=timerDuration
                    
            # we check if there was a keypress made duration the response window otherwise we set a defualt value
            if 'key' not in locals():
                key_pressed='Nil' # arbitary value 
            elif len(key)==0:
                key_pressed='Nil'
            else:
                key_pressed=key[0]
                
            #An escape key press here terminates the loop
            if key_pressed=='escape':
                break
                
            #we also store the reaction time
            RT=timerDuration-response_time
            # we assign a response value based on the keypress that was made by the subject
            if target_type=='numbers':
                if key_pressed=='num_0':
                    response_key='0'
                elif key_pressed=='num_1':
                    response_key='1'
                elif key_pressed=='num_2':
                    response_key='2'
                elif key_pressed=='num_3':
                    response_key='3'
                elif key_pressed=='num_4':
                    response_key='4'
                elif key_pressed=='num_5':
                    response_key='5'
                elif key_pressed=='num_6':
                    response_key='6'
                elif key_pressed=='num_7':
                    response_key='7'
                elif key_pressed=='num_8':
                    response_key='8'
                elif key_pressed=='num_9':
                    response_key='9'
                else:
                    response_key='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
            elif target_type=='bars':
                if key_pressed=='left' or key_pressed=='right' or key_pressed=='num_4' or key_pressed=='num_6':
                    response_key='h'
                elif key_pressed=='up' or key_pressed=='down' or key_pressed=='num_8' or key_pressed=='num_2':
                    response_key='v'
                else:
                    response_key='100'
            elif target_type=='T':
                if key_pressed=='up':
                    response_key='u'
                elif key_pressed=='down':
                    response_key='d'
                elif key_pressed=='left':
                    response_key='l'
                elif key_pressed=='right':
                    response_key='r'
                else:
                    response_key='100'
            elif target_type=='E':
                if key_pressed=='right':
                    response_key='E'
                elif key_pressed=='left':
                    response_key='3'
                elif key_pressed=='up':
                    response_key='e' # need to verify this
                elif key_pressed=='down':
                    response_key='f'
                else:
                    response_key='100'
            # we woudl provide feedback based on whether or not the response was correct
            if response_key==target.text:
                beep_sound_correct.play()
                corr=1
            else:
                beep_sound_incorrect.play()
                corr=0
                
            # we write the responses to the output file     
            # we convert target presentation to ms from flips before writing to output file
            target_presentation_ms=stim_dur*(1000/refreshRate)
            if key_pressed!='escape':
                outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%s,%2f,%2f,%d,%d\n"%(response_key,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                letter_size_deg,eccentricity_deg,aflanker,stim_location,stim_location,target_presentation_ms,RT,corr,questCounter) )
                questCounter+=1
            #inform QUEST of the response, needed to calculate next level
            staircase.addResponse(corr)
        
        outfile.close()
        # we get the threshold from the quest method and write the results to a separate file
        # We again convert from flips to ms
        s_flips=(10**(staircase.mean()))#mean threshold from staircase method
        s_ms=s_flips*(1000/refreshRate)
        print('The threshold is %2f ms or %d flips'%(target_presentation_ms,s_flips))
        thresh_file.write("%d,%2f,%d\n"%(eccentricity_deg,s_ms,s_flips))
        thresh_file.close()
    
#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=msg_size)
end_msg.draw()
window.flip()
event.waitKeys()

window.close()
core.quit()