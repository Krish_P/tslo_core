#from concurrent.futures.process import _threads_wakeups
from multiprocessing.connection import Client
from multiprocessing.connection import Listener
from xml.etree.ElementTree import TreeBuilder
import nidaqmx
from nidaqmx.constants import TerminalConfiguration
from nidaqmx.constants import AcquisitionType
import numpy as np
import random
import time
import pandas as pd
from datetime import datetime
import customTime
import math
import sys
import socket
from ReceiveDataFunction import stimMachineRecv
from pylsl import StreamInlet, resolve_stream

# Main set of parameters for the saccade detection method
vel_thresh=8
num_samples_above_thresh=3


# We first get the saccade detect method
import online_sac_detect_module
keepGoing=True

# Save logile function
def save_logfile(eyeposFilename,time_array,xpos_array,ypos_array,detected_array):
    # now save the eye position data that we have so far
    # we save the eye positions to a text file
    eyeposSignal=np.column_stack((time_array[0:array_idx],xpos_array[0:array_idx],ypos_array[0:array_idx],
        detected_array[0:array_idx]) )
    eyeposDF=pd.DataFrame(eyeposSignal)
    eyeposDF.columns=['time','xpos','ypos','sac_detected']
    eyeposDF.to_csv(eyeposFilename,index=False)
    print('Data saved to: %s!!'%(eyeposFilename),flush=True)

# We can indicate whether we are using the socket on same machine or not
# When using multiple machines we receive stimulus info from the stimulus machine
# through sockets and we don't need socket going the other way
# since messages are sent via parallel port
socket_conn=True
multiple_machines=True
if multiple_machines==True:
    # We can either send saccade messages through socket (if using the same machine)
    # otherwise we send them via parallel port (when using separate machines)
    parallel_port= True
# This would be used when we need to filter microsaccades based on direction otherwise set to false
stimLocationCond=False 
while keepGoing==True:
    # The first part sets up the socket connection and ideally we would want to do this
    # for the first run and have it stay connected as long as the experiment is in 
    # progress
    if socket_conn==True:
        if multiple_machines==False:
            print('Waiting for psychopy...',flush=True)
            ## Sender
            # This would send saccade messages
            # Set up the parameters for the socket
            sendAddress = ('localhost', 6000)
            while True:
                try:
                    connSender = Client(sendAddress, authkey=b'secret password')
                    break
                except ConnectionRefusedError:
                    time.sleep(0.1)
                    continue
            ## Listener
            # This would receive stimulus time stamp and message to stop the main loop
            # Set up parameters for the socket
            listenAddress=('localhost',6001)
            # family is deduced to be 'AF_INET'
            listener = Listener(listenAddress, authkey=b'secret password')
            connListener = listener.accept()
            
            print('connection established',flush=True)
        else: 
            print('Looking for stimulus computer!')
            # We setup LSL once
            # first resolve an TSLO stream on the lab network
            streams = resolve_stream('type', 'Server')
            
            # create a new inlet to read from the stream
            inlet = StreamInlet(streams[0])
            print('Connection established!')

    # We look for messages from psychopy to check if the trial/stream has started
    # and also to get the trial info string 
    # This sits in a loop and polls for message until gets the start signal from
    # psychopy
    trialStartMsg=True
    while trialStartMsg==True:
        if 'socketMsg' in locals() and socketMsg=='q':
            break
        if multiple_machines==True:
            # We would try to close existing socket connections 
            if 's' in locals():
                s.close()
            # We would also set up socket connection here
            host='192.168.0.143' # server IP 
            port=4000
            s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
            s.bind((host,port))
            s.setblocking(0)
            # Check for messages on LSL
            socketMsg=stimMachineRecv(streams,inlet,commSetup=True)[0]
            if 'S0' in socketMsg: # would have the trial info in this case
                trialMsg=socketMsg
            elif 'trialStart' in socketMsg:
                # this would ensure that the data collection is lined up with the video
                # recoding/ psychopy stream
                trialStartMsg=False
                # Get the starting time stamp at the same time as the other program
                startTime=customTime.millis()
            elif 'q' in socketMsg:
                keepGoing=False
                trialStartMsg=False
                conn_status=0
                sys.exit()
                break
        else:
            try:
                status=connListener.poll()
                if status==True:
                    socketMsg=connListener.recv()
                    if 'S0' in socketMsg: # would have the trial info in this case
                        trialMsg=socketMsg
                    elif 'trialStart' in socketMsg:
                        # this would ensure that the data collection is lined up with the video
                        # recoding/ psychopy stream
                        trialStartMsg=False
                        # Get the starting time stamp at the same time as the other program
                        startTime=customTime.millis()
                else: # no event
                    time.sleep(0.10)
            except (OSError,EOFError) as e:
                # This indicates the listener has disconnected/psychopy has closed so we would
                # set conn status back to 0
                conn_status=0
                keepGoing=False
                # close existing socket
                connListener.close()
                listener.close()
                connSender.close()
                break
        
    # We can also have it look for messages regarding stimulus conditions
    # Here we are using it to get stimulus location
    if stimLocationCond==True:
        if multiple_machines==True:
            socketMsg=stimMachineRecv(streams,inlet,commSetup=True)[0]
            # We use the message from psychopy to get stimulus location
            if 'Left' in socketMsg:
                stimLocation='left'
            elif 'Right' in socketMsg:
                stimLocation='right'
            elif 'q' in socketMsg:
                keepGoing=False
                trialStartMsg=False
                conn_status=0
                break    
        else:
            try:
                status=connListener.poll()
                if status==True:
                    socketMsg=connListener.recv()
                    # We use the message from psychopy to get stimulus location
                    if 'left' in socketMsg:
                        stimLocation='left'
                    elif 'right' in socketMsg:
                        stimLocation='right'
                else: # no event
                    time.sleep(0.10)
            except (OSError,EOFError) as e:
                # This indicates the listener has disconnected/psychopy has closed so we would
                # set conn status back to 0
                conn_status=0
                keepGoing=False
                # close existing socket
                connListener.close()
                listener.close()
                connSender.close()
                break
        
    # we setup arrays that would be later filled in the main loop
    num_samples=2**16
    xpos_list=np.zeros(num_samples)
    ypos_list=np.zeros(num_samples)
    timeStamp_list=np.zeros(num_samples)
    detected_list=np.zeros(num_samples)

    # 1. saccade detection options
    thres_fac = int(vel_thresh) # threshold factor, or lambda
    above_thres_needed = int(num_samples_above_thresh) # how many samples above threshold do we need?
    # When stimulus location is provided we would set a direction criterion based on that
    if stimLocationCond==True:
        if stimLocation=='left':
            restrict_dir_min= int(90)
            restrict_dir_max= int (270)
        elif stimLocation=='right':
            restrict_dir_min = int(45)
            restrict_dir_max = int(315)
    else:
        restrict_dir_min = int(0) # direction restriction.
        restrict_dir_max = int(0)
    samp_rate_now = int(1000) # resampling frequency
    anchor_vel_thres = thres_fac
    print_results = 0 # set to 0, if you don't want any feedback from stdout
    start_sample=30

    ## 2. import the module and set parameters
    detect_this = online_sac_detect_module.online_sac_detect()
    detect_this.set_parameters(thres_fac, above_thres_needed, 
                           restrict_dir_min, restrict_dir_max, 
                           samp_rate_now, anchor_vel_thres, print_results)
    detect_this.return_data() # returns current data (should be empty after loading)
    detect_this.get_parameters() # returns current parameters
    detect_this.reset_data() # resets/clears all existing data
    array_idx=0
    resetCounter=0
    conn_status=1
    ## Main Loop
    # Setup the ADC card using nidqmx library
    #1. Setup Analog input channel
    task_ai=nidaqmx.Task()
    task_ai.ai_channels.add_ai_voltage_chan("Dev1/ai0",terminal_config=TerminalConfiguration.RSE)
    task_ai.timing.cfg_samp_clk_timing(1000,"",sample_mode=AcquisitionType.HW_TIMED_SINGLE_POINT,samps_per_chan=1)
    task_do=nidaqmx.Task()
    task_do.do_channels.add_do_chan("Dev1/port0/line2")
    while conn_status==1: # this would prevent program flow closing before end of trial
        # We would read in the x position from the TSLO
        # Also add time stamp
        # An error here indicates the psychopy has shut down so we 
        # would also end this program
        xpos_raw= task_ai.read(number_of_samples_per_channel=1)
        # we scale it based on volts per degree setting on ICANDI
        volts_per_deg=0.8
        xpos_deg= xpos_raw[0]/volts_per_deg
        pix_per_deg=(512/4.75)
        xpos_pix=xpos_deg*pix_per_deg

        ypos=random.uniform(0.2, 0.21) # set an arbitary value for now
        ypos_pix=ypos*pix_per_deg
        pythonReadTime=customTime.millis()-startTime
        #Add to corresponding arrays
        # Since we know the samples are coming in at a regular rate 
        # We give it the array idx
        timeStamp_list[array_idx]=array_idx
        xpos_list[array_idx]=xpos_pix
        ypos_list[array_idx]=ypos_pix

        ### 4. Add new set of samples to data
        # add new sample coming in
        if array_idx<start_sample:
            array_idx+=1
            resetCounter+=1
            continue
        elif array_idx==start_sample:
            detect_this.add_data(xpos_list[range(1, array_idx)], 
                    ypos_list[range(1, array_idx)], 
                    timeStamp_list[range(1, array_idx)])
        else:
            detect_this.add_data(xpos_list[array_idx], 
                            ypos_list[array_idx], 
                            timeStamp_list[array_idx])

        #we reset the counter and the sitting in add data after 
        # certain number of samples
        resetSampleIdx=500
        counterWindow=20
        if resetCounter>resetSampleIdx:
            #reset data
            detect_this.reset_data()
            # add some data from earlier
            detect_this.add_data(xpos_list[(array_idx-counterWindow):array_idx],
                                ypos_list[(array_idx-counterWindow):array_idx],
                                timeStamp_list[(array_idx-counterWindow):array_idx])
            #reset counter
            resetCounter=0
        ### 5. Run detection method
        try:
            res_here, run_time_here = detect_this.run_detection() 
            # create a variable that tells whether or not there was a saccade
            sacc_status = res_here.sac_detected
            if math.isnan(sacc_status):
                sacc_status=0
            #do something when we find a saccade
            detected_list[array_idx]=sacc_status
        except (OSError,EOFError) as error:
            sacc_status=0
            print(error)

        try: # would help identify if connection is closed
            if np.sum(sacc_status)>0 : # This would only work if a saccade happens after
                #psychopy closes
                #connection closes automatically when the messages get non relevant/ when listener disconnects
                if parallel_port==True: 
                    task_do.write(True)
                else:   
                    connSender.send(1)
            elif np.sum(sacc_status)==0:
                # When there is no saccade we send a negative message
                if parallel_port==True:
                    task_do.write(False)
                else:
                    connSender.send(0)

        except (ConnectionAbortedError,ConnectionResetError) as e:
            print(e)
            continue

        # check message from socket before sending to psychopy
        if multiple_machines==True:
            while True:
                try:
                    (socketMsg,addr)=s.recvfrom(1024)
                    socketMsg=socketMsg.decode('utf-8')
                    s.sendto('q'.encode('utf-8'),addr)
                    break
                except BlockingIOError:
                    pass
            if 'socketMsg' in locals():
                checkMsg=True
            else:
                checkMsg=False
            if checkMsg==True:
                if 'trialStop' in socketMsg:
                    # we save the logFile before exiting
                    eyeposFilename=("logFiles/eyeposData_%d_%d_%s_logFile_%s.csv"%(thres_fac,above_thres_needed,trialMsg,time.strftime("%m_%d_%Y_%I_%M_%S",time.localtime())))
                    save_logfile(eyeposFilename,timeStamp_list,xpos_list,ypos_list,detected_list)
                    # we change conn status and the socket_conn boolean values at the 
                    # end of each stream to reset parameters
                    conn_status=0
                    socket_conn=False
                    checkMsg=False
                    s.close()
                    # We would close all ADC tasks before closing
                    try:
                        task_ai.close()
                        task_do.close()
                    except:
                        print('error closing cards!')
                    break
                elif 'q' in socketMsg:
                    checkMsg=False
                    socket_conn=False
                    conn_status=0
                    s.close()
                    # We would close all ADC tasks before closing
                    try:
                        task_ai.close()
                        task_do.close()
                    except:
                        print('error closing cards!')
                    sys.exit()
                    break
        else:
            try:
                status=connListener.poll()
                if status==True:
                    socketMsg=connListener.recv()
                    if 'trialStop' in socketMsg:
                        # we save the logFile before exiting
                        eyeposFilename=("logFiles/eyeposData_%d_%d_%s_logFile_%s.csv"%(thres_fac,above_thres_needed,trialMsg,time.strftime("%m_%d_%Y_%I_%M_%S",time.localtime())))
                        save_logfile(eyeposFilename,timeStamp_list,xpos_list,ypos_list,detected_list)
                        # we change conn status and the socket_conn boolean values at the 
                        # end of each stream to reset parameters
                        conn_status=0
                        socket_conn=False
                        break
            except (OSError,EOFError) as e:
                # This indicates the listener has disconnected/psychopy has closed so we would
                # set conn status back to 0
                conn_status=0
                keepGoing=False
                # close existing socket
                connListener.close()
                listener.close()
                connSender.close()
                break

        # we then increment the array idx
        array_idx+=1
        resetCounter+=1 

        # we reset arrat idx after a certain value
        if array_idx>(num_samples-5):
            array_idx=0
            continue
print('got here!')
# We would close all ADC tasks before closing
try:
    task_ai.close()
    task_do.close()
except:
    print('error closing cards!')
# Similarly we would close LSL 
if multiple_machines==True:
    inlet.close_stream()
# We print a message before exiting the program
print('All done!!')
