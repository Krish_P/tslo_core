from psychopy import visual, core, monitors,event
import os
import numpy
import time

import numpy as np
from ReceiveDataFunction import TSLOreceiver

monsize =  [1920, 1080]


#monitorsetting.setSizePix(monsize)
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[-1,-1,-1], screen=1)

rect_width=512
rect_height=256
rect_orientation=0
posx=0
posy=0

fixation_cross=visual.TextStim(window,text='+',pos=(posx,posy))
fixation_cross.draw()
window.flip()

event.waitKeys()
TSLOreceiver()
square_shape=visual.Rect(window,width=rect_width, height=rect_height,units='pix', lineColor='white',ori=rect_orientation,pos=(posx,posy),fillColor='red',fillColorSpace='rgb')

square_shape.draw()
window.flip()

event.waitKeys()

print('complete')