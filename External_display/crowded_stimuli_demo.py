from psychopy import gui, visual, core, data, event, logging, clock 
import numpy as np
import os  # handy system and path functions
import sys 
import pixlet

window = visual.Window(size=(900, 900),units='pix',fullscr=False,allowGUI=True,color=[0,0,0], screen=1)

#we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
letter_height=50
nominal_spacing=5
spacing=letter_height * nominal_spacing
# we can set whether or not we need a fixation cross
fixation_cross=True
# we can set this to true if we need to capture fixation cross
fixation_image=True
# We can either show only radial/tangential/both flankers
flanker_type='both'
# We can set them to be oblique
oblique_cond=False

target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="T",color=[-1,-1,-1] ,height=letter_height)
flanker_left = pixlet.PixLet(window,pos=(-spacing,0),alignHoriz='center', alignVert='center', text="+",color=[-1,-1,-1] ,height=letter_height)
flanker_right = pixlet.PixLet(window,pos=(spacing,0),alignHoriz='center', alignVert='center', text="+",color=[-1,-1,-1] ,height=letter_height)
flanker_up = pixlet.PixLet(window,pos=(0,spacing),alignHoriz='center', alignVert='center', text="+",color=[-1,-1,-1],height=letter_height)
flanker_down = pixlet.PixLet(window,pos=(0,-spacing),alignHoriz='center', alignVert='center', text="+",color=[-1,-1,-1] ,height=letter_height)
if fixation_cross==True:
    # Create fixation cross
    step_size=20
    fix_line_UR=visual.Line(window, start=(0,0), end=(0+step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_UL=visual.Line(window, start=(0,0), end=(0-step_size,step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LR=visual.Line(window, start=(0,0), end=(0+step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_line_LL=visual.Line(window, start=(0,0), end=(0-step_size,-step_size), lineColor=[-1,-1,-1], lineWidth=6 )
    fix_c=visual.Circle(window, size=12, fillColor=(0,0,0), pos=(0,0), lineColor=(-1,-1,-1) )
    target_c_ll=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    target_c_lr=visual.Circle(window, size=4, fillColor=(1,1,1), pos=(0,0), lineColor=(1,1,1) )
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 
    # Move crowded target away from the center
    eccentricity_pix=150
    if oblique_cond==True:
        eccentricity_x=fixation_pos[0]-eccentricity_pix
        eccentricity_y=fixation_pos[1]-eccentricity_pix
        target_location=(eccentricity_x,eccentricity_y)
        target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
        target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]-eccentricity_pix))
    else:
        eccentricity=fixation_pos[0]-eccentricity_pix
        target_location=(eccentricity,fixation_pos[1])
        target_c_ll.pos=((fixation_pos[0]-eccentricity_pix),(fixation_pos[1]))
        target_c_lr.pos=((fixation_pos[0]+eccentricity_pix),(fixation_pos[1]))
    target.pos=target_location
    
    # We would have separate spacings for testing meridian and opposite meridian
    flanker_spacing_testing_meridian=1.25
    flanker_deg_testing_meridian=flanker_spacing_testing_meridian*letter_height # The flanker spacing scales based on target letter size
    # Set the same for the opposite meridian
    flanker_spacing_opp_meridian=100
    flanker_deg_opp_meridian=flanker_spacing_opp_meridian*letter_height
    if flanker_type=='radial':
        # Flankers being tested are set first
        flanker_left.pos=(target_location[0]-flanker_deg_testing_meridian,target_location[1]-flanker_deg_testing_meridian)
        flanker_right.pos=(target_location[0]+flanker_deg_testing_meridian,target_location[1]+flanker_deg_testing_meridian)
        # Flankers not being tested
        flanker_up.pos=(target_location[0]-flanker_deg_opp_meridian,target_location[1]+flanker_deg_opp_meridian)
        flanker_down.pos=(target_location[0]+flanker_deg_opp_meridian,target_location[1]-flanker_deg_opp_meridian)
    elif flanker_type=='tangential':
        # flankers being tested are set first
        flanker_up.pos=(target_location[0]-flanker_deg_testing_meridian,target_location[1]+flanker_deg_testing_meridian)
        flanker_down.pos=(target_location[0]+flanker_deg_testing_meridian,target_location[1]-flanker_deg_testing_meridian)
        #flankers not being tested 
        flanker_left.pos=(target_location[0]-flanker_deg_opp_meridian,target_location[1]-flanker_deg_opp_meridian)
        flanker_right.pos=(target_location[0]+flanker_deg_opp_meridian,target_location[1]+flanker_deg_opp_meridian)
    elif flanker_type=='both':
        flanker_left.pos=(target_location[0]-flanker_deg_testing_meridian,target_location[1]-flanker_deg_testing_meridian)
        flanker_right.pos=(target_location[0]+flanker_deg_testing_meridian,target_location[1]+flanker_deg_testing_meridian)
        flanker_up.pos=(target_location[0]-flanker_deg_testing_meridian,target_location[1]+flanker_deg_testing_meridian)
        flanker_down.pos=(target_location[0]+flanker_deg_testing_meridian,target_location[1]-flanker_deg_testing_meridian)
    
    # We draw the fixation cross
    if fixation_image==False:
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_lr]:
            stim1.draw()
    else:
        for stim1 in [fix_line_UL,fix_line_UR,fix_line_LL,fix_line_LR,fix_c,target_c_ll,target_c_lr]:
            stim1.draw()
        
target.render()
flanker_right.render()
flanker_left.render()
flanker_up.render()
flanker_down.render()

if fixation_image==False:
    target.draw()
    flanker_left.draw()
    flanker_right.draw()
    flanker_up.draw()
    flanker_down.draw()
window.getMovieFrame(buffer='back')
window.flip()
core.wait(1)

window.close()

if fixation_image==False:
    window.saveMovieFrames('%s_Stimuli_%.2f'%(flanker_type,nominal_spacing)+'.jpg')
else:
    window.saveMovieFrames('Fixation_cross'+'.jpg')