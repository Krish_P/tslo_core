from psychopy import core, visual, event, monitors, data, gui
import numpy as np
import os
import time
import customTime
import random
from random import randint
from psychopy import logging
from psychopy.sound import Sound
import psychopy.core
logging.console.setLevel(logging.CRITICAL)
import cv2
from PixelConversion import PixelConversion
import pixlet
import online_sac_detect_module
import serial 
from SendDataFunction import TSLOsender 
from ReceiveDataFunction import TSLOreceiver
from multiprocessing.connection import Listener
from multiprocessing.connection import Client
import parallel64
from ximea import xiapi
#===================================================================================================================================
## SETUP CONDITIONS
# The examiner would set the main set of parameters before running this program
eye_tracker=True
lsl_connection=True
keep_repeating=True
cam_connection=True
# Display trigger options
# 1. Parallel port (server) : "Pport"
# 2. Digital out via NI card (old machine): "Dout"
# 3. Display triggers via photocell: "photocell"
trig_opt='Pport'
# Saccade trigger options
# 1. Parallel port (server) : "Pport"
# 2. Socket connection between programs(old machine): "socket"
saccade_trigger_comm='Pport'
#===================================================================================================================================
## First we set up the parameters to set up hardware

clock = psychopy.core.Clock()
# Equipment setup
monsize =  [1920, 1080]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)
window.setMouseVisible(False)
    
if eye_tracker==True:
    ## Setup listener
    # This would listen to saccade messages
    # Socket connection setup
    listenAddress=('localhost',6000)
     # family is deduced to be 'AF_INET'
    print('Looking for connection')
    listener = Listener(listenAddress, authkey=b'secret password')
    connListener = listener.accept()
    ## Setup Sender
    # This would send stimulus time and final close message to terminate all sockets
    # Set up the parameters for the socket
    sendAddress = ('localhost', 6001)
    while True:
        try:
            connSender = Client(sendAddress, authkey=b'secret password')
            break
        except ConnectionRefusedError:
            time.sleep(0.1)
            continue
    print('Connection established!')

# We also get the refresh rate of monitor. Which would be used later to control the duration of stimuli
refreshRate=window.getActualFrameRate()
#We enter the monitor parameters here and calculate pixels per cm
horz_res=1920
vert_res=1080
diagonal_size=62.5 #diagonal screen size
dist_to_screen= 180 #cm (for the mirror setup with 165cm (mirror to screen distance)+ 10 cm(between mirror distance)+5 cm (mirror to eye distance))

#we then get the pixel conversion value
pixel_per_degree=PixelConversion(horz_res,vert_res,diagonal_size,dist_to_screen)

# Firstly, we create the fixation target so it can be used fromt the very start of the experiment
fix_xpos=0
step_size=20
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=6, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

# Additionally,we set up the camera feature,so that subjects can visualize the retinal image quality and the PMT gain setting.
# For this to work the webcam would have to be setup such that it faces the retinal image window on ICANDI and the PMT box with the 
# gain setting
if cam_connection==True:
    # Note: Main purpose is to be able to use this when running the experiment SOLO. 
    cam_image=visual.ImageStim(win=window,pos=[0,0],size=[1000,1000],opacity=1.0,flipHoriz=True)
    #create instance for first connected camera 
    cam = xiapi.Camera()
    #start communication
    cam.open_device()
    #settings
    cam.set_exposure(20000)
    #create instance of Image to store image data and metadata
    img = xiapi.Image()
#===================================================================================================================================
# We also open the parallel port connection
p=parallel64.StandardPort(spp_base_address=0x3fc)
while keep_repeating==True:
    #Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
    if lsl_connection==True:
        keepGoing=True
        webcam_state=False
        while keepGoing:
            startup_msg1=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,150),color=[-1,-1,-1],height=pixel_per_degree/4)
            startup_msg2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-150),color=[-1,-1,-1],height=pixel_per_degree/3)
            startup_msg1.draw()
            startup_msg2.draw()
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
            window.flip()

            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
            elif len(keys)>0:
                if keys[0]=='space':
                    webcam_state=True                
                    #start data acquisition
                    cam.start_acquisition()
                    while webcam_state==True & cam_connection==True:
                        #get data and pass them from camera to img
                        cam.get_image(img)
                        #create numpy array with data from camera. Dimensions of the array are 
                        #determined by imgdataformat
                        camData = img.get_image_data_numpy()
                        #show acquired image 
                        newdata=np.subtract(np.divide(camData,128.0),1.0)
                        cam_image.setImage(newdata)
                        cam_image.draw()
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw() # overlays fixation cross
                        window.flip()
                        theKey = event.getKeys()
                        if len(theKey)!=0:
                            webcam_state=False
                            #stop data acquisition
                            cam.stop_acquisition()

    # we stop if escape is pressed
    if 'keys' in locals() and lsl_connection==True:
        if keys[0]=='escape':
            keep_repeating=False
            continue
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        protocol_packet=TSLOreceiver()

        # These parameters are provided from TSLO machine
        eccentricity_deg=protocol_packet[0]
        num_repeats=protocol_packet[2] 
        # This would be the case where we are repeating specific conditions
        if num_repeats==1:
            no_microsacc_cond=protocol_packet[1]
            flanker_info=protocol_packet[3]
            #Note for this the subject ID would have to be hardcoded
            subj_id=1
        else:
            # Otherwise 3rd info corresponds to subject id
            subj_id=protocol_packet[3]

    else: 
        # If LSL is false set these parameters locally to their defaults
        eccentricity_deg=5
        #no_microsacc_cond=1 # Sets whether it should stop with or w/o microsaccades
        subj_id=1
        num_repeats=1 # Sets the number of repeats for each flanker spacing at a given ecc

    # These parameters are set locally (Stimulus Machine)
    cue_condition=False
    mask_condition=True
    location_uncertainity=False 
    inner_flanker=False
    reverse_flanker_contrast=False
    save_image=False 
    foveal_mode=False

    target_type='numbers'

    # These parameters would be set at the start of the experiment and ideally would not want to change them 
    # after the start of the experiment
    #Note: For the cue condition: The cue is set to appear after brief fixation period following which the subjects fixate 
    # on the fixation cross for variable duration before stimulus onse5
    # Additionally we set the duration of the post stimulus mask duration and the time window to obtain subjective responses
    target_presentation=0.048 #sec

    if num_repeats==1:
        flanker_spacing_list=[float(flanker_info)]
    else:
        flanker_space=[1.6,100]

    timing_list=np.arange(0.400,0.500,0.25)

    # The no microsaccade condition would be interleaved with the microsaccade condition
    # Only a small proportion of trials would fall under the no microsaccade condition
    if num_repeats>1:
        prop_nosacc=0.2 # 20% of trials would be no microsaccade condition
        num_repeats_nosacc=num_repeats * prop_nosacc
        if int(num_repeats_nosacc)!=num_repeats_nosacc: # would help catch cases where num repeats is a float
            num_repeats_nosacc=1
        #We would then generally multiply each element of the flanker spacing list based on the number of repeates required at each level
        # we create separate flanker spacing list for each of the conditions
        #Microsaccade condition:
        print(num_repeats)
        flanker_spacing_list_sacc=np.repeat(flanker_space,num_repeats)
        microsacc_cond=np.zeros(len(flanker_space)*int(num_repeats))
        # No microsaccade condition
        flanker_spacing_list_nosacc=np.repeat(flanker_space,num_repeats_nosacc)
        nomicrosacc_cond=np.ones(len(flanker_space)*int(num_repeats_nosacc))
        # we would concatenate both lists and shuffle them and this what would be used 
        # in the for loop of the main experiment
        flanker_spacing_list=np.concatenate([flanker_spacing_list_sacc,flanker_spacing_list_nosacc])
        microsacc_status_list=np.concatenate([microsacc_cond,nomicrosacc_cond])
        temp_list=list(zip(flanker_spacing_list,microsacc_status_list))
        random.shuffle(temp_list)
        flanker_spacing_list,microsacc_status_list=zip(*temp_list)
        flanker_spacing_list=list(flanker_spacing_list)
        microsacc_status_list=list(microsacc_status_list)
    else:
        microsacc_status_list=[no_microsacc_cond]

    params={1:{1:0.14,2:0.36,3:0.33,4:0.43,5:0.48},
            2:{1:0.19,2:0.31,3:0.36,4:0.34,5:0.43},
            3:{1:0.22,2:0.27,3:0.43,4:0.46,5:0.56},
            4:{1:0.27,2:0.42,3:0.48,4:0.40,5:0.61},
            5:{1:0.17,2:0.37,3:0.39,4:0.56,5:0.64} }

    #===================================================================================================================================
    ## Create basic shapes
    # These would be drawn & /or manipulated during the main experiment
    # The stimulus parameters are altered based on whether or not positional ambiguity is set to True/False
    if location_uncertainity==True:
        stim_location_list=['left','right']
    else:
        stim_location_list=['right']
    fix_xpos=0
    msg_offset=150
    step_size=20 #pixels
    fix_ypos=fix_xpos+step_size #mid point of horizontal fixation line

    #Create basic stimuli here that could be called multiple times during the course of the experiment
    small_square=visual.Rect(window,width=200,height=200,units='pix',fillColor='white', fillColorSpace='rgb',pos=(850,-500))
    wait_msg=visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Press any key to start the experiment')
    wait_msg2= visual.TextStim(window,pos=(0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2,text='Please wait...')   

    # The next few lines would help create the cue target (letter T) that could either point to the left/right based on the target location
    cue_target_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
    # Cue for rightward target location
    cue_target_v_right1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    cue_target_v_right2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6) 
    #cue for leftward target location
    cue_target_v_left1=visual.Line(window, start=(fix_xpos,0+step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)
    cue_target_v_left2=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos-step_size,0), lineColor=[-1,-1,-1], lineWidth=6)

    #we draw the stimulus using the experimental parameters,the sizes would be adjusted later in the main experiment
    # Here we would draw it based on the target type that was selected earlier
    if target_type=='E' or target_type=='bars':
        target = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_left = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_right = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_up = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
        flanker_down = visual.TextStim(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        target.font="Sloan"
        flanker_left.font="Sloan"
        flanker_right.font="Sloan"
        flanker_up.font="Sloan"
        flanker_down.font="Sloan"
    elif target_type=='numbers':
        target = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Target",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_left = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_right = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        flanker_up = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1],height=pixel_per_degree )
        flanker_down = pixlet.PixLet(window,pos=(0,0),alignHoriz='center', alignVert='center', text="Flanker",color=[-1,-1,-1] ,height=pixel_per_degree)
        
    # This is the bigger outer loop that basically repeats a bunch of times for a given eccentricity and letter size  
    # The letter size is kept constant and is set to 1.5x the threshold from the isolated letter threshold measures.
    # To ensure letter size is supra threshold
    letter_height=(params[subj_id][eccentricity_deg])*1.5
    letter_height_pix=letter_height*pixel_per_degree

    #===================================================================================================================================
    ## Experimental sequence:

    #We use the position of the fixation cross center to position the target and flankers
    fixation_pos=(fix_c.pos[0],fix_c.pos[1]) 

    # The following set of lines set the flanker condition 
    eccentricity_pix=eccentricity_deg*pixel_per_degree
    eccentricity=fixation_pos[0]+eccentricity_pix
    if foveal_mode==True:
        target_location=(fix_h.start[0],0)
    else:
        target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
    target.pos=target_location
                                  
    #This is used to provide a audio feedback to the user
    beep_sound_correct=Sound(value=1000,secs=0.25)
    beep_sound_incorrect=Sound(value=250,secs=0.25)

    #Have something that waits for a key press from the subject. This essentially would initiate the sync between the stimulus and 
    #imaging machines and also start the experiment. 
    if lsl_connection==True:
        keepGoing=True
        while keepGoing:
            stream_wait1=visual.TextStim(window,text=('Waiting for TSLO machine'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/4)
            stream_wait2=visual.TextStim(window,text=('Press any key to continue'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=pixel_per_degree/3)

            stream_wait1.draw()
            stream_wait2.draw()
                
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()

            window.flip()
                
            keys=event.getKeys()
            if len(keys)>0 and 'space' not in keys:
                keepGoing=False
        #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
        #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
        data_packet=TSLOreceiver()
       
    #set a timer to get stimulus timing
    timerDuration=60 #set for 1 minute for now since trials would be shorter than this
    timer = core.CountdownTimer(timerDuration)

    #===================================================================================================================================
    ## Start of main loop of experiment
    # We would get this from the TSLO machine and would help number the files 
    # consistently across both machines
    if lsl_connection==True:
        repeatCounterStart=data_packet[2]
    else:
        repeatCounterStart=0
    flanker_counter=1 # would be used to identify the last flanker condition
    for aflanker in flanker_spacing_list:
        # For repeats/custom trials we have matlab set the parameters otherwise
        # psychopy controls the set of flankers and microsaccade condition
        if num_repeats>1:
            # We set the microsaccade condition based on the flanker counter index
            no_microsacc_cond=microsacc_status_list[flanker_counter-1]
        # Check if escape was pressed and stop loop
        if 'key_pressed' in locals():
            if key_pressed=='escape':
                keep_repeating=False
                break
        if 'keys' in locals():
            if keys=='espace':
                keep_repeating=False
                break
        # we create a new variable that would hold the value for flanker spacing 
        # to be used for naming the output file
        #if aflanker is a float this would result in a value !=0 else would be 0
        if int(aflanker) % float(aflanker)==0: 
            flanker_val=int(aflanker)
            flanker_val_str='%d'%(flanker_val)
        else:
            flanker_val=float(aflanker)
            flanker_val_str='%.1f'%(flanker_val)
        # 1. We create the behavioral output file
        # We use the information recieved from the TSLO machine to set the stimulus conditions and also to name the output file 
        # accordingly... Currently, the time duration of the entire experiment is the only parameter that is set using the transferred
        # data
        #output filename
        file_timestamp=time.strftime("%m_%d_%Y_%I_%M_%S", time.localtime())
        if lsl_connection==True:
            trial_info='S0%d_S%d_T00%d_ecc_%d_spacing_%s'%((data_packet[0]),(data_packet[1]),repeatCounterStart, eccentricity_deg,
                       flanker_val_str)
            if no_microsacc_cond==1:
                outfilename = ("results/%s_NoMicrosaccade_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
            else:
                outfilename = ("results/%s_Crowding_%s.csv" % (trial_info,file_timestamp) ) 
            timerDuration=data_packet[3]
        else:
            trial_info='S0%d_S%d_T00%d_ecc_%d_spacing_%s'%(subj_id,0,0,eccentricity_deg, flanker_val_str)
            outfilename = ("results/%s_%s_%s.csv" % (trial_info,'Crowding',file_timestamp ) )
        outfile = open(outfilename, "a")

        # we also another file that would store the information regarding trials
        # where saccade related conditions were not met and were omitted
        if lsl_connection==True:
            failedFilename='FailedTrials/S0%d_S%d_failedTrials.csv'%(data_packet[0],data_packet[1])
        else:
            failedFilename='FailedTrials/S0%d_S%d_failedTrials.csv'%(subj_id,0)
        failedFile=open(failedFilename,"a")
        #add header to failed file
        #failedFile.write("%s,%s,%s,%s,%s,%s\n"%("subj_id","session_num","trial_no","ecc","spacing","microsaccade_condition"))
        
        if eye_tracker==True:
            # We send the trial details to the tracker/saccade detection method
            connSender.send(trial_info)
            # Also send a message to indicate the start of the sequence/stream. This 
            # would be used to store data and write to log file
            connSender.send("trialStart")
            # we get the starting time stamp in ms and this can be used to get relative
            # time stamp for stimulus onset
            startTime=customTime.millis()
            
        #Add the header 
        outfile.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n"%("resp","target","inner","outer","down","up","size","ecc","spacing","stim_loc",
                     "ISI","stim_dur","corr","stimTime") )

        # 2. We setup the eye tracking and stimulus parameters for the current trial
        # we generate a random number which would indicate when the program would 
        # break out from the while loop
        if eye_tracker==True:
            # Set randNum to a large num for consistency with non-tracking debug method
            randNum=100
        else:
            randNum=np.random.randint(5,10)
            
        # we would wait for atleast a certain number of trials before halting the program
        minTrial=random.randint(2,4)
        event.clearEvents()
        #Have something that waits for a key press from the subject. This would send the flanker spacing condition to the TSLO system.
        if lsl_connection==True:
            keepGoing=True
            while keepGoing:
                trialStart1=visual.TextStim(window,text=('Sending flanker spacing condition'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/4)
                trialStart2=visual.TextStim(window,text=('Press any key to start trial'),pos=(0.0,-msg_offset),color=[-1,-1,-1],height=pixel_per_degree/3)

                trialStart1.draw()
                trialStart2.draw()
                    
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                
                window.flip()
                    
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    keepGoing=False
                elif len(keys)>0:
                    if keys[0]=='space':
                        webcam_state=True                
                        #start data acquisition
                        cam.start_acquisition()
                        while webcam_state==True and cam_connection==True:
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw() # overlays the fixation cross
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()
            #TSLO sender keeps sending packets until it is received by the TSLO machine. The second message here
            # is a dummy message
            TSLOsender(aflanker,repeatCounterStart)
            TSLOsender(no_microsacc_cond,0)
        else:
            keepGoing=True
            while keepGoing:
                wait_msg.draw()
              
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                window.flip()
                    
                keys=event.getKeys()
                if len(keys)>0 and 'space' not in keys:
                    keepGoing=False
                elif len(keys)>0:
                    if keys[0]=='space':
                        webcam_state=True                
                        #start data acquisition
                        cam.start_acquisition()
                        while webcam_state==True and cam_connection==True:
                            #get data and pass them from camera to img
                            cam.get_image(img)
                            #create numpy array with data from camera. Dimensions of the array are 
                            #determined by imgdataformat
                            camData = img.get_image_data_numpy()
                            #show acquired image 
                            newdata=np.subtract(np.divide(camData,128.0),1.0)
                            cam_image.setImage(newdata)
                            cam_image.draw()
                            for stim1 in [fix_v, fix_h, fix_c]:
                                stim1.draw() # overlays the fixation cross
                            window.flip()
                            theKey = event.getKeys()
                            if len(theKey)!=0:
                                webcam_state=False
                                #stop data acquisition
                                cam.stop_acquisition()
        if keys[0]=='escape':
            keep_repeating=False
            break
        #3. Main trial loop
        trialCounter=0 #increments on every trial
        # the program would break out of the while loop if:
        #   1. There trial counter is greater than the randNum 
        #   2. There was a saccade event on the last trial
        while trialCounter<=randNum:
            # The following set of lines set the flanker condition 
            # Additionally the horizontal position of the target is randomized
            
            eccentricity_pix=eccentricity_deg*pixel_per_degree
            stim_location=np.random.choice(stim_location_list)
                
            if stim_location=='left':
                eccentricity=fixation_pos[0]-eccentricity_pix
            elif stim_location=='right':
                eccentricity=fixation_pos[0]+eccentricity_pix

            #We also have a condition where the target can be presented at the foveally
            # replacing the fixation cross
            if foveal_mode==True:
                target_location=(fix_h.start[0],0)
            else:
                target_location=(eccentricity,0) # Center (pixels) of letter or trigram to identify 
            target.pos=target_location
            if target_type=='bars':
                target_let='e'
                target_orientation=[0,270]
            elif target_type=='E':
                target_let='E'
                target_orientation=[0,90,180,270]
            elif target_type=='numbers':
                target_nums=['0','1','2','3','4','5','6','7','8','9']
                random.shuffle(target_nums)
                target_let=target_nums[0]
                flanker_let_1=target_nums[1]
                flanker_let_2=target_nums[2]
                flanker_let_3=target_nums[3]
                flanker_let_4=target_nums[4]

            #The letter size is set here
            letter_size_deg=letter_height
            letter_size_pix=letter_size_deg*pixel_per_degree
            target.height=letter_size_pix
            flanker_left.height=letter_size_pix
            flanker_right.height=letter_size_pix
            flanker_up.height=letter_size_pix
            flanker_down.height=letter_size_pix
                
            #THe flanker spacing is set on eacn trial based on the stimulus size
            flanker_deg=aflanker*letter_height # The flanker spacing scales based on target letter size
            flanker_spacing_pix=int(flanker_deg*pixel_per_degree)
            flanker_left.pos=(target_location[0]-flanker_spacing_pix,0)
            flanker_right.pos=(target_location[0]+flanker_spacing_pix,0)
            flanker_up.pos=(target_location[0],flanker_spacing_pix)
            flanker_down.pos=(target_location[0],-flanker_spacing_pix)
                    
            # Now that we have the stimulus size and flanker spacing we rotate the target letter and 
            # flankers independently 
            # Note: This would only have to be done in the case of tumbling E's and bars 
            if target_type=='E' or target_type=='bars':
                letter_orientation=([(np.random.choice(target_orientation)) for n in [0,1,2,3,4] ])
                target_orientation=letter_orientation[0]
                target.ori=letter_orientation[0]
                flanker_left.ori=letter_orientation[1]
                flanker_right.ori=letter_orientation[2]
                flanker_up.ori=letter_orientation[3]
                flanker_down.ori=letter_orientation[4]

            # we can also switch the contrast between the target and flankers
            if reverse_flanker_contrast==True:
                flanker_left.color=[1,1,1]
                flanker_right.color=[1,1,1]
                flanker_up.color=[1,1,1]
                flanker_down.color=[1,1,1]

            # The following set of code is used to present the stimulus & cue at pre-determined
            # locations and duration
            # The experimental sequence is as follows:
            # 1. Variable delay b/w 400-500 msec 
            # 2. Stimulus presentation for 48 msec
            
            # we add a delay on the very first trial so each video is padded to prevent 
            # any stimuli from being missed from frame grab/recording
            initialDelay=1
            if trialCounter==0:
                nflips=0
                while nflips<(refreshRate*initialDelay):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    wait_msg2.draw()
                    window.flip()
                    nflips+=1
                    
                    # throw away any saccade messages
                    if eye_tracker==True:
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()
                            
            #we wait for a pseudo-random time before stimulus onset
            #to prevent anticipation
            clock.reset()
            ISI=np.random.choice(timing_list)
            nflips=0
            #arbitary number of flips during which we get the saccade status. 
            # We would not do this for the first trial of the sequence, and events
            # here would correspond to saccades that occurred after the previous stimulus offset
            # No microsaccade condition: This would be the opposite case of the current paradigm
            # where we would stop the program after a few trials (random) only when microsaccades
            # are not detected after previous stimulus offset
            maxReceiveWindow=60 
            sacc_cond=0 # default to no event happened. Only set to True if get evnt from detection process
            while nflips< int(refreshRate*ISI):
                for stim1 in [fix_v, fix_h, fix_c]:
                    stim1.draw()
                if eye_tracker==True:
                    if nflips<maxReceiveWindow and trialCounter>minTrial:
                        # when using the eye tracker we look for a saccade event following stimulus onset
                        # and halt the program when one is detected. 
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                try:
                                    got_one=connListener.recv()
                                    sacc_cond = sacc_cond | got_one
                                    #print('%d,%d,%d,%d '%(trialCounter,nflips,got_one,sacc_cond),end='');
                                except EOFError:
                                    print('receive function error')
                                    sacc_cond=0
                        elif saccade_trigger_comm=='Pport':
                            raw_msg=p.read_status_register()
                            # TODO: need to figure out which is 1
                            if raw_msg & 64==0:
                                got_one=True
                                sacc_cond = sacc_cond | got_one
                    else:
                        # throw away saccade messages outside window of interest
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()
                window.flip()
                nflips+=1

            # we clear key events that could occured after the end of the previous trial 
            event.clearEvents()
            # we would only draw the next stimulus only when a saccade was not detected 
            # during the delay period. If saccade did occur we skip to the response window
            if sacc_cond!=1 and no_microsacc_cond==False:
            # This would be for the no microsaccade or control condition
            # Here we would only draw a stimulus when a saccade is not detected 
            # We would also have a control condition where we would show the stimulus
                # we then set the respective target and flanker characters only when 
                # there are no saccades detected during the delay period
                target.text=target_let
                flanker_left.text=flanker_let_1
                flanker_right.text=flanker_let_2
                flanker_up.text=flanker_let_3
                flanker_down.text=flanker_let_4
                # The stimulus duration is used as a fraction of the frame rate of the display;
                # THis ensures better timing of the target presentations
                target.render()
                flanker_right.render()
                flanker_left.render()
                flanker_up.render()
                flanker_down.render()
                for frameN in range(int(refreshRate*target_presentation)):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw() 
                    target.draw()                
                    # we test whether the stimulus would appear on the left/right and based
                    # on that we set the inner and outer flankers
                    if stim_location=='left':                    
                        inner_flanker=flanker_right
                        outer_flanker=flanker_left
                    elif stim_location=='right':
                        inner_flanker=flanker_left
                        outer_flanker=flanker_right
                    # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
                    if inner_flanker==True or foveal_mode==True:
                        inner_flanker.draw()
                    outer_flanker.draw()
                    flanker_up.draw()
                    flanker_down.draw()
                    
                    # we clear unwanted saccade messages during stimulus display
                    if eye_tracker==True:
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()

                    #Draw the small square for Photodiode to detect and to create a marker in the video
                    if trig_opt=='photocell':
                        if frameN<5:
                            small_square.draw()
                        
                    # We capture fixation target and save to image file if needed
                    if save_image==True & frameN<5:
                        window.getMovieFrame(buffer='back')

                    flip_time=window.flip()

                    if trig_opt=='Pport':
                        startFrame=2
                        endFrame=startFrame+1
                        if frameN==startFrame:
                            p.write_data_register(0)
                        elif frameN==endFrame:
                            p.write_data_register(0xff)

                    if frameN==0:
                        # we would use this to debug missed markers in the retinal video
                        stimTime=timerDuration-timer.getTime() #gives relative time stamps
                        currTime=customTime.millis()
                        if eye_tracker==True:
                            pythonTime=str(currTime-startTime) # gives timestamps based on python clock
                            # we send the stimulus time to tracker/saccade method via
                            # socket
                            connSender.send(pythonTime)
            # This would be the no microsaccade or control condition where 
            # we would present stimuli only when we dont get a microsaccade during the 
            # window of interest
            no_microsacc_status=0 # this would change when the number of reqd trials 
            # are reached and there is no microsaccade detected 
            if trialCounter>=minTrial and sacc_cond==0:
                no_microsacc_status==1
            elif no_microsacc_cond==True and no_microsacc_status!=1:
                # We would also have a control condition where we would show the stimulus
                # we then set the respective target and flanker characters only when 
                # there are no saccades detected during the delay period
                target.text=target_let
                flanker_left.text=flanker_let_1
                flanker_right.text=flanker_let_2
                flanker_up.text=flanker_let_3
                flanker_down.text=flanker_let_4
                # The stimulus duration is used as a fraction of the frame rate of the display;
                # THis ensures better timing of the target presentations
                target.render()
                flanker_right.render()
                flanker_left.render()
                flanker_up.render()
                flanker_down.render()
                for frameN in range(int(refreshRate*target_presentation)):
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw() 
                    target.draw()                
                    # we test whether the stimulus would appear on the left/right and based
                    # on that we set the inner and outer flankers
                    if stim_location=='left':                    
                        inner_flanker=flanker_right
                        outer_flanker=flanker_left
                    elif stim_location=='right':
                        inner_flanker=flanker_left
                        outer_flanker=flanker_right
                    # If inner flanker is set to false we simply omit drawing the same to the stimulus buffer
                    if inner_flanker==True or foveal_mode==True:
                        inner_flanker.draw()
                    outer_flanker.draw()
                    flanker_up.draw()
                    flanker_down.draw()
                    
                    # we clear unwanted saccade messages during stimulus display
                    if eye_tracker==True:
                        if saccade_trigger_comm=='socket':
                            status=connListener.poll()
                            if status==True:
                                clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()

                    #Draw the small square for Photodiode to detect and to create a marker in the video
                    if frameN<10:
                        small_square.draw()
                        
                    # We capture fixation target and save to image file if needed
                    if save_image==True & frameN<5:
                        window.getMovieFrame(buffer='back')

                    flip_time=window.flip()

                    if frameN==0:
                        # we would use this to debug missed markers in the retinal video
                        stimTime=timerDuration-timer.getTime() #gives relative time stamps
                        currTime=customTime.millis()
                        if eye_tracker==True:
                            pythonTime=str(currTime-startTime) # gives timestamps based on python clock
                            # we send the stimulus time to tracker/saccade method via
                            # socket
                            connSender.send(pythonTime)

            #Use this to save the stimulus image
            if save_image==True:
                window.saveMovieFrames('Stimulus_'+str(trialCounter)+'_'+str(np.abs(eccentricity_deg))+'_'+str(aflanker)+'.jpg')
            # We ask for a key press when program halts
            # To ensure that the program does not halt on the very first trial we 
            # we set a min trial value so we would have atleast a few trials till the 
            # program detects a saccade and stops
            if eye_tracker==True and trialCounter>minTrial:
                # This would be the upper limit of trials and would ensure that the stream
                # does not go on for ever when saccade events are not detected
                maxTrial=20
                if trialCounter>=(maxTrial):
                    # We would also send a message to the TSLO machine to indicate that the trial is complete
                    # The first message would indcate if the trial is complete (1:Yes, 0: No)
                    # The second message would indicate if this is the last trial of the sequence
                    # we would do a flip to erase last stimulus before running LSL
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    window.flip()
                    # We would send a different message to matlab to delete the video 
                    # associated with invalid trial
                    trialComplete=2 # invalid trial message
                    if lsl_connection==True:
                        # Since the trial is not valid we would repeat even if it
                        # is the last flanker condition of the sequence
                        sequenceComplete=0 
                        # we would send a message to the TSLO machine
                        TSLOsender(trialComplete,sequenceComplete)
                    connSender.send("trialStop")
                    # we would write the response to the output file
                    response_num='invalidTrial'
                    corr='NaN'
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%s,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )
                    # This would be an invalid trial,we would want to remove the output file and add the trial to the end of 
                    # the list
                    outfile.close()
                    os.remove(outfilename)
                    # append the faulty trial to the end of the list
                    flanker_spacing_list.append(aflanker)
                    microsacc_status_list.append(no_microsacc_cond)

                    # we also store trial info to a csv file
                    if lsl_connection==True:
                        failedFile.write("S0%d,S%d,T00%d,%d,%s\n"%(data_packet[0],data_packet[1],repeatCounterStart, eccentricity_deg,
                                        flanker_val_str))
                    else:
                        failedFile.write("S0%d,S%d,T00%d,%d,%s\n"%(subj_id,0,repeatCounterStart, eccentricity_deg,
                                        flanker_val_str))
                    # we reduce repeatCounterStart which is also the trial counter
                    # to account for the lost trial
                    repeatCounterStart-=1
                    break
                if sacc_cond==1 and no_microsacc_cond==False:# this looks for whether a saccade was initiated after stimulus onset
                    # We would also send a message to the TSLO machine to indicate that the trial is complete
                    # The first message would indcate if the trial is complete (1:Yes, 0: No)
                    # The second message would indicate if this is the last trial of the sequence
                    # we would do a flip to erase last stimulus before running LSL
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    window.flip()
                    trialComplete=1
                    if lsl_connection==True:
                        if flanker_counter==len(flanker_spacing_list):
                            sequenceComplete=1
                        else:
                            sequenceComplete=0
                        # we would send a message to the TSLO machine
                        TSLOsender(trialComplete,sequenceComplete)
                    # we would then get the behavioral response from the subject
                    keepGoing=True
                    while keepGoing==True:
                        response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2)
                        response_wait.draw()
                            
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                            
                        flip_time=window.flip()

                        key=event.getKeys()
                        if len(key)>0:
                            keepGoing=False
                            continue
                            
                        # During response screen throw away any saccade messages as they
                        # are not useful
                        if eye_tracker==True:
                            if saccade_trigger_comm=='socket':
                                status=connListener.poll()
                                if status==True:
                                    clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()

                    # we check if there was a keypress made duration the response window otherwise we set a defualt value
                    if 'key' not in locals():
                        key_pressed='Nil' # arbitary value 
                    elif len(key)==0:
                        key_pressed='Nil'
                    else:
                        key_pressed=key[0]
                            
                    #An escape key press here terminates the loop
                    if key_pressed=='escape':
                        keep_repeated=False
                        break
                    # we assign a response value based on the keypress that was made by the subject
                    if key_pressed=='num_0':
                        response_num='0'
                    elif key_pressed=='num_1':
                        response_num='1'
                    elif key_pressed=='num_2':
                        response_num='2'
                    elif key_pressed=='num_3':
                        response_num='3'
                    elif key_pressed=='num_4':
                        response_num='4'
                    elif key_pressed=='num_5':
                        response_num='5'
                    elif key_pressed=='num_6':
                        response_num='6'
                    elif key_pressed=='num_7':
                        response_num='7'
                    elif key_pressed=='num_8':
                        response_num='8'
                    elif key_pressed=='num_9':
                        response_num='9'
                    else:
                        response_num='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
                        
                    # we woudl provide feedback based on whether or not the response was correct
                    if response_num==target.text:
                        beep_sound_correct.play()
                        corr=1
                    else:
                        beep_sound_incorrect.play()
                        corr=0
                    
                    # we would write the response to the output file
                    if key_pressed!='escape':
                        outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%d,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )
                    # also send stop message to tracker/saccade method & save file
                    connSender.send('trialStop')
                    # after saccade event is observed we break out from the main loop
                    break
                # we would also have a control condition where the program would stop 
                # when there is no microsaccade and get behavioral response
                elif sacc_cond==0 and no_microsacc_cond==True:
                    # We would also send a message to the TSLO machine to indicate that the trial is complete
                    # The first message would indcate if the trial is complete (1:Yes, 0: No)
                    # The second message would indicate if this is the last trial of the sequence
                    # we would do a flip to erase last stimulus before running LSL
                    for stim1 in [fix_v, fix_h, fix_c]:
                        stim1.draw()
                    window.flip()
                    trialComplete=1
                    if lsl_connection==True:
                        if flanker_counter==len(flanker_spacing_list):
                            sequenceComplete=1
                        else:
                            sequenceComplete=0
                        # we would send a message to the TSLO machine
                        TSLOsender(trialComplete,sequenceComplete)
                    # we would then get the behavioral response from the subject
                    keepGoing=True
                    while keepGoing==True:
                        response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2)
                        response_wait.draw()
                            
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                            
                        flip_time=window.flip()

                        key=event.getKeys()
                        if len(key)>0:
                            keepGoing=False
                            continue
                            
                        # During response screen throw away any saccade messages as they
                        # are not useful
                        if eye_tracker==True:
                            if saccade_trigger_comm=='socket':
                                status=connListener.poll()
                                if status==True:
                                    clear_msg=connListener.recv()
                        elif saccade_trigger_comm=='Pport':
                            clear_msg=p.read_status_register()

                    # we check if there was a keypress made duration the response window otherwise we set a defualt value
                    if 'key' not in locals():
                        key_pressed='Nil' # arbitary value 
                    elif len(key)==0:
                        key_pressed='Nil'
                    else:
                        key_pressed=key[0]
                            
                    #An escape key press here terminates the loop
                    if key_pressed=='escape':
                        keep_repeated=False
                        break
                    # we assign a response value based on the keypress that was made by the subject
                    if key_pressed=='num_0':
                        response_num='0'
                    elif key_pressed=='num_1':
                        response_num='1'
                    elif key_pressed=='num_2':
                        response_num='2'
                    elif key_pressed=='num_3':
                        response_num='3'
                    elif key_pressed=='num_4':
                        response_num='4'
                    elif key_pressed=='num_5':
                        response_num='5'
                    elif key_pressed=='num_6':
                        response_num='6'
                    elif key_pressed=='num_7':
                        response_num='7'
                    elif key_pressed=='num_8':
                        response_num='8'
                    elif key_pressed=='num_9':
                        response_num='9'
                    else:
                        response_num='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
                    # we woudl provide feedback based on whether or not the response was correct
                    if response_num==target.text:
                        beep_sound_correct.play()
                        corr=1
                    else:
                        beep_sound_incorrect.play()
                        corr=0
                    
                    # we would write the response to the output file
                    if key_pressed!='escape':
                        outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%d,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )
                    # also send stop message to tracker/saccade method & save file
                    connSender.send('trialStop')
                    # after saccade event is observed we break out from the main loop
                    break
                # we would also have a control condition where the program would stop 
                # when there is no microsaccade and get behavioral response
                else: # when a saccade event is not observed
                    # This would handle trials where the subject is not expected to give a response
                    response_num='nan'
                    corr='nan'
                    # we would write the response to the output file
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%s,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )

            elif trialCounter<=minTrial: # when the trial since saccade is less than min trial value
                # we increment trialSinceSaccade by one
                #trialSinceSaccade+=1
                # This would handle trials where the subject is not expected to give a response
                response_num='nan'
                corr='nan'
                # we would write the response to the output file
                outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%s,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime,) )    
        
            # when not using the eye tracker we stop the program at certain points during the 
            # sequence and ask the subject to respond to the most resent stimulus 
            # presentation
            elif eye_tracker==False:
                if trialCounter==randNum:
                    # We would also send a message to the TSLO machine to indicate that the trial is complete
                    # The first message would indcate if the trial is complete (1:Yes, 0: No)
                    # The second message would indicate if this is the last trial of the sequence
                    trialComplete=1
                    if lsl_connection==True:
                        if flanker_counter==len(flanker_spacing_list):
                            sequenceComplete=1
                        else:
                            sequenceComplete=0
                        # we would send a message to the TSLO machine
                        TSLOsender(trialComplete,sequenceComplete)
                    # we would then get the behavioral response from the subject
                    keepGoing=True
                    while keepGoing==True:
                        response_wait=visual.TextStim(window,text=('Response?'),pos=(0.0,msg_offset),color=[-1,-1,-1],height=pixel_per_degree/2)
                        response_wait.draw()
                            
                        for stim1 in [fix_v, fix_h, fix_c]:
                            stim1.draw()
                            
                        flip_time=window.flip()

                        key=event.getKeys()
                        if len(key)>0:
                            keepGoing=False
                            continue

                    # we check if there was a keypress made duration the response window otherwise we set a defualt value
                    if 'key' not in locals():
                        key_pressed='Nil' # arbitary value 
                    elif len(key)==0:
                        key_pressed='Nil'
                    else:
                        key_pressed=key[0]
                            
                    #An escape key press here terminates the loop
                    if key_pressed=='escape':
                        keep_repeating=False
                        break
                    
                    # we assign a response value based on the keypress that was made by the subject
                    if key_pressed=='num_0':
                        response_num='0'
                    elif key_pressed=='num_1':
                        response_num='1'
                    elif key_pressed=='num_2':
                        response_num='2'
                    elif key_pressed=='num_3':
                        response_num='3'
                    elif key_pressed=='num_4':
                        response_num='4'
                    elif key_pressed=='num_5':
                        response_num='5'
                    elif key_pressed=='num_6':
                        response_num='6'
                    elif key_pressed=='num_7':
                        response_num='7'
                    elif key_pressed=='num_8':
                        response_num='8'
                    elif key_pressed=='num_9':
                        response_num='9'
                    else:
                        response_num='100'#arbitary value to prevent the program from crashing; happens when no key is pressed
                        
                    # we woudl provide feedback based on whether or not the response was correct
                    if response_num==target.text:
                        beep_sound_correct.play()
                        corr=1
                    else:
                        beep_sound_incorrect.play()
                        corr=0
                    # we would write the response to the output file
                    if key_pressed!='escape':
                        outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%d,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )

                # This would handle trials where the subject is not expected to give a response
                else:
                    response_num='nan'
                    corr='nan'
                    # we would write the response to the output file
                    outfile.write("%s,%s,%s,%s,%s,%s,%.2f,%.2f,%2f,%s,%2f,%2f,%s,%2f\n"%(response_num,target.text,inner_flanker.text,outer_flanker.text,flanker_up.text,flanker_down.text,
                                    letter_size_deg,eccentricity_deg,aflanker,stim_location,ISI,target_presentation,corr,stimTime) )

            # we would finally increment the trial counter
            trialCounter+=1

        # we increment the flanker counter after each stream
        flanker_counter+=1
        repeatCounterStart+=1
        #close the output file here
        outfile.close()
        # Also save failed file
        failedFile.close()

        # Read the file back in and clear the second last line. THis would be a copy of the last one to which 
        # the subject responds.
        # The program currently looks for microsaccade only after writing the last stimulus, hence when it gets 
        # a microsaccade we get the response and write the same stimulus parameters, so the one where we dont 
        # respond needs to be removed
        try:
            with open(outfilename,"r") as f:
                lines=f.readlines()
            # write back the outputfile after removing the 2nd last line
            with open(outfilename,"w") as f:
                for lineIdx in range(len(lines)):
                    if lineIdx!=(len(lines)-2):
                        f.write(lines[lineIdx])
            f.close()
        except FileNotFoundError:
            pass
            
    # We would print out a message for the subject and wait for them to hit a key before restarting the program
    loop_msg1=visual.TextStim(window,text='Block Complete!',pos=(0,0),height=pixel_per_degree/4)
    loop_msg2=visual.TextStim(window,text='Press any key to continue!',pos=(0,-150),height=pixel_per_degree/4)
    loop_msg1.draw()
    loop_msg2.draw()
    window.flip()
    event.waitKeys()

   
#End of experiment, exit gracefully:
end_msg=visual.TextStim(window,text='Done!',pos=(0,0),height=pixel_per_degree)
end_msg.draw()
window.flip()
event.waitKeys()

# we close the different programs/processes that were opened
window.close()
core.quit()

if eye_tracker==True:
    # we close the socket program
    connListener.close()
    listener.close()
    connSender.close()
    print('connection closed')