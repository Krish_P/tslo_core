from psychopy import visual, core, monitors,event
import os
import numpy as np
import time

# Equipment setup
monsize =  [1920, 1080]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)

#First we draw some of the basic stimulus
#1. Fixation cross
fix_xpos=0
step_size=20 #pixels
fix_h=visual.Line(window, start=(fix_xpos-step_size,0), end=(fix_xpos+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(fix_xpos,0-step_size), end=(fix_xpos,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=6, fillColor=(0,0,0), pos=(fix_xpos,0), lineColor=(0,0,0) )

#2. Arrow target
square_shape=visual.Rect(window,width=100,height=50,units='pix',fillColor='black',lineColor='black', fillColorSpace='rgb',pos=(0,200))
triangle_shape=visual.Polygon(window,edges=3,radius=50,units='pix',fillColor='black',lineColor='black', fillColorSpace='rgb',pos=(0,200))

#3.Text stimulus
good_pos_txt=visual.TextStim(window,pos=(0,100),color=[-1,-1,-1],height=50,text='Good Location!!\nPlease Hold!!')


keys=event.waitKeys()
fix_cross=0       
while 'escape' not in keys:
    keys=event.waitKeys()
    triangle_shape.pos=(0,200)
    triangle_shape.ori=0
    square_shape.pos=(0,200)
    square_shape.ori=0
    if len(keys)>0:
        if (keys[len(keys)-1] == 'left'):
            triangle_shape.pos=(-50,200)
            triangle_shape.ori=-90
            square_shape.draw()
            triangle_shape.draw()
            event.clearEvents()
        elif (keys[len(keys)-1] == 'right'):
            triangle_shape.pos=(50,200)
            triangle_shape.ori=90
            square_shape.draw()
            triangle_shape.draw()
        elif (keys[len(keys)-1] == 'up'):
            triangle_shape.pos=(0,250)
            square_shape.ori=90
            square_shape.draw()
            triangle_shape.draw()
        elif (keys[len(keys)-1] == 'down'):
            triangle_shape.pos=(0,150)
            triangle_shape.ori=-180
            square_shape.ori=-90
            square_shape.draw()
            triangle_shape.draw()
        elif (keys[len(keys)-1] == 'return'):
            good_pos_txt.draw()
        elif (keys[len(keys)-1] == 'space'):
            if fix_cross==0:
                fix_cross=1
            elif fix_cross==1:
                fix_cross=0
    if fix_cross==1:
        for stim1 in [fix_v, fix_h, fix_c]:
            stim1.draw()
    window.flip()


window.close()
core.quit()