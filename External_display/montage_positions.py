from psychopy import visual, core, monitors,event
import os
import numpy as np
import time
from psychopy import logging
logging.console.setLevel(logging.CRITICAL)
from ReceiveDataFunction import TSLOreceiver #This would be used to communicate with the TSLO machine
from PixelConversion import PixelConversion
import numpy as np

monsize =  [2160, 3840]
window=visual.Window(monsize, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=0)
diagonal_size=68.58
dist_to_screen=245
pixel_per_degree,pixel_per_cm=PixelConversion(monsize[0],monsize[1],diagonal_size,dist_to_screen)

lsl_connection=True
#Calibration options
# 9 point calib
# 5 point calib
calib_option='5Pt'

#Create basic stimuli here that could be called multiple times during the course of the experiment
step_size=20
fix_h=visual.Line(window, start=(0-step_size,0), end=(0+step_size,0), lineColor=[-1,-1,-1], lineWidth=6 )
fix_v=visual.Line(window, start=(0,0-step_size), end=(0,0+step_size), lineColor=[-1,-1,-1], lineWidth=6)
fix_c=visual.Circle(window, size=4, fillColor=(0,0,0), pos=(0,0), lineColor=(0,0,0) )

#Have something that waits for a key press from the subject. We get the experimental parameters from the TSLO machine
if lsl_connection==True:
    keepGoing=True
    while keepGoing:
        startup_msg=visual.TextStim(window,text=('Loading Experimental Parameters'),pos=(0.0,150),color=[-1,-1,-1],height=50)
        startup_msg.draw()
        window.flip()

        keys=event.getKeys()
        if len(keys)>0:
            keepGoing=False


    #TSLO receiver waits for information packets from the other machine (TSLO machine in this case). 
    #NOTE: Make sure that the TSLO machine is in send mode before calling this function. Otherwise it freezes the program. 
    protocol_packet=TSLOreceiver()

if calib_option=='9Pt':
    x_min=-100
    x_max=100
    y_min=-100
    y_max=100
    calib_posn_h=[0,x_min,x_max,0,x_min,x_max,0,x_min,x_max]
    calib_posn_v=[0,0,0,y_min,y_min,y_min,y_max,y_max,y_max]
elif calib_option=='5Pt':
    calib_step=0.10 * pixel_per_degree #pixels
    calib_posn_h=[0,(calib_step),(2*calib_step),(3*calib_step),(4*calib_step)]
    calib_posn_v=np.repeat(0,len(calib_posn_h))


#Have something that waits for a key press from the subject. This essentially would initiate the sync between the stimulus and 
#imaging machines
keepGoing=True
while keepGoing:
    for stim1 in [fix_v, fix_h, fix_c]:
        stim1.draw()
    window.flip()
    keys=event.getKeys()

    if len(keys)>0:
            keepGoing=False

for calib_idx in range(len(calib_posn_h)):
    # Set the fixation target location parameters
    xpos=calib_posn_h[calib_idx]
    ypos=calib_posn_v[calib_idx]
    # Initially check if the LSL connection is set to true
    if lsl_connection==True:
        #set a timer so that the program waits for video to finish
        # we split the time based on the number of points that we want image
        timerDuration=protocol_packet[3]/len(calib_posn_h)
        timer = core.CountdownTimer(timerDuration)

        while timer.getTime()>0:
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
                stim1.pos=[xpos,ypos]
            window.flip()
    else:
        timerDuration=1
        timer=core.CountdownTimer(timerDuration)

        while timer.getTime()>0:
            for stim1 in [fix_v, fix_h, fix_c]:
                stim1.draw()
                stim1.pos=[xpos,ypos]
            window.flip()


wait_scr_txt=visual.TextStim(window,text='Montage completed!',pos=(0,0),height=50)

wait_scr_txt.draw()
window.flip()
core.wait(2)

window.close()
core.quit()