from http import client
import socket
# This simply receives messages
def Main():
   
    host = '192.168.0.101' #Server ip
    port = 4000

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((host, port))
    s.setblocking(0)

    print("Server Started")
    while True:
        try:
            (data1,addr) = s.recvfrom(1024)
            data1=data1.decode('utf-8')
            print(data1)
            s.sendto('q'.encode('utf-8'),addr)
            break
        except BlockingIOError:
            continue

    while True:
        try:
            (data2,addr) = s.recvfrom(1024)
            data2=data2.decode('utf-8')
            print(data2)
            s.sendto('q'.encode('utf-8'),addr)
            break
        except BlockingIOError:
            continue
    s.close()
if __name__=='__main__':
    Main()